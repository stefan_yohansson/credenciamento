<?php

/* para teste dos examples roda-se php -S ip:port na raiz e abre
* http://ip:port/examples/formulariostone.php no navegador, então os arquivos
* tem de estar visiveis
*
*/

include('../boot.php');

$stone = new Credenciamento\Facades\Stone();

/* onlyHtml é definido por padrão como true, porém caso queira retornar o HTML inteiro
* é necessário setar manualmente como false e definir os assets da página
*/
$stone->onlyContent = false;
// assets e onlyContent = false não combinam
$stone->assets = array(
  'scripts' => array("<script type='text/javascript' src='../bower_components/jquery/dist/jquery.min.js'></script>"),
  'links' => array('<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />'),
);

$stone->oldInput = $_POST;

echo $stone->obterFormulario();
