<?php

/* para teste dos examples roda-se php -S ip:port na raiz e abre
* http://ip:port/examples/salvarformulariostone.php no navegador, então os arquivos
* tem de estar visiveis
*
*/

include('../boot.php');

$stone = new Credenciamento\Facades\Stone();

$input = array(
  'dados_cpfcnpj' => '16.647.409/0001-34',
  'dados_mcc' => '213-5',
  'dados_razaosocial_nomecompleto' => 'Stefan Yohansson da Silva Areeira Pinto',
  'dados_faturamentomensal' => '1200,00',
  'dados_ticketmedio' => '30,00',
  'pagamentos_banco' => 'Itaú',
  'pagamentos_agencia' => '29453-0',
  'pagamentos_agenciadv' => '3',
  'pagamentos_conta' => '593130',
  'pagamentos_contadv' => '1',
  'contatos_nome' => 'stefan',
  'contatos_email' => 'sy.fen0@gmail.com',
  'contatos_celular' => '88927382',
  'contatos_telefone' => '41412567',
  'enderecos_endereco' => 'Endereço bom',
  'enderecos_numero' => '13',
  'enderecos_complemento' => '',
  'enderecos_bairro' => 'nova parnamirim',
  'enderecos_cep' => '59151140',
  'enderecos_cidade' => 'natal',
  'enderecos_uf' => 'rn',
  'endereco_operadora' => array(
    0 => 'vivo',
    1 => 'claro',
    ),
  'condcomerciais_txdebito' => '10',
  'condcomerciais_txcredito' => '20',
  'condcomerciais_txparcelado26' => '30',
  'condcomerciais_txparcelado712' => '10',
  'condcomerciais_meiocaptura' => array(
    0 => 'posgprs',
    1 => 'mobile'
  ),
  'condcomerciais_posgprsqtd' => '1',
  'condcomerciais_posethqtd' => '',
  'condcomerciais_mobileqtd' => '1',
  'condcomerciais_pinpadqtd' => '',
);

$stone->usuario_id = 1;

list($return, $errors) = $stone->salvarFormulario($input);

if($errors)
{
  // errors
  var_dump($errors);die;
}

// caso contrário, faça alguma coisa com os dados de retorno, ou redirecione
