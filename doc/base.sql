create table rede (
  id bigserial primary key,
  nome character varying
);

insert into rede (id, nome) values (1, 'Stone');

-- tabela de produtos é basicamente para consulta rápida ( para obter os dados se faz join com produtos_x )
create table produtos (
  id bigserial primary key,
  form_data json,
  identificador character varying, -- o identificador é algum dado que identifique o formulário ex: (nome fantasia, cnpj)
  usuario_id bigint references usuario(id) on update cascade on delete cascade,
  solicitante_nome character varying,
  solicitante_email character varying,
  solicitante_telefone character varying,
  data_insercao timestamp without time zone default now(),
  valido boolean default true,
  enviado boolean default false,
  rede bigint references rede(id) on update cascade on delete cascade,
  ordenacao bigint default 1
);

create table produtos_stone (
  id bigserial primary key,
  produto_id bigint references produtos(id)  on update cascade on delete cascade,
  dados_cpfcnpj character varying,
  dados_mcc character varying,
  dados_razaosocial_nomecompleto character varying,
  dados_faturamentomensal character varying,
  dados_ticketmedio character varying,
  pagamentos_banco character varying,
  pagamentos_agencia character varying,
  pagamentos_agenciadv character varying,
  pagamentos_conta character varying,
  pagamentos_contadv character varying,
  contatos_nome character varying,
  contatos_email character varying,
  contatos_celular character varying,
  contatos_telefone character varying,
  enderecos_endereco character varying,
  enderecos_numero character varying,
  enderecos_complemento character varying,
  enderecos_bairro character varying,
  enderecos_cep character varying,
  enderecos_cidade character varying,
  enderecos_uf character varying,
  endereco_operadora character varying,
  condcomerciais_txdebito character varying,
  condcomerciais_txcredito character varying,
  condcomerciais_txparcelado26 character varying,
  condcomerciais_txparcelado712 character varying,
  condcomerciais_meiocaptura character varying,
  condcomerciais_posgprsqtd character varying,
  condcomerciais_posethqtd character varying,
  condcomerciais_mobileqtd character varying,
  condcomerciais_pinpadqtd character varying
);
