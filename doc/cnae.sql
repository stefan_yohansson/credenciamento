--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cnae; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cnae (
    cnae_id bigint NOT NULL,
    cnae_secao character varying(2),
    cnae_divisao character varying(20),
    cnae_grupo character varying(20),
    cnae_classe character varying(20),
    cnae_subclasse character varying(20),
    cnae_denominacao character varying,
    cnae_mcc bigint,
    cnae_grupo_mcc bigint
);


ALTER TABLE public.cnae OWNER TO postgres;

--
-- Name: cnae_cnae_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cnae_cnae_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cnae_cnae_id_seq OWNER TO postgres;

--
-- Name: cnae_cnae_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cnae_cnae_id_seq OWNED BY cnae.cnae_id;


--
-- Name: cnae_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cnae ALTER COLUMN cnae_id SET DEFAULT nextval('cnae_cnae_id_seq'::regclass);


--
-- Data for Name: cnae; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cnae (cnae_id, cnae_secao, cnae_divisao, cnae_grupo, cnae_classe, cnae_subclasse, cnae_denominacao, cnae_mcc, cnae_grupo_mcc) FROM stdin;
2	A	1	\N	\N	\N	AGRICULTURA, PECUÁRIA E SERVIÇOS RELACIONADOS	7399	8
3	A	1	01.1	\N	\N	Produção de lavouras temporárias	7399	8
4	A	1	01.1	01.11-3	\N	Cultivo de cereais	7399	8
13	A	1	01.1	01.13-0	\N	Cultivo de cana-de-açúcar	7399	8
15	A	1	01.1	01.14-8	\N	Cultivo de fumo	7399	8
17	A	1	01.1	01.15-6	\N	Cultivo de soja	7399	8
19	A	1	01.1	01.16-4	\N	Cultivo de oleaginosas de lavoura temporária, exceto soja	7399	8
24	A	1	01.1	01.19-9	\N	Cultivo de plantas de lavoura temporária não especificadas anteriormente	7399	8
35	A	1	01.2	\N	\N	Horticultura e floricultura	7399	8
36	A	1	01.2	01.21-1	\N	Horticultura	7399	8
39	A	1	01.2	01.22-9	\N	Floricultura	7399	8
41	A	1	01.3	\N	\N	Produção de lavouras permanentes	7399	8
42	A	1	01.3	01.31-8	\N	Cultivo de laranja	7399	8
44	A	1	01.3	01.32-6	\N	Cultivo de uva	7399	8
59	A	1	01.3	01.34-2	\N	Cultivo de café	7399	8
61	A	1	01.3	01.35-1	\N	Cultivo de cacau	7399	8
63	A	1	01.3	01.39-3	\N	Cultivo de plantas de lavoura permanente não especificadas anteriormente	7399	8
71	A	1	01.4	\N	\N	Produção de sementes e mudas certificadas	7399	8
72	A	1	01.4	01.41-5	\N	Produção de sementes certificadas	7399	8
75	A	1	01.4	01.42-3	\N	Produção de mudas e outras formas de propagação vegetal, certificadas	7399	8
77	A	1	01.5	\N	\N	Pecuária	7399	8
78	A	1	01.5	01.51-2	\N	Criação de bovinos	7399	8
82	A	1	01.5	01.52-1	\N	Criação de outros animais de grande porte	7399	8
86	A	1	01.5	01.53-9	\N	Criação de caprinos e ovinos	7399	8
89	A	1	01.5	01.54-7	\N	Criação de suínos	7399	8
91	A	1	01.5	01.55-5	\N	Criação de aves	7399	8
94	B	6	\N	\N	\N	EXTRAÇÃO DE PETRÓLEO E GÁS NATURAL	7399	8
104	A	1	01.6	\N	\N	Atividades de apoio à agricultura e à pecuária; atividades de pós-colheita	7399	8
105	A	1	01.6	01.61-0	\N	Atividades de apoio à agricultura	7399	8
110	A	1	01.6	01.62-8	\N	Atividades de apoio à pecuária	7399	8
115	A	1	01.6	01.63-6	\N	Atividades de pós-colheita	7399	8
117	A	1	01.7	\N	\N	Caça e serviços relacionados	7399	8
118	A	1	01.7	01.70-9	\N	Caça e serviços relacionados	7399	8
120	A	2	\N	\N	\N	PRODUÇÃO FLORESTAL	7399	8
121	A	2	02.1	\N	\N	Produção florestal - florestas plantadas	7399	8
122	A	2	02.1	02.10-1	\N	Produção florestal - florestas plantadas	7399	8
133	A	2	02.2	\N	\N	Produção florestal - florestas nativas	7399	8
142	A	2	02.3	\N	\N	Atividades de apoio à produção florestal	7399	8
143	A	2	02.3	02.30-6	\N	Atividades de apoio à produção florestal	7399	8
145	A	3	\N	\N	\N	PESCA E AQÜICULTURA	7399	8
146	A	3	03.1	\N	\N	Pesca	7399	8
147	A	3	03.1	03.11-6	\N	Pesca em água salgada	7399	8
152	A	3	03.1	03.12-4	\N	Pesca em água doce	7399	8
157	A	3	03.2	\N	\N	Aqüicultura	7399	8
158	A	3	03.2	03.21-3	\N	Aqüicultura em água salgada e salobra	7399	8
165	A	3	03.2	03.22-1	\N	Aqüicultura em água doce	7399	8
174	B	\N	\N	\N	\N	INDÚSTRIAS EXTRATIVAS	7399	8
175	B	5	\N	\N	\N	EXTRAÇÃO DE CARVÃO MINERAL	7399	8
177	B	5	05.0	05.00-3	\N	Extração de carvão mineral	7399	8
180	B	6	06.0	\N	\N	Extração de petróleo e gás natural	7399	8
181	B	6	06.0	06.00-0	\N	Extração de petróleo e gás natural	7399	8
185	B	7	\N	\N	\N	EXTRAÇÃO DE MINERAIS METÁLICOS	7399	8
186	B	7	07.1	\N	\N	Extração de minério de ferro	7399	8
187	B	7	07.1	07.10-3	\N	Extração de minério de ferro	7399	8
191	B	7	07.2	07.21-9	\N	Extração de minério de alumínio	7399	8
194	B	7	07.2	07.22-7	\N	Extração de minério de estanho	7399	8
197	B	7	07.2	07.23-5	\N	Extração de minério de manganês	7399	8
200	B	7	07.2	07.24-3	\N	Extração de minério de metais preciosos	7399	8
203	B	7	07.2	07.25-1	\N	Extração de minerais radioativos	7399	8
205	B	7	07.2	07.29-4	\N	Extração de minerais metálicos não-ferrosos não especificados anteriormente	7399	8
211	B	8	\N	\N	\N	EXTRAÇÃO DE MINERAIS NÃO-METÁLICOS	7399	8
212	B	8	08.1	\N	\N	Extração de pedra, areia e argila	7399	8
213	B	8	08.1	08.10-0	\N	Extração de pedra, areia e argila	7399	8
1070	E	37	\N	\N	\N	ESGOTO E ATIVIDADES RELACIONADAS	7399	8
2125	P	\N	\N	\N	\N	EDUCAÇÃO	7399	8
228	B	8	08.9	08.92-4	\N	Extração e refino de sal marinho e sal-gema	7399	8
232	B	8	08.9	08.93-2	\N	Extração de gemas (pedras preciosas e semipreciosas)	7399	8
2362	T	97	\N	\N	\N	SERVIÇOS DOMÉSTICOS	7399	8
517	C	19	19.1	19.10-1	1910100	Coquerias	7399	8
234	B	8	08.9	08.99-1	\N	Extração de minerais não-metálicos não especificados anteriormente	7399	8
239	B	9	\N	\N	\N	ATIVIDADES DE APOIO À EXTRAÇÃO DE MINERAIS	7399	8
240	B	9	09.1	\N	\N	Atividades de apoio à extração de petróleo e gás natural	7399	8
241	B	9	09.1	09.10-6	\N	Atividades de apoio à extração de petróleo e gás natural	7399	8
243	B	9	09.9	\N	\N	Atividades de apoio à extração de minerais, exceto petróleo e gás natural	7399	8
244	B	9	09.9	09.90-4	\N	Atividades de apoio à extração de minerais, exceto petróleo e gás natural	7399	8
248	C	\N	\N	\N	\N	INDÚSTRIAS DE TRANSFORMAÇÃO	7399	8
249	C	10	\N	\N	\N	FABRICAÇÃO DE PRODUTOS ALIMENTÍCIOS	7399	8
250	C	10	10.1	\N	\N	Abate e fabricação de produtos de carne	7399	8
251	C	10	10.1	10.11-2	\N	Abate de reses, exceto suínos	7399	8
262	C	10	10.1	10.13-9	\N	Fabricação de produtos de carne	7399	8
265	C	10	10.2	\N	\N	Preservação do pescado e fabricação de produtos do pescado	7399	8
266	C	10	10.2	10.20-1	\N	Preservação do pescado e fabricação de produtos do pescado	7399	8
269	C	10	10.3	\N	\N	Fabricação de conservas de frutas, legumes e outros vegetais	7399	8
270	C	10	10.3	10.31-7	\N	Fabricação de conservas de frutas	7399	8
272	C	10	10.3	10.32-5	\N	Fabricação de conservas de legumes e outros vegetais	7399	8
275	C	10	10.3	10.33-3	\N	Fabricação de sucos de frutas, hortaliças e legumes	7399	8
278	C	10	10.4	\N	\N	Fabricação de óleos e gorduras vegetais e animais	7399	8
279	C	10	10.4	10.41-4	\N	Fabricação de óleos vegetais em bruto, exceto óleo de milho	7399	8
281	C	10	10.4	10.42-2	\N	Fabricação de óleos vegetais refinados, exceto óleo de milho	7399	8
285	C	10	10.5	\N	\N	Laticínios	7399	8
286	C	10	10.5	10.51-1	\N	Preparação do leite	7399	8
288	C	10	10.5	10.52-0	\N	Fabricação de laticínios	7399	8
290	C	10	10.5	10.53-8	\N	Fabricação de sorvetes e outros gelados comestíveis	7399	8
292	C	10	10.6	\N	\N	Moagem, fabricação de produtos amiláceos e de alimentos para animais	7399	8
293	C	10	10.6	10.61-9	\N	Beneficiamento de arroz e fabricação de produtos do arroz	7399	8
296	C	10	10.6	10.62-7	\N	Moagem de trigo e fabricação de derivados	7399	8
298	C	10	10.6	10.63-5	\N	Fabricação de farinha de mandioca e derivados	7399	8
300	C	10	10.6	10.64-3	\N	Fabricação de farinha de milho e derivados, exceto óleos de milho	7399	8
302	C	10	10.6	10.65-1	\N	Fabricação de amidos e féculas de vegetais e de óleos de milho	7399	8
306	C	10	10.6	10.66-0	\N	Fabricação de alimentos para animais	7399	8
308	C	10	10.6	10.69-4	\N	Moagem e fabricação de produtos de origem vegetal não especificados anteriormente	7399	8
310	C	10	10.7	\N	\N	Fabricação e refino de açúcar	7399	8
311	C	10	10.7	10.71-6	\N	Fabricação de açúcar em bruto	7399	8
313	C	10	10.7	10.72-4	\N	Fabricação de açúcar refinado	7399	8
316	C	10	10.8	\N	\N	Torrefação e moagem de café	7399	8
320	C	10	10.8	10.82-1	\N	Fabricação de produtos à base de café	7399	8
322	C	10	10.9	\N	\N	Fabricação de outros produtos alimentícios	7399	8
323	C	10	10.9	10.91-1	\N	Fabricação de produtos de panificação	7399	8
325	C	10	10.9	10.92-9	\N	Fabricação de biscoitos e bolachas	7399	8
327	C	10	10.9	10.93-7	\N	Fabricação de produtos derivados do cacau, de chocolates e confeitos	7399	8
330	C	10	10.9	10.94-5	\N	Fabricação de massas alimentícias	7399	8
332	C	10	10.9	10.95-3	\N	Fabricação de especiarias, molhos, temperos e condimentos	7399	8
334	C	10	10.9	10.96-1	\N	Fabricação de alimentos e pratos prontos	7399	8
344	C	11	\N	\N	\N	FABRICAÇÃO DE BEBIDAS	7399	8
345	C	11	11.1	\N	\N	Fabricação de bebidas alcoólicas	7399	8
346	C	11	11.1	11.11-9	\N	Fabricação de aguardentes e outras bebidas destiladas	7399	8
349	C	11	11.1	11.12-7	\N	Fabricação de vinho	7399	8
351	C	11	11.1	11.13-5	\N	Fabricação de malte, cervejas e chopes	7399	8
354	C	11	11.2	\N	\N	Fabricação de bebidas não-alcoólicas	7399	8
355	C	11	11.2	11.21-6	\N	Fabricação de águas envasadas	7399	8
357	C	11	11.2	11.22-4	\N	Fabricação de refrigerantes e de outras bebidas não-alcoólicas	7399	8
362	C	12	\N	\N	\N	FABRICAÇÃO DE PRODUTOS DO FUMO	7399	8
363	C	12	12.1	\N	\N	Processamento industrial do fumo	7399	8
364	C	12	12.1	12.10-7	\N	Processamento industrial do fumo	7399	8
366	C	12	12.2	\N	\N	Fabricação de produtos do fumo	7399	8
372	C	13	\N	\N	\N	FABRICAÇÃO DE PRODUTOS TÊXTEIS	7399	8
373	C	13	13.1	\N	\N	Preparação e fiação de fibras têxteis	7399	8
374	C	13	13.1	13.11-1	\N	Preparação e fiação de fibras de algodão	7399	8
376	C	13	13.1	13.12-0	\N	Preparação e fiação de fibras têxteis naturais, exceto algodão	7399	8
378	C	13	13.1	13.13-8	\N	Fiação de fibras artificiais e sintéticas	7399	8
380	C	13	13.1	13.14-6	\N	Fabricação de linhas para costurar e bordar	7399	8
382	C	13	13.2	\N	\N	Tecelagem, exceto malha	7399	8
383	C	13	13.2	13.21-9	\N	Tecelagem de fios de algodão	7399	8
387	C	13	13.2	13.23-5	\N	Tecelagem de fios de fibras artificiais e sintéticas	7399	8
389	C	13	13.3	\N	\N	Fabricação de tecidos de malha	7399	8
390	C	13	13.3	13.30-8	\N	Fabricação de tecidos de malha	7399	8
392	C	13	13.4	\N	\N	Acabamentos em fios, tecidos e artefatos têxteis	7399	8
393	C	13	13.4	13.40-5	\N	Acabamentos em fios, tecidos e artefatos têxteis	7399	8
397	C	13	13.5	\N	\N	Fabricação de artefatos têxteis, exceto vestuário	7399	8
398	C	13	13.5	13.51-1	\N	Fabricação de artefatos têxteis para uso doméstico	7399	8
400	C	13	13.5	13.52-9	\N	Fabricação de artefatos de tapeçaria	7399	8
402	C	13	13.5	13.53-7	\N	Fabricação de artefatos de cordoaria	7399	8
404	C	13	13.5	13.54-5	\N	Fabricação de tecidos especiais, inclusive artefatos	7399	8
408	C	14	\N	\N	\N	CONFECÇÃO DE ARTIGOS DO VESTUÁRIO E ACESSÓRIOS	7399	8
409	C	14	14.1	\N	\N	Confecção de artigos do vestuário e acessórios	7399	8
410	C	14	14.1	14.11-8	\N	Confecção de roupas íntimas	7399	8
413	C	14	14.1	14.12-6	\N	Confecção de peças do vestuário, exceto roupas íntimas	7399	8
417	C	14	14.1	14.13-4	\N	Confecção de roupas profissionais	7399	8
421	C	14	14.1	14.14-2	\N	Fabricação de acessórios do vestuário, exceto para segurança e proteção	7399	8
423	C	14	14.2	\N	\N	Fabricação de artigos de malharia e tricotagem	7399	8
424	C	14	14.2	14.21-5	\N	Fabricação de meias	7399	8
426	C	14	14.2	14.22-3	\N	Fabricação de artigos do vestuário, produzidos em malharias e tricotagens, exceto meias	7399	8
428	C	15	\N	\N	\N	PREPARAÇÃO DE COUROS E FABRICAÇÃO DE ARTEFATOS DE COURO, ARTIGOS PARA VIAGEM E CALÇADOS	7399	8
429	C	15	15.1	\N	\N	Curtimento e outras preparações de couro	7399	8
430	C	15	15.1	15.10-6	\N	Curtimento e outras preparações de couro	7399	8
432	C	15	15.2	\N	\N	Fabricação de artigos para viagem e de artefatos diversos de couro	7399	8
435	C	15	15.2	15.29-7	\N	Fabricação de artefatos de couro não especificados anteriormente	7399	8
437	C	15	15.3	\N	\N	Fabricação de calçados	7399	8
438	C	15	15.3	15.31-9	\N	Fabricação de calçados de couro	7399	8
441	C	15	15.3	15.32-7	\N	Fabricação de tênis de qualquer material	7399	8
443	C	15	15.3	15.33-5	\N	Fabricação de calçados de material sintético	7399	8
445	C	15	15.3	15.39-4	\N	Fabricação de calçados de materiais não especificados anteriormente	7399	8
447	C	15	15.4	\N	\N	Fabricação de partes para calçados, de qualquer material	7399	8
448	C	15	15.4	15.40-8	\N	Fabricação de partes para calçados, de qualquer material	7399	8
450	C	16	\N	\N	\N	FABRICAÇÃO DE PRODUTOS DE MADEIRA	7399	8
451	C	16	16.1	\N	\N	Desdobramento de madeira	7399	8
452	C	16	16.1	16.10-2	\N	Desdobramento de madeira	7399	8
455	C	16	16.2	\N	\N	Fabricação de produtos de madeira, cortiça e material trançado, exceto móveis	7399	8
458	C	16	16.2	16.22-6	\N	Fabricação de estruturas de madeira e de artigos de carpintaria para construção	7399	8
462	C	16	16.2	16.23-4	\N	Fabricação de artefatos de tanoaria e de embalagens de madeira	7399	8
464	C	16	16.2	16.29-3	\N	Fabricação de artefatos de madeira, palha, cortiça, vime e material trançado não especificados anteriormente, exceto móveis	7399	8
467	C	17	\N	\N	\N	FABRICAÇÃO DE CELULOSE, PAPEL E PRODUTOS DE PAPEL	7399	8
468	C	17	17.1	\N	\N	Fabricação de celulose e outras pastas para a fabricação de papel	7399	8
469	C	17	17.1	17.10-9	\N	Fabricação de celulose e outras pastas para a fabricação de papel	7399	8
471	C	17	17.2	\N	\N	Fabricação de papel, cartolina e papel-cartão	7399	8
472	C	17	17.2	17.21-4	\N	Fabricação de papel	7399	8
474	C	17	17.2	17.22-2	\N	Fabricação de cartolina e papel-cartão	7399	8
476	C	17	17.3	\N	\N	Fabricação de embalagens de papel, cartolina, papel-cartão e papelão ondulado	7399	8
477	C	17	17.3	17.31-1	\N	Fabricação de embalagens de papel	7399	8
479	C	17	17.3	17.32-0	\N	Fabricação de embalagens de cartolina e papel-cartão	7399	8
481	C	17	17.3	17.33-8	\N	Fabricação de chapas e de embalagens de papelão ondulado	7399	8
487	C	17	17.4	17.42-7	\N	Fabricação de produtos de papel para usos doméstico e higiênico-sanitário	7399	8
490	C	23	23.4	23.41-9	\N	Fabricação de produtos cerâmicos refratários	7399	8
492	C	17	17.4	17.49-4	\N	Fabricação de produtos de pastas celulósicas, papel, cartolina, papel-cartão e papelão ondulado não especificados anteriormente	7399	8
494	C	18	\N	\N	\N	IMPRESSÃO E REPRODUÇÃO DE GRAVAÇÕES	7399	8
495	C	18	18.1	\N	\N	Atividade de impressão	7399	8
496	C	18	18.1	18.11-3	\N	Impressão de jornais, livros, revistas e outras publicações periódicas	7399	8
499	C	18	18.1	18.12-1	\N	Impressão de material de segurança	7399	8
501	C	18	18.1	18.13-0	\N	Impressão de materiais para outros usos	7399	8
504	C	18	18.2	\N	\N	Serviços de pré-impressão e acabamentos gráficos	7399	8
505	C	18	18.2	18.21-1	\N	Serviços de pré-impressão	7399	8
507	C	18	18.2	18.22-9	\N	Serviços de acabamentos gráficos	7399	8
509	C	18	18.3	\N	\N	Reprodução de materiais gravados em qualquer suporte	7399	8
510	C	18	18.3	18.30-0	\N	Reprodução de materiais gravados em qualquer suporte	7399	8
514	C	19	\N	\N	\N	FABRICAÇÃO DE COQUE, DE PRODUTOS DERIVADOS DO PETRÓLEO E DE BIOCOMBUSTÍVEIS	7399	8
515	C	19	19.1	\N	\N	Coquerias	7399	8
516	C	19	19.1	19.10-1	\N	Coquerias	7399	8
518	C	19	19.2	\N	\N	Fabricação de produtos derivados do petróleo	7399	8
519	C	19	19.2	19.21-7	\N	Fabricação de produtos do refino de petróleo	7399	8
521	C	19	19.2	19.22-5	\N	Fabricação de produtos derivados do petróleo, exceto produtos do refino	7399	8
525	C	19	19.3	\N	\N	Fabricação de biocombustíveis	7399	8
526	C	19	19.3	19.31-4	\N	Fabricação de álcool	7399	8
528	C	19	19.3	19.32-2	\N	Fabricação de biocombustíveis, exceto álcool	7399	8
530	C	20	\N	\N	\N	FABRICAÇÃO DE PRODUTOS QUÍMICOS	7399	8
531	C	20	20.1	\N	\N	Fabricação de produtos químicos inorgânicos	7399	8
532	C	20	20.1	20.11-8	\N	Fabricação de cloro e álcalis	7399	8
536	C	20	20.1	20.13-4	\N	Fabricação de adubos e fertilizantes	7399	8
538	C	20	20.1	20.14-2	\N	Fabricação de gases industriais	7399	8
540	C	20	20.1	20.19-3	\N	Fabricação de produtos químicos inorgânicos não especificados anteriormente	7399	8
543	C	20	20.2	\N	\N	Fabricação de produtos químicos orgânicos	7399	8
544	C	20	20.2	20.21-5	\N	Fabricação de produtos petroquímicos básicos	7399	8
546	C	20	20.2	20.22-3	\N	Fabricação de intermediários para plastificantes, resinas e fibras	7399	8
548	C	20	20.2	20.29-1	\N	Fabricação de produtos químicos orgânicos não especificados anteriormente	7399	8
550	C	20	20.3	\N	\N	Fabricação de resinas e elastômeros	7399	8
551	C	20	20.3	20.31-2	\N	Fabricação de resinas termoplásticas	7399	8
553	C	20	20.3	20.32-1	\N	Fabricação de resinas termofixas	7399	8
555	C	20	20.3	20.33-9	\N	Fabricação de elastômeros	7399	8
558	C	20	20.4	20.40-1	\N	Fabricação de fibras artificiais e sintéticas	7399	8
560	C	20	20.5	\N	\N	Fabricação de defensivos agrícolas e desinfestantes domissanitários	7399	8
561	C	20	20.5	20.51-7	\N	Fabricação de defensivos agrícolas	7399	8
563	C	20	20.5	20.52-5	\N	Fabricação de desinfestantes domissanitários	7399	8
566	C	20	20.6	20.61-4	\N	Fabricação de sabões e detergentes sintéticos	7399	8
568	C	20	20.6	20.62-2	\N	Fabricação de produtos de limpeza e polimento	7399	8
570	C	28	28.4	28.40-2	\N	Fabricação de máquinas-ferramenta	7399	8
571	C	20	20.6	20.63-1	\N	Fabricação de cosméticos, produtos de perfumaria e de higiene pessoal	7399	8
573	C	20	20.7	\N	\N	Fabricação de tintas, vernizes, esmaltes, lacas e produtos afins	7399	8
574	C	20	20.7	20.71-1	\N	Fabricação de tintas, vernizes, esmaltes e lacas	7399	8
576	C	20	20.7	20.72-0	\N	Fabricação de tintas de impressão	7399	8
578	C	20	20.7	20.73-8	\N	Fabricação de impermeabilizantes, solventes e produtos afins	7399	8
580	C	20	20.9	\N	\N	Fabricação de produtos e preparados químicos diversos	7399	8
581	C	20	20.9	20.91-6	\N	Fabricação de adesivos e selantes	7399	8
583	C	20	20.9	20.92-4	\N	Fabricação de explosivos	7399	8
587	C	20	20.9	20.93-2	\N	Fabricação de aditivos de uso industrial	7399	8
589	C	20	20.9	20.94-1	\N	Fabricação de catalisadores	7399	8
591	C	20	20.9	20.99-1	\N	Fabricação de produtos químicos não especificados anteriormente	7399	8
594	C	21	\N	\N	\N	FABRICAÇÃO DE PRODUTOS FARMOQUÍMICOS E FARMACÊUTICOS	7399	8
595	C	21	21.1	\N	\N	Fabricação de produtos farmoquímicos	7399	8
596	C	21	21.1	21.10-6	\N	Fabricação de produtos farmoquímicos	7399	8
598	C	21	21.2	\N	\N	Fabricação de produtos farmacêuticos	7399	8
599	C	21	21.2	21.21-1	\N	Fabricação de medicamentos para uso humano	7399	8
603	C	21	21.2	21.22-0	\N	Fabricação de medicamentos para uso veterinário	7399	8
605	C	21	21.2	21.23-8	\N	Fabricação de preparações farmacêuticas	7399	8
607	C	22	\N	\N	\N	FABRICAÇÃO DE PRODUTOS DE BORRACHA E DE MATERIAL PLÁSTICO	7399	8
608	C	22	22.1	\N	\N	Fabricação de produtos de borracha	7399	8
611	C	22	22.1	22.12-9	\N	Reforma de pneumáticos usados	7399	8
613	C	22	22.1	22.19-6	\N	Fabricação de artefatos de borracha não especificados anteriormente	7399	8
615	C	22	22.2	\N	\N	Fabricação de produtos de material plástico	7399	8
616	C	22	22.2	22.21-8	\N	Fabricação de laminados planos e tubulares de material plástico	7399	8
2363	T	97	97.0	\N	\N	Serviços domésticos	7399	8
618	C	22	22.2	22.22-6	\N	Fabricação de embalagens de material plástico	7399	8
620	C	22	22.2	22.23-4	\N	Fabricação de tubos e acessórios de material plástico para uso na construção	7399	8
622	C	22	22.2	22.29-3	\N	Fabricação de artefatos de material plástico não especificados anteriormente	7399	8
627	C	23	\N	\N	\N	FABRICAÇÃO DE PRODUTOS DE MINERAIS NÃO-METÁLICOS	7399	8
628	C	23	23.1	\N	\N	Fabricação de vidro e de produtos do vidro	7399	8
629	C	23	23.1	23.11-7	\N	Fabricação de vidro plano e de segurança	7399	8
631	C	23	23.1	23.12-5	\N	Fabricação de embalagens de vidro	7399	8
635	C	23	23.2	\N	\N	Fabricação de cimento	7399	8
636	C	23	23.2	23.20-6	\N	Fabricação de cimento	7399	8
638	C	23	23.3	\N	\N	Fabricação de artefatos de concreto, cimento, fibrocimento, gesso e materiais semelhantes	7399	8
639	C	23	23.3	23.30-3	\N	Fabricação de artefatos de concreto, cimento, fibrocimento, gesso e materiais semelhantes	7399	8
646	C	23	23.4	\N	\N	Fabricação de produtos cerâmicos	7399	8
651	C	23	23.4	23.49-4	\N	Fabricação de produtos cerâmicos não-refratários não especificados anteriormente	7399	8
654	C	23	23.9	\N	\N	Aparelhamento de pedras e fabricação de outros produtos de minerais não-metálicos	7399	8
655	C	23	23.9	23.91-5	\N	Aparelhamento e outros trabalhos em pedras	7399	8
659	C	23	23.9	23.92-3	\N	Fabricação de cal e gesso	7399	8
661	C	23	23.9	23.99-1	\N	Fabricação de produtos de minerais não-metálicos não especificados anteriormente	7399	8
664	C	24	\N	\N	\N	METALURGIA	7399	8
665	C	24	24.1	\N	\N	Produção de ferro-gusa e de ferroligas	7399	8
666	C	24	24.1	24.11-3	\N	Produção de ferro-gusa	7399	8
668	C	24	24.1	24.12-1	\N	Produção de ferroligas	7399	8
670	C	24	24.2	\N	\N	Siderurgia	7399	8
671	C	24	24.2	24.21-1	\N	Produção de semi-acabados de aço	7399	8
673	C	24	24.2	24.22-9	\N	Produção de laminados planos de aço	7399	8
676	C	24	24.2	24.23-7	\N	Produção de laminados longos de aço	7399	8
679	C	24	24.2	24.24-5	\N	Produção de relaminados, trefilados e perfilados de aço	7399	8
682	C	24	24.3	\N	\N	Produção de tubos de aço, exceto tubos sem costura	7399	8
683	C	24	24.3	24.31-8	\N	Produção de tubos de aço com costura	7399	8
685	C	24	24.3	24.39-3	\N	Produção de outros tubos de ferro e aço	7399	8
687	C	24	24.4	\N	\N	Metalurgia dos metais não-ferrosos	7399	8
691	C	24	24.4	24.42-3	\N	Metalurgia dos metais preciosos	7399	8
693	C	24	24.4	24.43-1	\N	Metalurgia do cobre	7399	8
695	C	24	24.4	24.49-1	\N	Metalurgia dos metais não-ferrosos e suas ligas não especificados anteriormente	7399	8
700	C	24	24.5	\N	\N	Fundição	7399	8
701	C	24	24.5	24.51-2	\N	Fundição de ferro e aço	7399	8
703	C	24	24.5	24.52-1	\N	Fundição de metais não-ferrosos e suas ligas	7399	8
705	C	25	\N	\N	\N	FABRICAÇÃO DE PRODUTOS DE METAL, EXCETO MÁQUINAS E EQUIPAMENTOS	7399	8
706	C	25	25.1	\N	\N	Fabricação de estruturas metálicas e obras de caldeiraria pesada	7399	8
707	C	25	25.1	25.11-0	\N	Fabricação de estruturas metálicas	7399	8
709	C	25	25.1	25.12-8	\N	Fabricação de esquadrias de metal	7399	8
713	C	25	25.2	\N	\N	Fabricação de tanques, reservatórios metálicos e caldeiras	7399	8
714	C	25	25.2	25.21-7	\N	Fabricação de tanques, reservatórios metálicos e caldeiras para aquecimento central	7399	8
716	C	25	25.2	25.22-5	\N	Fabricação de caldeiras geradoras de vapor, exceto para aquecimento central e para veículos	7399	8
718	C	25	25.3	\N	\N	Forjaria, estamparia, metalurgia do pó e serviços de tratamento de metais	7399	8
719	C	25	25.3	25.31-4	\N	Produção de forjados de aço e de metais não-ferrosos e suas ligas	7399	8
722	C	25	25.3	25.32-2	\N	Produção de artefatos estampados de metal; metalurgia do pó	7399	8
1405	G	47	\N	\N	\N	COMÉRCIO VAREJISTA	7399	8
725	C	25	25.3	25.39-0	\N	Serviços de usinagem, solda, tratamento e revestimento em metais	7399	8
727	C	25	25.4	\N	\N	Fabricação de artigos de cutelaria, de serralheria e ferramentas	7399	8
728	C	25	25.4	25.41-1	\N	Fabricação de artigos de cutelaria	7399	8
730	C	25	25.4	25.42-0	\N	Fabricação de artigos de serralheria, exceto esquadrias	7399	8
732	C	25	25.4	25.43-8	\N	Fabricação de ferramentas	7399	8
735	C	25	25.5	25.50-1	\N	Fabricação de equipamento bélico pesado, armas de fogo e munições	7399	8
738	C	25	25.9	\N	\N	Fabricação de produtos de metal não especificados anteriormente	7399	8
739	C	25	25.9	25.91-8	\N	Fabricação de embalagens metálicas	7399	8
741	C	25	25.9	25.92-6	\N	Fabricação de produtos de trefilados de metal	7399	8
744	C	25	25.9	25.93-4	\N	Fabricação de artigos de metal para uso doméstico e pessoal	7399	8
746	C	25	25.9	25.99-3	\N	Fabricação de produtos de metal não especificados anteriormente	7399	8
749	C	26	\N	\N	\N	FABRICAÇÃO DE EQUIPAMENTOS DE INFORMÁTICA, PRODUTOS ELETRÔNICOS E ÓPTICOS	7399	8
750	C	26	26.1	\N	\N	Fabricação de componentes eletrônicos	7399	8
751	C	26	26.1	26.10-8	\N	Fabricação de componentes eletrônicos	7399	8
753	C	26	26.2	\N	\N	Fabricação de equipamentos de informática e periféricos	7399	8
754	C	26	26.2	26.21-3	\N	Fabricação de equipamentos de informática	7399	8
758	C	26	26.3	\N	\N	Fabricação de equipamentos de comunicação	7399	8
759	C	26	26.3	26.31-1	\N	Fabricação de equipamentos transmissores de comunicação	7399	8
761	C	26	26.3	26.32-9	\N	Fabricação de aparelhos telefônicos e de outros equipamentos de comunicação	7399	8
763	C	26	26.4	\N	\N	Fabricação de aparelhos de recepção, reprodução, gravação e amplificação de áudio e vídeo	7399	8
764	C	26	26.4	26.40-0	\N	Fabricação de aparelhos de recepção, reprodução, gravação e amplificação de áudio e vídeo	7399	8
766	C	26	26.5	\N	\N	Fabricação de aparelhos e instrumentos de medida, teste e controle; cronômetros e relógios	7399	8
767	C	26	26.5	26.51-5	\N	Fabricação de aparelhos e equipamentos de medida, teste e controle	7399	8
769	C	26	26.5	26.52-3	\N	Fabricação de cronômetros e relógios	7399	8
771	C	26	26.6	\N	\N	Fabricação de aparelhos eletromédicos e eletroterapêuticos e equipamentos de irradiação	7399	8
772	C	26	26.6	26.60-4	\N	Fabricação de aparelhos eletromédicos e eletroterapêuticos e equipamentos de irradiação	7399	8
775	C	26	26.7	26.70-1	\N	Fabricação de equipamentos e instrumentos ópticos, fotográficos e cinematográficos	7399	8
778	C	26	26.8	\N	\N	Fabricação de mídias virgens, magnéticas e ópticas	7399	8
779	C	26	26.8	26.80-9	\N	Fabricação de mídias virgens, magnéticas e ópticas	7399	8
781	C	27	\N	\N	\N	FABRICAÇÃO DE MÁQUINAS, APARELHOS E MATERIAIS ELÉTRICOS	7399	8
782	C	27	27.1	\N	\N	Fabricação de geradores, transformadores e motores elétricos	7399	8
783	C	27	27.1	27.10-4	\N	Fabricação de geradores, transformadores e motores elétricos	7399	8
787	C	27	27.2	\N	\N	Fabricação de pilhas, baterias e acumuladores elétricos	7399	8
788	C	27	27.2	27.21-0	\N	Fabricação de pilhas, baterias e acumuladores elétricos, exceto para veículos automotores	7399	8
790	C	27	27.2	27.22-8	\N	Fabricação de baterias e acumuladores para veículos automotores	7399	8
793	C	27	27.3	\N	\N	Fabricação de equipamentos para distribuição e controle de energia elétrica	7399	8
794	C	27	27.3	27.31-7	\N	Fabricação de aparelhos e equipamentos para distribuição e controle de energia elétrica	7399	8
796	C	27	27.3	27.32-5	\N	Fabricação de material elétrico para instalações em circuito de consumo	7399	8
798	C	27	27.3	27.33-3	\N	Fabricação de fios, cabos e condutores elétricos isolados	7399	8
800	C	27	27.4	\N	\N	Fabricação de lâmpadas e outros equipamentos de iluminação	7399	8
801	C	27	27.4	27.40-6	\N	Fabricação de lâmpadas e outros equipamentos de iluminação	7399	8
804	C	27	27.5	\N	\N	Fabricação de eletrodomésticos	7399	8
807	C	27	27.5	27.59-7	\N	Fabricação de aparelhos eletrodomésticos não especificados anteriormente	7399	8
810	C	27	27.9	\N	\N	Fabricação de equipamentos e aparelhos elétricos não especificados anteriormente	7399	8
811	C	27	27.9	27.90-2	\N	Fabricação de equipamentos e aparelhos elétricos não especificados anteriormente	7399	8
815	C	28	\N	\N	\N	FABRICAÇÃO DE MÁQUINAS E EQUIPAMENTOS	7399	8
816	C	28	28.1	\N	\N	Fabricação de motores, bombas, compressores e equipamentos de transmissão	7399	8
817	C	28	28.1	28.11-9	\N	Fabricação de motores e turbinas, exceto para aviões e veículos rodoviários	7399	8
819	C	28	28.1	28.12-7	\N	Fabricação de equipamentos hidráulicos e pneumáticos, exceto válvulas	7399	8
821	C	28	28.1	28.13-5	\N	Fabricação de válvulas, registros e dispositivos semelhantes	7399	8
1406	G	47	47.1	\N	\N	Comércio varejista não-especializado	7399	8
829	C	28	28.2	\N	\N	Fabricação de máquinas e equipamentos de uso geral	7399	8
830	C	28	28.2	28.21-6	\N	Fabricação de aparelhos e equipamentos para instalações térmicas	7399	8
833	C	28	28.2	28.22-4	\N	Fabricação de máquinas, equipamentos e aparelhos para transporte e elevação de cargas e pessoas	7399	8
836	C	28	28.2	28.23-2	\N	Fabricação de máquinas e aparelhos de refrigeração e ventilação para uso industrial e comercial	7399	8
838	C	28	28.2	28.24-1	\N	Fabricação de aparelhos e equipamentos de ar condicionado	7399	8
841	C	28	28.2	28.25-9	\N	Fabricação de máquinas e equipamentos para saneamento básico e ambiental	7399	8
843	C	28	28.2	28.29-1	\N	Fabricação de máquinas e equipamentos de uso geral não especificados anteriormente	7399	8
846	C	28	28.3	\N	\N	Fabricação de tratores e de máquinas e equipamentos para a agricultura e pecuária	7399	8
847	C	28	28.3	28.31-3	\N	Fabricação de tratores agrícolas	7399	8
849	C	28	28.3	28.32-1	\N	Fabricação de equipamentos para irrigação agrícola	7399	8
853	C	28	28.4	\N	\N	Fabricação de máquinas-ferramenta	7399	8
855	C	28	28.5	\N	\N	Fabricação de máquinas e equipamentos de uso na extração mineral e na construção	7399	8
856	C	28	28.5	28.51-8	\N	Fabricação de máquinas e equipamentos para a prospecção e extração de petróleo	7399	8
858	C	28	28.5	28.52-6	\N	Fabricação de outras máquinas e equipamentos para uso na extração mineral, exceto na extração de petróleo	7399	8
860	C	28	28.5	28.53-4	\N	Fabricação de tratores, exceto agrícolas	7399	8
862	C	28	28.5	28.54-2	\N	Fabricação de máquinas e equipamentos para terraplenagem, pavimentação e construção, exceto tratores	7399	8
864	C	28	28.6	\N	\N	Fabricação de máquinas e equipamentos de uso industrial específico	7399	8
865	C	28	28.6	28.61-5	\N	Fabricação de máquinas para a indústria metalúrgica, exceto máquinas-ferramenta	7399	8
867	C	28	28.6	28.62-3	\N	Fabricação de máquinas e equipamentos para as indústrias de alimentos, bebidas e fumo	7399	8
869	C	28	28.6	28.63-1	\N	Fabricação de máquinas e equipamentos para a indústria têxtil	7399	8
871	C	28	28.6	28.64-0	\N	Fabricação de máquinas e equipamentos para as indústrias do vestuário, do couro e de calçados	7399	8
873	C	28	28.6	28.65-8	\N	Fabricação de máquinas e equipamentos para as indústrias de celulose, papel e papelão e artefatos	7399	8
875	C	28	28.6	28.66-6	\N	Fabricação de máquinas e equipamentos para a indústria do plástico	7399	8
879	C	29	\N	\N	\N	FABRICAÇÃO DE VEÍCULOS AUTOMOTORES, REBOQUES E CARROCERIAS	7399	8
880	C	29	29.1	\N	\N	Fabricação de automóveis, camionetas e utilitários	7399	8
881	C	29	29.1	29.10-7	\N	Fabricação de automóveis, camionetas e utilitários	7399	8
885	C	29	29.2	\N	\N	Fabricação de caminhões e ônibus	7399	8
886	C	29	29.2	29.20-4	\N	Fabricação de caminhões e ônibus	7399	8
889	C	29	29.3	\N	\N	Fabricação de cabines, carrocerias e reboques para veículos automotores	7399	8
890	C	29	29.3	29.30-1	\N	Fabricação de cabines, carrocerias e reboques para veículos automotores	7399	8
894	C	29	29.4	\N	\N	Fabricação de peças e acessórios para veículos automotores	7399	8
895	C	29	29.4	29.41-7	\N	Fabricação de peças e acessórios para o sistema motor de veículos automotores	7399	8
897	C	29	29.4	29.42-5	\N	Fabricação de peças e acessórios para os sistemas de marcha e transmissão de veículos automotores	7399	8
899	C	29	29.4	29.43-3	\N	Fabricação de peças e acessórios para o sistema de freios de veículos automotores	7399	8
901	C	29	29.4	29.44-1	\N	Fabricação de peças e acessórios para o sistema de direção e suspensão de veículos automotores	7399	8
903	C	29	29.4	29.45-0	\N	Fabricação de material elétrico e eletrônico para veículos automotores, exceto baterias	7399	8
905	C	29	29.4	29.49-2	\N	Fabricação de peças e acessórios para veículos automotores não especificados anteriormente	7399	8
908	C	29	29.5	\N	\N	Recondicionamento e recuperação de motores para veículos automotores	7399	8
909	C	29	29.5	29.50-6	\N	Recondicionamento e recuperação de motores para veículos automotores	7399	8
911	C	30	\N	\N	\N	FABRICAÇÃO DE OUTROS EQUIPAMENTOS DE TRANSPORTE, EXCETO VEÍCULOS AUTOMOTORES	7399	8
912	C	30	30.1	\N	\N	Construção de embarcações	7399	8
913	C	30	30.1	30.11-3	\N	Construção de embarcações e estruturas flutuantes	7399	8
918	C	30	30.2	\N	\N	Manutenção e reparação de embarcações	7399	8
919	C	30	30.2	30.21-1	\N	Manutenção e reparação de embarcações e estruturas flutuantes	7399	8
922	C	30	30.2	30.22-9	3022900	Manutenção e reparação de embarcações para esporte e lazer	7399	8
921	C	30	30.2	30.22-9	\N	Manutenção e reparação de embarcações para esporte e lazer	7399	8
923	C	30	30.3	\N	\N	Fabricação de veículos ferroviários	7399	8
1071	E	37	37.0	\N	\N	Esgoto e atividades relacionadas	7399	8
920	C	30	30.2	30.21-1	3021100	Manutenção e reparação de embarcações e estruturas flutuantes	7399	8
928	C	30	30.4	\N	\N	Fabricação de aeronaves	7399	8
929	C	30	30.4	30.41-5	\N	Fabricação de aeronaves	7399	8
931	C	30	30.4	30.42-3	\N	Fabricação de turbinas, motores e outros componentes e peças para aeronaves	7399	8
933	C	30	30.5	\N	\N	Fabricação de veículos militares de combate	7399	8
934	C	30	30.5	30.50-4	\N	Fabricação de veículos militares de combate	7399	8
936	C	30	30.9	\N	\N	Fabricação de equipamentos de transporte não especificados anteriormente	7399	8
937	C	30	30.9	30.91-1	\N	Fabricação de motocicletas	7399	8
939	C	30	30.9	30.92-0	\N	Fabricação de bicicletas e triciclos não-motorizados	7399	8
941	C	30	30.9	30.99-7	\N	Fabricação de equipamentos de transporte não especificados anteriormente	7399	8
943	C	31	\N	\N	\N	FABRICAÇÃO DE MÓVEIS	7399	8
944	C	31	31.0	\N	\N	Fabricação de móveis	7399	8
945	C	31	31.0	31.01-2	\N	Fabricação de móveis com predominância de madeira	7399	8
947	C	31	31.0	31.02-1	\N	Fabricação de móveis com predominância de metal	7399	8
951	C	31	31.0	31.04-7	\N	Fabricação de colchões	7399	8
953	C	32	\N	\N	\N	FABRICAÇÃO DE PRODUTOS DIVERSOS	7399	8
954	C	32	32.1	\N	\N	Fabricação de artigos de joalheria, bijuteria e semelhantes	7399	8
955	C	32	32.1	32.11-6	\N	Lapidação de gemas e fabricação de artefatos de ourivesaria e joalheria	7399	8
959	C	32	32.1	32.12-4	\N	Fabricação de bijuterias e artefatos semelhantes	7399	8
961	C	32	32.2	\N	\N	Fabricação de instrumentos musicais	7399	8
962	C	32	32.2	32.20-5	\N	Fabricação de instrumentos musicais	7399	8
964	C	32	32.3	\N	\N	Fabricação de artefatos para pesca e esporte	7399	8
965	C	32	32.3	32.30-2	\N	Fabricação de artefatos para pesca e esporte	7399	8
967	C	32	32.4	\N	\N	Fabricação de brinquedos e jogos recreativos	7399	8
968	C	32	32.4	32.40-0	\N	Fabricação de brinquedos e jogos recreativos	7399	8
974	C	32	32.5	32.50-7	\N	Fabricação de instrumentos e materiais para uso médico e odontológico e de artigos ópticos	7399	8
983	C	32	32.9	\N	\N	Fabricação de produtos diversos	7399	8
984	C	32	32.9	32.91-4	\N	Fabricação de escovas, pincéis e vassouras	7399	8
986	C	32	32.9	32.92-2	\N	Fabricação de equipamentos e acessórios para segurança e proteção pessoal e profissional	7399	8
989	C	32	32.9	32.99-0	\N	Fabricação de produtos diversos não especificados anteriormente	7399	8
996	C	33	\N	\N	\N	MANUTENÇÃO, REPARAÇÃO E INSTALAÇÃO DE MÁQUINAS E EQUIPAMENTOS	7399	8
997	C	33	33.1	\N	\N	Manutenção e reparação de máquinas e equipamentos	7399	8
998	C	33	33.1	33.11-2	\N	Manutenção e reparação de tanques, reservatórios metálicos e caldeiras, exceto para veículos	7399	8
1000	C	33	33.1	33.12-1	\N	Manutenção e reparação de equipamentos eletrônicos e ópticos	7399	8
1005	C	33	33.1	33.13-9	\N	Manutenção e reparação de máquinas e equipamentos elétricos	7399	8
1009	C	33	33.1	33.14-7	\N	Manutenção e reparação de máquinas e equipamentos da indústria mecânica	7399	8
1033	C	33	33.1	33.15-5	\N	Manutenção e reparação de veículos ferroviários	7399	8
1035	C	33	33.1	33.16-3	\N	Manutenção e reparação de aeronaves	7399	8
1040	C	33	33.2	\N	\N	Instalação de máquinas e equipamentos	7399	8
1041	C	33	33.2	33.21-0	\N	Instalação de máquinas e equipamentos industriais	7399	8
1043	C	33	33.2	33.29-5	\N	Instalação de equipamentos não especificados anteriormente	7399	8
1046	D	\N	\N	\N	\N	ELETRICIDADE E GÁS	7399	8
1047	D	35	\N	\N	\N	ELETRICIDADE, GÁS E OUTRAS UTILIDADES	7399	8
1048	D	35	35.1	\N	\N	Geração, transmissão e distribuição de energia elétrica	7399	8
1049	D	35	35.1	35.11-5	\N	Geração de energia elétrica	7399	8
1051	D	35	35.1	35.12-3	\N	Transmissão de energia elétrica	7399	8
1053	D	35	35.1	35.13-1	\N	Comércio atacadista de energia elétrica	7399	8
1055	D	35	35.1	35.14-0	\N	Distribuição de energia elétrica	7399	8
1057	D	35	35.2	\N	\N	Produção e distribuição de combustíveis gasosos por redes urbanas	7399	8
1061	D	35	35.3	\N	\N	Produção e distribuição de vapor, água quente e ar condicionado	7399	8
1062	D	35	35.3	35.30-1	\N	Produção e distribuição de vapor, água quente e ar condicionado	7399	8
1064	E	\N	\N	\N	\N	ÁGUA, ESGOTO, ATIVIDADES DE GESTÃO DE RESÍDUOS E DESCONTAMINAÇÃO	7399	8
1065	E	36	\N	\N	\N	CAPTAÇÃO, TRATAMENTO E DISTRIBUIÇÃO DE ÁGUA	7399	8
1066	E	36	36.0	\N	\N	Captação, tratamento e distribuição de água	7399	8
1067	E	36	36.0	36.00-6	\N	Captação, tratamento e distribuição de água	7399	8
1001	C	33	33.1	33.12-1	3312101	Manutenção e reparação de equipamentos transmissores de comunicação	7399	8
1076	E	38	\N	\N	\N	COLETA, TRATAMENTO E DISPOSIÇÃO DE RESÍDUOS; RECUPERAÇÃO DE MATERIAIS	7399	8
1077	E	38	38.1	\N	\N	Coleta de resíduos	7399	8
1078	E	38	38.1	38.11-4	\N	Coleta de resíduos não-perigosos	7399	8
1080	E	38	38.1	38.12-2	\N	Coleta de resíduos perigosos	7399	8
1082	E	38	38.2	\N	\N	Tratamento e disposição de resíduos	7399	8
1083	E	38	38.2	38.21-1	\N	Tratamento e disposição de resíduos não-perigosos	7399	8
1085	E	38	38.2	38.22-0	\N	Tratamento e disposição de resíduos perigosos	7399	8
1087	E	38	38.3	\N	\N	Recuperação de materiais	7399	8
1088	E	38	38.3	38.31-9	\N	Recuperação de materiais metálicos	7399	8
1091	E	38	38.3	38.32-7	\N	Recuperação de materiais plásticos	7399	8
1096	E	39	\N	\N	\N	DESCONTAMINAÇÃO E OUTROS SERVIÇOS DE GESTÃO DE RESÍDUOS	7399	8
1097	E	39	39.0	\N	\N	Descontaminação e outros serviços de gestão de resíduos	7399	8
1098	E	39	39.0	39.00-5	\N	Descontaminação e outros serviços de gestão de resíduos	7399	8
1100	F	\N	\N	\N	\N	CONSTRUÇÃO	7399	8
1101	F	41	\N	\N	\N	CONSTRUÇÃO DE EDIFÍCIOS	7399	8
1102	F	41	41.1	\N	\N	Incorporação de empreendimentos imobiliários	7399	8
1103	F	41	41.1	41.10-7	\N	Incorporação de empreendimentos imobiliários	7399	8
1105	F	41	41.2	\N	\N	Construção de edifícios	7399	8
1106	F	41	41.2	41.20-4	\N	Construção de edifícios	7399	8
1108	F	42	\N	\N	\N	OBRAS DE INFRA-ESTRUTURA	7399	8
1109	F	42	42.1	\N	\N	Construção de rodovias, ferrovias, obras urbanas e obras de arte especiais	7399	8
1110	F	42	42.1	42.11-1	\N	Construção de rodovias e ferrovias	7399	8
1113	F	42	42.1	42.12-0	\N	Construção de obras de arte especiais	7399	8
1117	F	42	42.2	\N	\N	Obras de infra-estrutura para energia elétrica, telecomunicações, água, esgoto e transporte por dutos	7399	8
1118	F	42	42.2	42.21-9	\N	Obras para geração e distribuição de energia elétrica e para telecomunicações	7399	8
1124	F	42	42.2	42.22-7	\N	Construção de redes de abastecimento de água, coleta de esgoto e construções correlatas	7399	8
1127	F	42	42.2	42.23-5	\N	Construção de redes de transportes por dutos, exceto para água e esgoto	7399	8
1129	F	42	42.9	\N	\N	Construção de outras obras de infra-estrutura	7399	8
1130	F	42	42.9	42.91-0	\N	Obras portuárias, marítimas e fluviais	7399	8
1132	F	42	42.9	42.92-8	\N	Montagem de instalações industriais e de estruturas metálicas	7399	8
1135	F	42	42.9	42.99-5	\N	Obras de engenharia civil não especificadas anteriormente	7399	8
1138	F	43	\N	\N	\N	SERVIÇOS ESPECIALIZADOS PARA CONSTRUÇÃO	7399	8
1139	F	43	43.1	\N	\N	Demolição e preparação do terreno	7399	8
1143	F	43	43.1	43.12-6	\N	Perfurações e sondagens	7399	8
1145	F	43	43.1	43.13-4	\N	Obras de terraplenagem	7399	8
1147	F	43	43.1	43.19-3	\N	Serviços de preparação do terreno não especificados anteriormente	7399	8
1149	F	43	43.2	\N	\N	Instalações elétricas, hidráulicas e outras instalações em construções	7399	8
1150	F	43	43.2	43.21-5	\N	Instalações elétricas	7399	8
1152	F	43	43.2	43.22-3	\N	Instalações hidráulicas, de sistemas de ventilação e refrigeração	7399	8
1156	F	43	43.2	43.29-1	\N	Obras de instalações em construções não especificadas anteriormente	7399	8
1163	F	43	43.3	\N	\N	Obras de acabamento	7399	8
1164	F	43	43.3	43.30-4	\N	Obras de acabamento	7399	8
1171	F	43	43.9	\N	\N	Outros serviços especializados para construção	7399	8
1172	F	43	43.9	43.91-6	\N	Obras de fundações	7399	8
1181	G	\N	\N	\N	\N	COMÉRCIO; REPARAÇÃO DE VEÍCULOS AUTOMOTORES E MOTOCICLETAS	7399	8
1182	G	45	\N	\N	\N	COMÉRCIO E REPARAÇÃO DE VEÍCULOS AUTOMOTORES E MOTOCICLETAS	7399	8
1183	G	45	45.1	\N	\N	Comércio de veículos automotores	7399	8
1184	G	45	45.1	45.11-1	\N	Comércio a varejo e por atacado de veículos automotores	7399	8
1191	G	45	45.1	45.12-9	\N	Representantes comerciais e agentes do comércio de veículos automotores	7399	8
1194	G	45	45.2	\N	\N	Manutenção e reparação de veículos automotores	7399	8
1195	G	45	45.2	45.20-0	\N	Manutenção e reparação de veículos automotores	7399	8
1202	G	46	46.3	46.31-1	\N	Comércio atacadista de leite e laticínios	7399	8
1204	G	45	45.3	\N	\N	Comércio de peças e acessórios para veículos automotores	7399	8
1205	G	45	45.3	45.30-7	\N	Comércio de peças e acessórios para veículos automotores	7399	8
1212	G	45	45.4	\N	\N	Comércio, manutenção e reparação de motocicletas, peças e acessórios	7399	8
1219	G	45	45.4	45.42-1	\N	Representantes comerciais e agentes do comércio de motocicletas, peças e acessórios	7399	8
1651	I	55	\N	\N	\N	ALOJAMENTO	7399	8
1225	G	46	46.1	\N	\N	Representantes comerciais e agentes do comércio, exceto de veículos automotores e motocicletas	7399	8
1226	G	46	46.1	46.11-7	\N	Representantes comerciais e agentes do comércio de matérias-primas agrícolas e animais vivos	7399	8
1228	G	46	46.1	46.12-5	\N	Representantes comerciais e agentes do comércio de combustíveis, minerais, produtos siderúrgicos e químicos	7399	8
1230	G	46	46.1	46.13-3	\N	Representantes comerciais e agentes do comércio de madeira, material de construção e ferragens	7399	8
1232	G	46	46.1	46.14-1	\N	Representantes comerciais e agentes do comércio de máquinas, equipamentos, embarcações e aeronaves	7399	8
1234	G	46	46.1	46.15-0	\N	Representantes comerciais e agentes do comércio de eletrodomésticos, móveis e artigos de uso doméstico	7399	8
1238	G	46	46.1	46.17-6	\N	Representantes comerciais e agentes do comércio de produtos alimentícios, bebidas e fumo	7399	8
1240	G	46	46.1	46.18-4	\N	Representantes comerciais e agentes do comércio especializado em produtos não especificados anteriormente	7399	8
1245	G	46	46.1	46.19-2	\N	Representantes comerciais e agentes do comércio de mercadorias em geral não especializado	7399	8
1247	G	46	46.2	\N	\N	Comércio atacadista de matérias-primas agrícolas e animais vivos	7399	8
1248	G	46	46.2	46.21-4	\N	Comércio atacadista de café em grão	7399	8
1250	G	46	46.2	46.22-2	\N	Comércio atacadista de soja	7399	8
1252	G	46	46.2	46.23-1	\N	Comércio atacadista de animais vivos, alimentos para animais e matérias-primas agrícolas, exceto café e soja	7399	8
1263	G	46	46.3	\N	\N	Comércio atacadista especializado em produtos alimentícios, bebidas e fumo	7399	8
1265	G	46	46.3	46.32-0	\N	Comércio atacadista de cereais e leguminosas beneficiados, farinhas, amidos e féculas	7399	8
1269	G	46	46.3	46.33-8	\N	Comércio atacadista de hortifrutigranjeiros	7399	8
1273	G	46	46.3	46.34-6	\N	Comércio atacadista de carnes, produtos da carne e pescado	7399	8
1278	G	46	46.3	46.35-4	\N	Comércio atacadista de bebidas	7399	8
1283	G	46	46.3	46.36-2	\N	Comércio atacadista de produtos do fumo	7399	8
1286	G	46	46.3	46.37-1	\N	Comércio atacadista especializado em produtos alimentícios não especificados anteriormente	7399	8
1295	G	46	46.3	46.39-7	\N	Comércio atacadista de produtos alimentícios em geral	7399	8
1298	G	46	46.4	\N	\N	Comércio atacadista de produtos de consumo não-alimentar	7399	8
1299	G	46	46.4	46.41-9	\N	Comércio atacadista de tecidos, artefatos de tecidos e de armarinho	7399	8
1303	G	46	46.4	46.42-7	\N	Comércio atacadista de artigos do vestuário e acessórios	7399	8
1306	G	46	46.4	46.43-5	\N	Comércio atacadista de calçados e artigos de viagem	7399	8
1312	G	46	46.4	46.45-1	\N	Comércio atacadista de instrumentos e materiais para uso médico, cirúrgico, ortopédico e odontológico	7399	8
1316	G	46	46.4	46.46-0	\N	Comércio atacadista de cosméticos, produtos de perfumaria e de higiene pessoal	7399	8
1319	G	46	46.4	46.47-8	\N	Comércio atacadista de artigos de escritório e de papelaria; livros, jornais e outras publicações	7399	8
1322	G	46	46.4	46.49-4	\N	Comércio atacadista de equipamentos e artigos de uso pessoal e doméstico não especificados anteriormente	7399	8
1335	G	46	46.5	\N	\N	Comércio atacadista de equipamentos e produtos de tecnologias de informação e comunicação	7399	8
1336	G	46	46.5	46.51-6	\N	Comércio atacadista de computadores, periféricos e suprimentos de informática	7399	8
1339	G	46	46.5	46.52-4	\N	Comércio atacadista de componentes eletrônicos e equipamentos de telefonia e comunicação	7399	8
1341	G	46	46.6	\N	\N	Comércio atacadista de máquinas, aparelhos e equipamentos, exceto de tecnologias de informação e comunicação	7399	8
1342	G	46	46.6	46.61-3	\N	Comércio atacadista de máquinas, aparelhos e equipamentos para uso agropecuário; partes e peças	7399	8
1346	G	46	46.6	46.63-0	\N	Comércio atacadista de máquinas e equipamentos para uso industrial; partes e peças	7399	8
1348	G	46	46.6	46.64-8	\N	Comércio atacadista de máquinas, aparelhos e equipamentos para uso odonto-médico-hospitalar; partes e peças	7399	8
1350	G	46	46.6	46.65-6	\N	Comércio atacadista de máquinas e equipamentos para uso comercial; partes e peças	7399	8
1352	G	46	46.6	46.69-9	\N	Comércio atacadista de máquinas, aparelhos e equipamentos não especificados anteriormente; partes e peças	7399	8
1355	G	46	46.7	\N	\N	Comércio atacadista de madeira, ferragens, ferramentas, material elétrico e material de construção	7399	8
1356	G	46	46.7	46.71-1	\N	Comércio atacadista de madeira e produtos derivados	7399	8
1358	G	46	46.7	46.72-9	\N	Comércio atacadista de ferragens e ferramentas	7399	8
1360	G	46	46.7	46.73-7	\N	Comércio atacadista de material elétrico	7399	8
1362	G	46	46.7	46.74-5	\N	Comércio atacadista de cimento	7399	8
1565	H	50	50.1	\N	\N	Transporte marítimo de cabotagem e longo curso	7399	8
1652	I	55	55.1	\N	\N	Hotéis e similares	7399	8
1941	M	72	\N	\N	\N	PESQUISA E DESENVOLVIMENTO CIENTÍFICO	7399	8
1338	G	46	46.5	46.51-6	4651602	Comércio atacadista de suprimentos para informática	7399	8
1370	G	46	46.8	\N	\N	Comércio atacadista especializado em outros produtos	7399	8
1371	G	46	46.8	46.81-8	\N	Comércio atacadista de combustíveis sólidos, líquidos e gasosos, exceto gás natural e GLP	7399	8
1377	G	46	46.8	46.82-6	\N	Comércio atacadista de gás liqüefeito de petróleo (GLP)	7399	8
1379	G	46	46.8	46.83-4	\N	Comércio atacadista de defensivos agrícolas, adubos, fertilizantes e corretivos do solo	7399	8
1381	G	46	46.8	46.84-2	\N	Comércio atacadista de produtos químicos e petroquímicos, exceto agroquímicos	7399	8
1385	G	46	46.8	46.85-1	\N	Comércio atacadista de produtos siderúrgicos e metalúrgicos, exceto para construção	7399	8
1387	G	46	46.8	46.86-9	\N	Comércio atacadista de papel e papelão em bruto e de embalagens	7399	8
1390	G	46	46.8	46.87-7	\N	Comércio atacadista de resíduos e sucatas	7399	8
1398	G	46	46.9	\N	\N	Comércio atacadista não-especializado	7399	8
1399	G	46	46.9	46.91-5	\N	Comércio atacadista de mercadorias em geral, com predominância de produtos alimentícios	7399	8
1401	G	46	46.9	46.92-3	\N	Comércio atacadista de mercadorias em geral, com predominância de insumos agropecuários	7399	8
1403	G	46	46.9	46.93-1	\N	Comércio atacadista de mercadorias em geral, sem predominância de alimentos ou de insumos agropecuários	7399	8
1407	G	47	47.1	47.11-3	\N	Comércio varejista de mercadorias em geral, com predominância de produtos alimentícios - hipermercados e supermercados	7399	8
1410	G	47	47.1	47.12-1	\N	Comércio varejista de mercadorias em geral, com predominância de produtos alimentícios - minimercados, mercearias e armazéns	7399	8
1412	G	47	47.1	47.13-0	\N	Comércio varejista de mercadorias em geral, sem predominância de produtos alimentícios	7399	8
1416	G	47	47.2	\N	\N	Comércio varejista de produtos alimentícios, bebidas e fumo	7399	8
1417	G	47	47.2	47.21-1	\N	Comércio varejista de produtos de padaria, laticínio, doces, balas e semelhantes	7399	8
1422	G	47	47.2	47.22-9	\N	Comércio varejista de carnes e pescados - açougues e peixarias	7399	8
1425	G	47	47.2	47.23-7	\N	Comércio varejista de bebidas	7399	8
1427	G	47	47.2	47.24-5	\N	Comércio varejista de hortifrutigranjeiros	7399	8
1432	G	47	47.3	\N	\N	Comércio varejista de combustíveis para veículos automotores	7399	8
1433	G	47	47.3	47.31-8	\N	Comércio varejista de combustíveis para veículos automotores	7399	8
1435	G	47	47.3	47.32-6	\N	Comércio varejista de lubrificantes	7399	8
1437	G	47	47.4	\N	\N	Comércio varejista de material de construção	7399	8
1438	G	47	47.4	47.41-5	\N	Comércio varejista de tintas e materiais para pintura	7399	8
1440	G	47	47.4	47.42-3	\N	Comércio varejista de material elétrico	7399	8
1442	G	47	47.4	47.43-1	\N	Comércio varejista de vidros	7399	8
1444	G	47	47.4	47.44-0	\N	Comércio varejista de ferragens, madeira e materiais de construção	7399	8
1451	G	47	47.5	\N	\N	Comércio varejista de equipamentos de informática e comunicação; equipamentos e artigos de uso doméstico	7399	8
1452	G	47	47.5	47.51-2	\N	Comércio varejista especializado de equipamentos e suprimentos de informática	7399	8
1454	G	47	47.5	47.52-1	\N	Comércio varejista especializado de equipamentos de telefonia e comunicação	7399	8
1456	G	47	47.5	47.53-9	\N	Comércio varejista especializado de eletrodomésticos e equipamentos de áudio e vídeo	7399	8
1458	G	47	47.5	47.54-7	\N	Comércio varejista especializado de móveis, colchoaria e artigos de iluminação	7399	8
1462	G	47	47.5	47.55-5	\N	Comércio varejista especializado de tecidos e artigos de cama, mesa e banho	7399	8
1466	G	47	47.5	47.56-3	\N	Comércio varejista especializado de instrumentos musicais e acessórios	7399	8
1470	G	47	47.5	47.59-8	\N	Comércio varejista de artigos de uso doméstico não especificados anteriormente	7399	8
1473	G	47	47.6	\N	\N	Comércio varejista de artigos culturais, recreativos e esportivos	7399	8
1474	G	47	47.6	47.61-0	\N	Comércio varejista de livros, jornais, revistas e papelaria	7399	8
1478	G	47	47.6	47.62-8	\N	Comércio varejista de discos, CDs, DVDs e fitas	7399	8
1480	G	47	47.6	47.63-6	\N	Comércio varejista de artigos recreativos e esportivos	7399	8
1486	G	47	47.7	\N	\N	Comércio varejista de produtos farmacêuticos, perfumaria e cosméticos e artigos médicos, ópticos e ortopédicos	7399	8
1487	G	47	47.7	47.71-7	\N	Comércio varejista de produtos farmacêuticos para uso humano e veterinário	7399	8
1492	G	47	47.7	47.72-5	\N	Comércio varejista de cosméticos, produtos de perfumaria e de higiene pessoal	7399	8
1494	G	47	47.7	47.73-3	\N	Comércio varejista de artigos médicos e ortopédicos	7399	8
1496	G	47	47.7	47.74-1	\N	Comércio varejista de artigos de óptica	7399	8
1498	G	47	47.8	\N	\N	Comércio varejista de produtos novos não especificados anteriormente e de produtos usados	7399	8
1653	I	55	55.1	55.10-8	\N	Hotéis e similares	7399	8
1488	G	47	47.7	47.71-7	4771701	Comércio varejista de produtos farmacêuticos, sem manipulação de fórmulas	7399	8
1501	G	47	47.8	47.82-2	\N	Comércio varejista de calçados e artigos de viagem	7399	8
1504	G	47	47.8	47.83-1	\N	Comércio varejista de jóias e relógios	7399	8
1507	G	47	47.8	47.84-9	\N	Comércio varejista de gás liqüefeito de petróleo (GLP)	7399	8
1509	G	47	47.8	47.85-7	\N	Comércio varejista de artigos usados	7399	8
1523	G	47	47.9	\N	\N	Comércio ambulante e outros tipos de comércio varejista	7399	8
1524	G	47	47.9	47.90-3	\N	Comércio ambulante e outros tipos de comércio varejista	7399	8
1525	H	\N	\N	\N	\N	TRANSPORTE, ARMAZENAGEM E CORREIO	7399	8
1526	H	49	\N	\N	\N	TRANSPORTE TERRESTRE	7399	8
1527	H	49	49.1	\N	\N	Transporte ferroviário e metroferroviário	7399	8
1528	H	49	49.1	49.11-6	\N	Transporte ferroviário de carga	7399	8
1529	H	49	49.1	49.12-4	\N	Transporte metroferroviário de passageiros	7399	8
1533	H	49	49.2	\N	\N	Transporte rodoviário de passageiros	7399	8
1534	H	49	49.2	49.21-3	\N	Transporte rodoviário coletivo de passageiros, com itinerário fixo, municipal e em região metropolitana	7399	8
1541	H	49	49.2	49.23-0	\N	Transporte rodoviário de táxi	7399	8
1544	H	49	49.2	49.24-8	\N	Transporte escolar	7399	8
1546	H	49	49.2	49.29-9	\N	Transporte rodoviário coletivo de passageiros, sob regime de fretamento, e outros transportes rodoviários não especificados anteriormente	7399	8
1552	H	49	49.3	\N	\N	Transporte rodoviário de carga	7399	8
1553	H	49	49.3	49.30-2	\N	Transporte rodoviário de carga	7399	8
1558	H	49	49.4	\N	\N	Transporte dutoviário	7399	8
1559	H	49	49.4	49.40-0	\N	Transporte dutoviário	7399	8
1561	H	49	49.5	\N	\N	Trens turísticos, teleféricos e similares	7399	8
1562	H	49	49.5	49.50-7	\N	Trens turísticos, teleféricos e similares	7399	8
1564	H	50	\N	\N	\N	TRANSPORTE AQUAVIÁRIO	7399	8
1566	H	50	50.1	50.11-4	\N	Transporte marítimo de cabotagem	7399	8
1569	H	50	50.1	50.12-2	\N	Transporte marítimo de longo curso	7399	8
1572	H	50	50.2	\N	\N	Transporte por navegação interior	7399	8
1573	H	50	50.2	50.21-1	\N	Transporte por navegação interior de carga	7399	8
1576	H	50	50.2	50.22-0	\N	Transporte por navegação interior de passageiros em linhas regulares	7399	8
1579	H	50	50.3	\N	\N	Navegação de apoio	7399	8
1580	H	50	50.3	50.30-1	\N	Navegação de apoio	7399	8
1583	H	50	50.9	\N	\N	Outros transportes aquaviários	7399	8
1584	H	50	50.9	50.91-2	\N	Transporte por navegação de travessia	7399	8
258	C	10	10.1	10.12-1	1012101	Abate de aves	7399	8
1587	H	50	50.9	50.99-8	\N	Transportes aquaviários não especificados anteriormente	7399	8
1590	H	51	\N	\N	\N	TRANSPORTE AÉREO	7399	8
1591	H	51	51.1	\N	\N	Transporte aéreo de passageiros	7399	8
1594	H	51	51.1	51.12-9	\N	Transporte aéreo de passageiros não-regular	7399	8
1597	H	51	51.2	\N	\N	Transporte aéreo de carga	7399	8
1598	H	51	51.2	51.20-0	\N	Transporte aéreo de carga	7399	8
1600	H	51	51.3	\N	\N	Transporte espacial	7399	8
1601	H	51	51.3	51.30-7	\N	Transporte espacial	7399	8
1603	H	52	\N	\N	\N	ARMAZENAMENTO E ATIVIDADES AUXILIARES DOS TRANSPORTES	7399	8
1604	H	52	52.1	\N	\N	Armazenamento, carga e descarga	7399	8
1605	H	52	52.1	52.11-7	\N	Armazenamento	7399	8
1609	H	52	52.1	52.12-5	\N	Carga e descarga	7399	8
1611	H	52	52.2	\N	\N	Atividades auxiliares dos transportes terrestres	7399	8
1614	H	52	52.2	52.22-2	\N	Terminais rodoviários e ferroviários	7399	8
1616	H	52	52.2	52.23-1	\N	Estacionamento de veículos	7399	8
1618	H	52	52.2	52.29-0	\N	Atividades auxiliares dos transportes terrestres não especificadas anteriormente	7399	8
1622	H	52	52.3	\N	\N	Atividades auxiliares dos transportes aquaviários	7399	8
1623	H	52	52.3	52.31-1	\N	Gestão de portos e terminais	7399	8
1626	H	52	52.3	52.32-0	\N	Atividades de agenciamento marítimo	7399	8
1628	H	52	52.3	52.39-7	\N	Atividades auxiliares dos transportes aquaviários não especificadas anteriormente	7399	8
1630	H	52	52.4	\N	\N	Atividades auxiliares dos transportes aéreos	7399	8
1631	H	52	52.4	52.40-1	\N	Atividades auxiliares dos transportes aéreos	7399	8
1634	H	52	52.5	\N	\N	Atividades relacionadas à organização do transporte de carga	7399	8
1641	H	53	\N	\N	\N	CORREIO E OUTRAS ATIVIDADES DE ENTREGA	7399	8
1642	H	53	53.1	\N	\N	Atividades de Correio	7399	8
1643	H	53	53.1	53.10-5	\N	Atividades de Correio	7399	8
1646	H	53	53.2	\N	\N	Atividades de malote e de entrega	7399	8
1647	H	53	53.2	53.20-2	\N	Atividades de malote e de entrega	7399	8
1650	I	\N	\N	\N	\N	ALOJAMENTO E ALIMENTAÇÃO	7399	8
1657	I	55	55.9	\N	\N	Outros tipos de alojamento não especificados anteriormente	7399	8
1658	I	55	55.9	55.90-6	\N	Outros tipos de alojamento não especificados anteriormente	7399	8
1663	I	56	\N	\N	\N	ALIMENTAÇÃO	7399	8
1664	I	56	56.1	\N	\N	Restaurantes e outros serviços de alimentação e bebidas	7399	8
1669	I	56	56.1	56.12-1	\N	Serviços ambulantes de alimentação	7399	8
1671	I	56	56.2	\N	\N	Serviços de catering, bufê e outros serviços de comida preparada	7399	8
1672	I	56	56.2	56.20-1	\N	Serviços de catering, bufê e outros serviços de comida preparada	7399	8
1677	J	\N	\N	\N	\N	INFORMAÇÃO E COMUNICAÇÃO	7399	8
1678	J	58	\N	\N	\N	EDIÇÃO E EDIÇÃO INTEGRADA À IMPRESSÃO	7399	8
1679	J	58	58.1	\N	\N	Edição de livros, jornais, revistas e outras atividades de edição	7399	8
1680	J	58	58.1	58.11-5	\N	Edição de livros	7399	8
1682	J	58	58.1	58.12-3	\N	Edição de jornais	7399	8
1684	J	58	58.1	58.13-1	\N	Edição de revistas	7399	8
1686	J	58	58.1	58.19-1	\N	Edição de cadastros, listas e de outros produtos gráficos	7399	8
1688	J	58	58.2	\N	\N	Edição integrada à impressão de livros, jornais, revistas e outras publicações	7399	8
1689	J	58	58.2	58.21-2	\N	Edição integrada à impressão de livros	7399	8
1691	J	58	58.2	58.22-1	\N	Edição integrada à impressão de jornais	7399	8
1693	J	58	58.2	58.23-9	\N	Edição integrada à impressão de revistas	7399	8
1697	J	59	\N	\N	\N	ATIVIDADES CINEMATOGRÁFICAS, PRODUÇÃO DE VÍDEOS E DE PROGRAMAS DE TELEVISÃO; GRAVAÇÃO DE SOM E EDIÇÃO DE MÚSICA	7399	8
1698	J	59	59.1	\N	\N	Atividades cinematográficas, produção de vídeos e de programas de televisão	7399	8
1699	J	59	59.1	59.11-1	\N	Atividades de produção cinematográfica, de vídeos e de programas de televisão	7399	8
1703	J	59	59.1	59.12-0	\N	Atividades de pós-produção cinematográfica, de vídeos e de programas de televisão	7399	8
1707	J	59	59.1	59.13-8	\N	Distribuição cinematográfica, de vídeo e de programas de televisão	7399	8
1709	J	59	59.1	59.14-6	\N	Atividades de exibição cinematográfica	7399	8
1711	J	59	59.2	\N	\N	Atividades de gravação de som e de edição de música	7399	8
1712	J	59	59.2	59.20-1	\N	Atividades de gravação de som e de edição de música	7399	8
1714	J	60	\N	\N	\N	ATIVIDADES DE RÁDIO E DE TELEVISÃO	7399	8
1715	J	60	60.1	\N	\N	Atividades de rádio	7399	8
1716	J	60	60.1	60.10-1	\N	Atividades de rádio	7399	8
1718	J	60	60.2	\N	\N	Atividades de televisão	7399	8
1719	J	60	60.2	60.21-7	\N	Atividades de televisão aberta	7399	8
1724	J	61	\N	\N	\N	TELECOMUNICAÇÕES	7399	8
1725	J	61	61.1	\N	\N	Telecomunicações por fio	7399	8
1726	J	61	61.1	61.10-8	\N	Telecomunicações por fio	7399	8
1731	J	61	61.2	\N	\N	Telecomunicações sem fio	7399	8
1732	J	61	61.2	61.20-5	\N	Telecomunicações sem fio	7399	8
1736	J	61	61.3	\N	\N	Telecomunicações por satélite	7399	8
1737	J	61	61.3	61.30-2	\N	Telecomunicações por satélite	7399	8
1739	J	61	61.4	\N	\N	Operadoras de televisão por assinatura	7399	8
1740	J	61	61.4	61.41-8	\N	Operadoras de televisão por assinatura por cabo	7399	8
1742	J	61	61.4	61.42-6	\N	Operadoras de televisão por assinatura por microondas	7399	8
1744	J	61	61.4	61.43-4	\N	Operadoras de televisão por assinatura por satélite	7399	8
1746	J	61	61.9	\N	\N	Outras atividades de telecomunicações	7399	8
1747	J	61	61.9	61.90-6	\N	Outras atividades de telecomunicações	7399	8
1751	J	62	\N	\N	\N	ATIVIDADES DOS SERVIÇOS DE TECNOLOGIA DA INFORMAÇÃO	7399	8
1753	J	62	62.0	62.01-5	\N	Desenvolvimento de programas de computador sob encomenda	7399	8
1755	J	62	62.0	62.02-3	\N	Desenvolvimento e licenciamento de programas de computador customizáveis	7399	8
1757	J	62	62.0	62.03-1	\N	Desenvolvimento e licenciamento de programas de computador não-customizáveis	7399	8
1759	J	62	62.0	62.04-0	\N	Consultoria em tecnologia da informação	7399	8
1761	J	62	62.0	62.09-1	\N	Suporte técnico, manutenção e outros serviços em tecnologia da informação	7399	8
1763	J	63	\N	\N	\N	ATIVIDADES DE PRESTAÇÃO DE SERVIÇOS DE INFORMAÇÃO	7399	8
1764	J	63	63.1	\N	\N	Tratamento de dados, hospedagem na internet e outras atividades relacionadas	7399	8
1767	J	63	63.1	63.19-4	\N	Portais, provedores de conteúdo e outros serviços de informação na internet	7399	8
1769	J	63	63.9	\N	\N	Outras atividades de prestação de serviços de informação	7399	8
1770	J	63	63.9	63.91-7	\N	Agências de notícias	7399	8
1772	J	63	63.9	63.99-2	\N	Outras atividades de prestação de serviços de informação não especificadas anteriormente	7399	8
1775	K	64	\N	\N	\N	ATIVIDADES DE SERVIÇOS FINANCEIROS	7399	8
1776	K	64	64.1	\N	\N	Banco Central	7399	8
1777	K	64	64.1	64.10-7	\N	Banco Central	7399	8
1780	K	64	64.2	64.21-2	\N	Bancos comerciais	7399	8
1782	K	64	64.2	64.22-1	\N	Bancos múltiplos, com carteira comercial	7399	8
1784	K	64	64.2	64.23-9	\N	Caixas econômicas	7399	8
1786	K	64	64.2	64.24-7	\N	Crédito cooperativo	7399	8
1791	K	64	64.3	\N	\N	Intermediação não-monetária - outros instrumentos de captação	7399	8
1792	K	64	64.3	64.31-0	\N	Bancos múltiplos, sem carteira comercial	7399	8
1794	K	64	64.3	64.32-8	\N	Bancos de investimento	7399	8
1796	K	64	64.3	64.33-6	\N	Bancos de desenvolvimento	7399	8
1798	K	64	64.3	64.34-4	\N	Agências de fomento	7399	8
1800	K	64	64.3	64.35-2	\N	Crédito imobiliário	7399	8
1806	K	64	64.3	64.37-9	\N	Sociedades de crédito ao microempreendedor	7399	8
1808	K	64	64.4	\N	\N	Arrendamento mercantil	7399	8
1809	K	64	64.4	64.40-9	\N	Arrendamento mercantil	7399	8
1811	K	64	64.5	\N	\N	Sociedades de capitalização	7399	8
1812	K	64	64.5	64.50-6	\N	Sociedades de capitalização	7399	8
1814	K	64	64.6	\N	\N	Atividades de sociedades de participação	7399	8
1815	K	64	64.6	64.61-1	\N	Holdings de instituições financeiras	7399	8
1817	K	64	64.6	64.62-0	\N	Holdings de instituições não-financeiras	7399	8
1819	K	64	64.6	64.63-8	\N	Outras sociedades de participação, exceto holdings	7399	8
1821	K	64	64.7	\N	\N	Fundos de investimento	7399	8
1822	K	64	64.7	64.70-1	\N	Fundos de investimento	7399	8
1827	K	64	64.9	64.91-3	\N	Sociedades de fomento mercantil - factoring	7399	8
1829	K	64	64.9	64.92-1	\N	Securitização de créditos	7399	8
1831	K	64	64.9	64.93-0	\N	Administração de consórcios para aquisição de bens e direitos	7399	8
1833	K	64	64.9	64.99-9	\N	Outras atividades de serviços financeiros não especificadas anteriormente	7399	8
1840	K	65	\N	\N	\N	SEGUROS, RESSEGUROS, PREVIDÊNCIA COMPLEMENTAR E PLANOS DE SAÚDE	7399	8
1841	K	65	65.1	\N	\N	Seguros de vida e não-vida	7399	8
1842	K	65	65.1	65.11-1	\N	Seguros de vida	7399	8
1845	K	65	65.1	65.12-0	\N	Seguros não-vida	7399	8
1847	K	65	65.2	\N	\N	Seguros-saúde	7399	8
1848	K	65	65.2	65.20-1	\N	Seguros-saúde	7399	8
1850	K	65	65.3	\N	\N	Resseguros	7399	8
1851	K	65	65.3	65.30-8	\N	Resseguros	7399	8
1853	K	65	65.4	\N	\N	Previdência complementar	7399	8
1856	R	\N	\N	\N	\N	ARTES, CULTURA, ESPORTE E RECREAÇÃO	7399	8
1857	K	65	65.4	65.42-1	\N	Previdência complementar aberta	7399	8
1859	K	65	65.5	\N	\N	Planos de saúde	7399	8
1860	K	65	65.5	65.50-2	\N	Planos de saúde	7399	8
1863	K	66	66.1	\N	\N	Atividades auxiliares dos serviços financeiros	7399	8
1864	K	66	66.1	66.11-8	\N	Administração de bolsas e mercados de balcão organizados	7399	8
1869	K	66	66.1	66.12-6	\N	Atividades de intermediários em transações de títulos, valores mobiliários e mercadorias	7399	8
1875	K	66	66.1	66.13-4	\N	Administração de cartões de crédito	7399	8
1877	K	66	66.1	66.19-3	\N	Atividades auxiliares dos serviços financeiros não especificadas anteriormente	7399	8
1884	K	66	66.2	\N	\N	Atividades auxiliares dos seguros, da previdência complementar e dos planos de saúde	7399	8
1885	K	66	66.2	66.21-5	\N	Avaliação de riscos e perdas	7399	8
1888	K	66	66.2	66.22-3	\N	Corretores e agentes de seguros, de planos de previdência complementar e de saúde	7399	8
1890	K	66	66.2	66.29-1	\N	Atividades auxiliares dos seguros, da previdência complementar e dos planos de saúde não especificadas anteriormente	7399	8
1892	K	66	66.3	\N	\N	Atividades de administração de fundos por contrato ou comissão	7399	8
1893	K	66	66.3	66.30-4	\N	Atividades de administração de fundos por contrato ou comissão	7399	8
1895	L	\N	\N	\N	\N	ATIVIDADES IMOBILIÁRIAS	7399	8
1896	L	68	\N	\N	\N	ATIVIDADES IMOBILIÁRIAS	7399	8
1897	L	68	68.1	\N	\N	Atividades imobiliárias de imóveis próprios	7399	8
1898	L	68	68.1	68.10-2	\N	Atividades imobiliárias de imóveis próprios	7399	8
1901	L	68	68.2	\N	\N	Atividades imobiliárias por contrato ou comissão	7399	8
1902	L	68	68.2	68.21-8	\N	Intermediação na compra, venda e aluguel de imóveis	7399	8
1907	M	\N	\N	\N	\N	ATIVIDADES PROFISSIONAIS, CIENTÍFICAS E TÉCNICAS	7399	8
1908	M	69	\N	\N	\N	ATIVIDADES JURÍDICAS, DE CONTABILIDADE E DE AUDITORIA	7399	8
1909	M	69	69.1	\N	\N	Atividades jurídicas	7399	8
1914	M	69	69.1	69.12-5	\N	Cartórios	7399	8
1917	M	69	69.2	69.20-6	\N	Atividades de contabilidade, consultoria e auditoria contábil e tributária	7399	8
1920	M	70	\N	\N	\N	ATIVIDADES DE SEDES DE EMPRESAS E DE CONSULTORIA EM GESTÃO EMPRESARIAL	7399	8
1921	M	70	70.1	\N	\N	Sedes de empresas e unidades administrativas locais	7399	8
1922	M	70	70.1	70.10-7	\N	Sedes de empresas e unidades administrativas locais	7399	8
1923	M	70	70.2	\N	\N	Atividades de consultoria em gestão empresarial	7399	8
1924	M	70	70.2	70.20-4	\N	Atividades de consultoria em gestão empresarial	7399	8
1926	M	71	\N	\N	\N	SERVIÇOS DE ARQUITETURA E ENGENHARIA; TESTES E ANÁLISES TÉCNICAS	7399	8
1927	M	71	71.1	\N	\N	Serviços de arquitetura e engenharia e atividades técnicas relacionadas	7399	8
1928	M	71	71.1	71.11-1	\N	Serviços de arquitetura	7399	8
1930	M	71	71.1	71.12-0	\N	Serviços de engenharia	7399	8
1932	M	71	71.1	71.19-7	\N	Atividades técnicas relacionadas à arquitetura e engenharia	7399	8
1938	M	71	71.2	\N	\N	Testes e análises técnicas	7399	8
1939	M	71	71.2	71.20-1	\N	Testes e análises técnicas	7399	8
1943	M	72	72.1	72.10-0	\N	Pesquisa e desenvolvimento experimental em ciências físicas e naturais	7399	8
1945	M	72	72.2	\N	\N	Pesquisa e desenvolvimento experimental em ciências sociais e humanas	7399	8
1946	M	72	72.2	72.20-7	\N	Pesquisa e desenvolvimento experimental em ciências sociais e humanas	7399	8
1948	M	73	\N	\N	\N	PUBLICIDADE E PESQUISA DE MERCADO	7399	8
1949	M	73	73.1	\N	\N	Publicidade	7399	8
1950	M	73	73.1	73.11-4	\N	Agências de publicidade	7399	8
1952	M	73	73.1	73.12-2	\N	Agenciamento de espaços para publicidade, exceto em veículos de comunicação	7399	8
1954	M	73	73.1	73.19-0	\N	Atividades de publicidade não especificadas anteriormente	7399	8
1960	M	73	73.2	\N	\N	Pesquisas de mercado e de opinião pública	7399	8
1961	M	73	73.2	73.20-3	\N	Pesquisas de mercado e de opinião pública	7399	8
1963	M	74	\N	\N	\N	OUTRAS ATIVIDADES PROFISSIONAIS, CIENTÍFICAS E TÉCNICAS	7399	8
1964	M	74	74.1	\N	\N	Design e decoração de interiores	7399	8
1965	M	74	74.1	74.10-2	\N	Design e decoração de interiores	7399	8
1968	M	74	74.2	\N	\N	Atividades fotográficas e similares	7399	8
1975	M	74	74.9	\N	\N	Atividades profissionais, científicas e técnicas não especificadas anteriormente	7399	8
1976	M	74	74.9	74.90-1	\N	Atividades profissionais, científicas e técnicas não especificadas anteriormente	7399	8
1983	M	75	\N	\N	\N	ATIVIDADES VETERINÁRIAS	7399	8
1984	M	75	75.0	\N	\N	Atividades veterinárias	7399	8
1985	M	75	75.0	75.00-1	\N	Atividades veterinárias	7399	8
1987	N	\N	\N	\N	\N	ATIVIDADES ADMINISTRATIVAS E SERVIÇOS COMPLEMENTARES	7399	8
1989	N	77	77.1	\N	\N	Locação de meios de transporte sem condutor	7399	8
1990	N	77	77.1	77.11-0	\N	Locação de automóveis sem condutor	7399	8
1992	N	77	77.1	77.19-5	\N	Locação de meios de transporte, exceto automóveis, sem condutor	7399	8
1996	N	77	77.2	\N	\N	Aluguel de objetos pessoais e domésticos	7399	8
1997	N	77	77.2	77.21-7	\N	Aluguel de equipamentos recreativos e esportivos	7399	8
1999	N	77	77.2	77.22-5	\N	Aluguel de fitas de vídeo, DVDs e similares	7399	8
2001	N	77	77.2	77.23-3	\N	Aluguel de objetos do vestuário, jóias e acessórios	7399	8
2127	P	85	85.1	\N	\N	Educação infantil e ensino fundamental	7399	8
2003	N	77	77.2	77.29-2	\N	Aluguel de objetos pessoais e domésticos não especificados anteriormente	7399	8
2008	N	77	77.3	\N	\N	Aluguel de máquinas e equipamentos sem operador	7399	8
2009	N	77	77.3	77.31-4	\N	Aluguel de máquinas e equipamentos agrícolas sem operador	7399	8
2011	N	77	77.3	77.32-2	\N	Aluguel de máquinas e equipamentos para construção sem operador	7399	8
2014	N	77	77.3	77.33-1	\N	Aluguel de máquinas e equipamentos para escritórios	7399	8
2021	N	77	77.4	\N	\N	Gestão de ativos intangíveis não-financeiros	7399	8
2022	N	77	77.4	77.40-3	\N	Gestão de ativos intangíveis não-financeiros	7399	8
2024	N	78	\N	\N	\N	SELEÇÃO, AGENCIAMENTO E LOCAÇÃO DE MÃO-DE-OBRA	7399	8
2025	N	78	78.1	\N	\N	Seleção e agenciamento de mão-de-obra	7399	8
2026	N	78	78.1	78.10-8	\N	Seleção e agenciamento de mão-de-obra	7399	8
2028	N	78	78.2	\N	\N	Locação de mão-de-obra temporária	7399	8
2029	N	78	78.2	78.20-5	\N	Locação de mão-de-obra temporária	7399	8
2031	N	78	78.3	\N	\N	Fornecimento e gestão de recursos humanos para terceiros	7399	8
2032	N	78	78.3	78.30-2	\N	Fornecimento e gestão de recursos humanos para terceiros	7399	8
2034	N	79	\N	\N	\N	AGÊNCIAS DE VIAGENS, OPERADORES TURÍSTICOS E SERVIÇOS DE RESERVAS	7399	8
2035	N	79	79.1	\N	\N	Agências de viagens e operadores turísticos	7399	8
2036	N	79	79.1	79.11-2	\N	Agências de viagens	7399	8
2041	N	79	79.9	79.90-2	\N	Serviços de reservas e outros serviços de turismo não especificados anteriormente	7399	8
2043	N	80	\N	\N	\N	ATIVIDADES DE VIGILÂNCIA, SEGURANÇA E INVESTIGAÇÃO	7399	8
2044	N	80	80.1	\N	\N	Atividades de vigilância, segurança privada e transporte de valores	7399	8
2045	N	80	80.1	80.11-1	\N	Atividades de vigilância e segurança privada	7399	8
2048	N	80	80.1	80.12-9	\N	Atividades de transporte de valores	7399	8
2050	N	80	80.2	\N	\N	Atividades de monitoramento de sistemas de segurança	7399	8
2051	N	80	80.2	80.20-0	\N	Atividades de monitoramento de sistemas de segurança	7399	8
2053	N	80	80.3	\N	\N	Atividades de investigação particular	7399	8
2054	N	80	80.3	80.30-7	\N	Atividades de investigação particular	7399	8
2056	N	81	\N	\N	\N	SERVIÇOS PARA EDIFÍCIOS E ATIVIDADES PAISAGÍSTICAS	7399	8
2057	N	81	81.1	\N	\N	Serviços combinados para apoio a edifícios	7399	8
2060	N	81	81.1	81.12-5	\N	Condomínios prediais	7399	8
2062	N	81	81.2	\N	\N	Atividades de limpeza	7399	8
2063	N	81	81.2	81.21-4	\N	Limpeza em prédios e em domicílios	7399	8
2065	N	81	81.2	81.22-2	\N	Imunização e controle de pragas urbanas	7399	8
2067	N	81	81.2	81.29-0	\N	Atividades de limpeza não especificadas anteriormente	7399	8
2069	N	81	81.3	\N	\N	Atividades paisagísticas	7399	8
2070	N	81	81.3	81.30-3	\N	Atividades paisagísticas	7399	8
2072	N	82	\N	\N	\N	SERVIÇOS DE ESCRITÓRIO, DE APOIO ADMINISTRATIVO E OUTROS SERVIÇOS PRESTADOS ÀS EMPRESAS	7399	8
2073	N	82	82.1	\N	\N	Serviços de escritório e apoio administrativo	7399	8
2074	N	82	82.1	82.11-3	\N	Serviços combinados de escritório e apoio administrativo	7399	8
2079	N	82	82.2	\N	\N	Atividades de teleatendimento	7399	8
2080	N	82	82.2	82.20-2	\N	Atividades de teleatendimento	7399	8
2082	N	82	82.3	\N	\N	Atividades de organização de eventos, exceto culturais e esportivos	7399	8
2083	N	82	82.3	82.30-0	\N	Atividades de organização de eventos, exceto culturais e esportivos	7399	8
2086	N	82	82.9	\N	\N	Outras atividades de serviços prestados principalmente às empresas	7399	8
2087	N	82	82.9	82.91-1	\N	Atividades de cobranças e informações cadastrais	7399	8
2089	N	82	82.9	82.92-0	\N	Envasamento e empacotamento sob contrato	7399	8
2091	N	82	82.9	82.99-7	\N	Atividades de serviços prestados principalmente às empresas não especificadas anteriormente	7399	8
2100	O	\N	\N	\N	\N	ADMINISTRAÇÃO PÚBLICA, DEFESA E SEGURIDADE SOCIAL	7399	8
2101	O	84	\N	\N	\N	ADMINISTRAÇÃO PÚBLICA, DEFESA E SEGURIDADE SOCIAL	7399	8
2102	O	84	84.1	\N	\N	Administração do estado e da política econômica e social	7399	8
2103	O	84	84.1	84.11-6	\N	Administração pública em geral	7399	8
2105	O	84	84.1	84.12-4	\N	Regulação das atividades de saúde, educação, serviços culturais e outros serviços sociais	7399	8
2107	O	84	84.1	84.13-2	\N	Regulação das atividades econômicas	7399	8
2109	O	84	84.1	84.14-1	\N	Atividades de suporte à administração pública	7399	8
2111	O	84	84.2	\N	\N	Serviços coletivos prestados pela administração pública	7399	8
2112	O	84	84.2	84.21-3	\N	Relações exteriores	7399	8
2114	O	84	84.2	84.22-1	\N	Defesa	7399	8
2116	O	84	84.2	84.23-0	\N	Justiça	7399	8
2120	O	84	84.2	84.25-6	\N	Defesa Civil	7399	8
2122	O	84	84.3	\N	\N	Seguridade social obrigatória	7399	8
2123	O	84	84.3	84.30-2	\N	Seguridade social obrigatória	7399	8
2126	P	85	\N	\N	\N	EDUCAÇÃO	7399	8
2128	P	85	85.1	85.11-2	\N	Educação infantil - creche	7399	8
2130	P	85	85.1	85.12-1	\N	Educação infantil - pré-escola	7399	8
2132	P	85	85.1	85.13-9	\N	Ensino fundamental	7399	8
2134	P	85	85.2	\N	\N	Ensino médio	7399	8
2137	P	85	85.3	\N	\N	Educação superior	7399	8
2138	P	85	85.3	85.31-7	\N	Educação superior - graduação	7399	8
2140	P	85	85.3	85.32-5	\N	Educação superior - graduação e pós-graduação	7399	8
2142	P	85	85.3	85.33-3	\N	Educação superior - pós-graduação e extensão	7399	8
2145	P	85	85.4	85.41-4	\N	Educação profissional de nível técnico	7399	8
2147	P	85	85.4	85.42-2	\N	Educação profissional de nível tecnológico	7399	8
2149	P	85	85.5	\N	\N	Serviços auxiliares à educação	7399	8
2150	P	85	85.5	85.50-3	\N	Serviços auxiliares à educação	7399	8
2153	P	85	85.9	\N	\N	Outras atividades de ensino	7399	8
2154	P	85	85.9	85.91-1	\N	Ensino de esportes	7399	8
2110	O	84	84.1	84.14-1	8414100	Atividades de suporte à administração pública	7399	8
2156	P	85	85.9	85.92-9	\N	Ensino de arte e cultura	7399	8
2161	P	85	85.9	85.93-7	\N	Ensino de idiomas	7399	8
2163	P	85	85.9	85.99-6	\N	Atividades de ensino não especificadas anteriormente	7399	8
2170	Q	\N	\N	\N	\N	SAÚDE HUMANA E SERVIÇOS SOCIAIS	7399	8
2171	Q	86	\N	\N	\N	ATIVIDADES DE ATENÇÃO À SAÚDE HUMANA	7399	8
2172	Q	86	86.1	\N	\N	Atividades de atendimento hospitalar	7399	8
2176	Q	86	86.2	\N	\N	Serviços móveis de atendimento a urgências e de remoção de pacientes	7399	8
2177	Q	86	86.2	86.21-6	\N	Serviços móveis de atendimento a urgências	7399	8
2180	Q	86	86.2	86.22-4	\N	Serviços de remoção de pacientes, exceto os serviços móveis de atendimento a urgências	7399	8
2182	Q	86	86.3	\N	\N	Atividades de atenção ambulatorial executadas por médicos e odontólogos	7399	8
2183	Q	86	86.3	86.30-5	\N	Atividades de atenção ambulatorial executadas por médicos e odontólogos	7399	8
2192	Q	86	86.4	\N	\N	Atividades de serviços de complementação diagnóstica e terapêutica	7399	8
2193	Q	86	86.4	86.40-2	\N	Atividades de serviços de complementação diagnóstica e terapêutica	7399	8
2209	Q	86	86.5	\N	\N	Atividades de profissionais da área de saúde, exceto médicos e odontólogos	7399	8
2210	Q	86	86.5	86.50-0	\N	Atividades de profissionais da área de saúde, exceto médicos e odontólogos	7399	8
2219	Q	86	86.6	\N	\N	Atividades de apoio à gestão de saúde	7399	8
2220	Q	86	86.6	86.60-7	\N	Atividades de apoio à gestão de saúde	7399	8
2222	Q	86	86.9	\N	\N	Atividades de atenção à saúde humana não especificadas anteriormente	7399	8
2223	Q	86	86.9	86.90-9	\N	Atividades de atenção à saúde humana não especificadas anteriormente	7399	8
2227	Q	87	\N	\N	\N	ATIVIDADES DE ATENÇÃO À SAÚDE HUMANA INTEGRADAS COM ASSISTÊNCIA SOCIAL, PRESTADAS EM RESIDÊNCIAS COLETIVAS E PARTICULARES	7399	8
2229	Q	87	87.1	87.11-5	\N	Atividades de assistência a idosos, deficientes físicos, imunodeprimidos e convalescentes prestadas em residências coletivas e particulares	7399	8
2235	Q	87	87.1	87.12-3	\N	Atividades de fornecimento de infra-estrutura de apoio e assistência a paciente no domicílio	7399	8
2237	Q	87	87.2	\N	\N	Atividades de assistência psicossocial e à saúde a portadores de distúrbios psíquicos, deficiência mental e dependência química	7399	8
2238	Q	87	87.2	87.20-4	\N	Atividades de assistência psicossocial e à saúde a portadores de distúrbios psíquicos, deficiência mental e dependência química	7399	8
2241	Q	87	87.3	\N	\N	Atividades de assistência social prestadas em residências coletivas e particulares	7399	8
2242	Q	87	87.3	87.30-1	\N	Atividades de assistência social prestadas em residências coletivas e particulares	7399	8
2246	Q	88	\N	\N	\N	SERVIÇOS DE ASSISTÊNCIA SOCIAL SEM ALOJAMENTO	7399	8
2247	Q	88	88.0	\N	\N	Serviços de assistência social sem alojamento	7399	8
2248	Q	88	88.0	88.00-6	\N	Serviços de assistência social sem alojamento	7399	8
2250	R	90	\N	\N	\N	ATIVIDADES ARTÍSTICAS, CRIATIVAS E DE ESPETÁCULOS	7399	8
2251	R	90	90.0	\N	\N	Atividades artísticas, criativas e de espetáculos	7399	8
2252	R	90	90.0	90.01-9	\N	Artes cênicas, espetáculos e atividades complementares	7399	8
2260	R	90	90.0	90.02-7	\N	Criação artística	7399	8
2263	R	90	90.0	90.03-5	\N	Gestão de espaços para artes cênicas, espetáculos e outras atividades artísticas	7399	8
2265	R	91	\N	\N	\N	ATIVIDADES LIGADAS AO PATRIMÔNIO CULTURAL E AMBIENTAL	7399	8
2266	R	91	91.0	\N	\N	Atividades ligadas ao patrimônio cultural e ambiental	7399	8
2267	R	91	91.0	91.01-5	\N	Atividades de bibliotecas e arquivos	7399	8
2269	R	91	91.0	91.02-3	\N	Atividades de museus e de exploração, restauração artística e conservação de lugares e prédios históricos e atrações similares	7399	8
2272	R	91	91.0	91.03-1	\N	Atividades de jardins botânicos, zoológicos, parques nacionais, reservas ecológicas e áreas de proteção ambiental	7399	8
2274	R	92	\N	\N	\N	ATIVIDADES DE EXPLORAÇÃO DE JOGOS DE AZAR E APOSTAS	7399	8
2275	R	92	92.0	\N	\N	Atividades de exploração de jogos de azar e apostas	7399	8
2276	R	92	92.0	92.00-3	\N	Atividades de exploração de jogos de azar e apostas	7399	8
2280	R	93	\N	\N	\N	ATIVIDADES ESPORTIVAS E DE RECREAÇÃO E LAZER	7399	8
2281	R	93	93.1	\N	\N	Atividades esportivas	7399	8
2282	R	93	93.1	93.11-5	\N	Gestão de instalações de esportes	7399	8
2284	R	93	93.1	93.12-3	\N	Clubes sociais, esportivos e similares	7399	8
2286	R	93	93.1	93.13-1	\N	Atividades de condicionamento físico	7399	8
2288	R	93	93.1	93.19-1	\N	Atividades esportivas não especificadas anteriormente	7399	8
2291	R	93	93.2	\N	\N	Atividades de recreação e lazer	7399	8
2294	R	93	93.2	93.29-8	\N	Atividades de recreação e lazer não especificadas anteriormente	7399	8
2300	S	\N	\N	\N	\N	OUTRAS ATIVIDADES DE SERVIÇOS	7399	8
2301	S	94	\N	\N	\N	ATIVIDADES DE ORGANIZAÇÕES ASSOCIATIVAS	7399	8
10	A	1	01.1	01.12-1	0112101	Cultivo de algodão herbáceo	7399	8
11	A	1	01.1	01.12-1	0112102	Cultivo de juta	7399	8
14	A	1	01.1	01.13-0	0113000	Cultivo de cana-de-açúcar	7399	8
16	A	1	01.1	01.14-8	0114800	Cultivo de fumo	7399	8
18	A	1	01.1	01.15-6	0115600	Cultivo de soja	7399	8
20	A	1	01.1	01.16-4	0116401	Cultivo de amendoim	7399	8
21	A	1	01.1	01.16-4	0116402	Cultivo de girassol	7399	8
22	A	1	01.1	01.16-4	0116403	Cultivo de mamona	7399	8
23	A	1	01.1	01.16-4	0116499	Cultivo de outras oleaginosas de lavoura temporária não especificadas anteriormente	7399	8
27	A	1	01.1	01.19-9	0119903	Cultivo de batata-inglesa	7399	8
28	A	1	01.1	01.19-9	0119904	Cultivo de cebola	7399	8
29	A	1	01.1	01.19-9	0119905	Cultivo de feijão	7399	8
30	A	1	01.1	01.19-9	0119906	Cultivo de mandioca	7399	8
31	A	1	01.1	01.19-9	0119907	Cultivo de melão	7399	8
32	A	1	01.1	01.19-9	0119908	Cultivo de melancia	7399	8
33	A	1	01.1	01.19-9	0119909	Cultivo de tomate rasteiro	7399	8
37	A	1	01.2	01.21-1	0121101	Horticultura, exceto morango	7399	8
38	A	1	01.2	01.21-1	0121102	Cultivo de morango	7399	8
40	A	1	01.2	01.22-9	0122900	Floricultura	7399	8
43	A	1	01.3	01.31-8	0131800	Cultivo de laranja	7399	8
45	A	1	01.3	01.32-6	0132600	Cultivo de uva	7399	8
47	A	1	01.3	01.33-4	0133401	Cultivo de açaí	7399	8
48	A	1	01.3	01.33-4	0133402	Cultivo de banana	7399	8
49	A	1	01.3	01.33-4	0133403	Cultivo de caju	7399	8
50	A	1	01.3	01.33-4	0133404	Cultivo de cítricos, exceto laranja	7399	8
51	A	1	01.3	01.33-4	0133405	Cultivo de coco-da-baía	7399	8
52	A	1	01.3	01.33-4	0133406	Cultivo de guaraná	7399	8
53	A	1	01.3	01.33-4	0133407	Cultivo de maçã	7399	8
54	A	1	01.3	01.33-4	0133408	Cultivo de mamão	7399	8
2303	S	94	94.1	94.11-1	\N	Atividades de organizações associativas patronais e empresariais	7399	8
2305	S	94	94.1	94.12-0	\N	Atividades de organizações associativas profissionais	7399	8
2307	S	94	94.2	\N	\N	Atividades de organizações sindicais	7399	8
2308	S	94	94.2	94.20-1	\N	Atividades de organizações sindicais	7399	8
2310	S	94	94.3	\N	\N	Atividades de associações de defesa de direitos sociais	7399	8
2311	S	94	94.3	94.30-8	\N	Atividades de associações de defesa de direitos sociais	7399	8
2313	S	94	94.9	\N	\N	Atividades de organizações associativas não especificadas anteriormente	7399	8
2314	S	94	94.9	94.91-0	\N	Atividades de organizações religiosas	7399	8
2316	S	94	94.9	94.92-8	\N	Atividades de organizações políticas	7399	8
2318	S	94	94.9	94.93-6	\N	Atividades de organizações associativas ligadas à cultura e à arte	7399	8
2320	S	94	94.9	94.99-5	\N	Atividades associativas não especificadas anteriormente	7399	8
2323	S	95	95.1	\N	\N	Reparação e manutenção de equipamentos de informática e comunicação	7399	8
2324	S	95	95.1	95.11-8	\N	Reparação e manutenção de computadores e de equipamentos periféricos	7399	8
2326	S	95	95.1	95.12-6	\N	Reparação e manutenção de equipamentos de comunicação	7399	8
2328	S	95	95.2	\N	\N	Reparação e manutenção de objetos e equipamentos pessoais e domésticos	7399	8
2329	S	95	95.2	95.21-5	\N	Reparação e manutenção de equipamentos eletroeletrônicos de uso pessoal e doméstico	7399	8
2331	S	95	95.2	95.29-1	\N	Reparação e manutenção de objetos e equipamentos pessoais e domésticos não especificados anteriormente	7399	8
2339	S	96	\N	\N	\N	OUTRAS ATIVIDADES DE SERVIÇOS PESSOAIS	7399	8
2340	S	96	96.0	\N	\N	Outras atividades de serviços pessoais	7399	8
2341	S	96	96.0	96.01-7	\N	Lavanderias, tinturarias e toalheiros	7399	8
2345	S	96	96.0	96.02-5	\N	Cabeleireiros e outras atividades de tratamento de beleza	7399	8
2348	S	96	96.0	96.03-3	\N	Atividades funerárias e serviços relacionados	7399	8
2355	S	96	96.0	96.09-2	\N	Atividades de serviços pessoais não especificadas anteriormente	7399	8
2361	T	\N	\N	\N	\N	SERVIÇOS DOMÉSTICOS	7399	8
2364	T	97	97.0	97.00-5	\N	Serviços domésticos	7399	8
2366	U	\N	\N	\N	\N	ORGANISMOS INTERNACIONAIS E OUTRAS INSTITUIÇÕES EXTRATERRITORIAIS	7399	8
2368	U	99	99.0	\N	\N	Organismos internacionais e outras instituições extraterritoriais	7399	8
2369	U	99	99.0	99.00-8	\N	Organismos internacionais e outras instituições extraterritoriais	7399	8
8	A	1	01.1	01.11-3	0111399	Cultivo de outros cereais não especificados anteriormente	7399	8
62	A	1	01.3	01.35-1	0135100	Cultivo de cacau	7399	8
64	A	1	01.3	01.39-3	0139301	Cultivo de chá-da-índia	7399	8
65	A	1	01.3	01.39-3	0139302	Cultivo de erva-mate	7399	8
66	A	1	01.3	01.39-3	0139303	Cultivo de pimenta-do-reino	7399	8
67	A	1	01.3	01.39-3	0139304	Cultivo de plantas para condimento, exceto pimenta-do-reino	7399	8
68	A	1	01.3	01.39-3	0139305	Cultivo de dendê	7399	8
69	A	1	01.3	01.39-3	0139306	Cultivo de seringueira	7399	8
73	A	1	01.4	01.41-5	0141501	Produção de sementes certificadas, exceto de forrageiras para pasto	7399	8
74	A	1	01.4	01.41-5	0141502	Produção de sementes certificadas de forrageiras para formação de pasto	7399	8
76	A	1	01.4	01.42-3	0142300	Produção de mudas e outras formas de propagação vegetal, certificadas	7399	8
79	A	1	01.5	01.51-2	0151201	Criação de bovinos para corte	7399	8
80	A	1	01.5	01.51-2	0151202	Criação de bovinos para leite	7399	8
81	A	1	01.5	01.51-2	0151203	Criação de bovinos, exceto para corte e leite	7399	8
83	A	1	01.5	01.52-1	0152101	Criação de bufalinos	7399	8
84	A	1	01.5	01.52-1	0152102	Criação de eqüinos	7399	8
85	A	1	01.5	01.52-1	0152103	Criação de asininos e muares	7399	8
87	A	1	01.5	01.53-9	0153901	Criação de caprinos	7399	8
88	A	1	01.5	01.53-9	0153902	Criação de ovinos, inclusive para produção de lã	7399	8
90	A	1	01.5	01.54-7	0154700	Criação de suínos	7399	8
92	A	1	01.5	01.55-5	0155501	Criação de frangos para corte	7399	8
93	A	1	01.5	01.55-5	0155502	Produção de pintos de um dia	7399	8
95	A	1	01.5	01.55-5	0155503	Criação de outros galináceos, exceto para corte	7399	8
96	A	1	01.5	01.55-5	0155504	Criação de aves, exceto galináceos	7399	8
97	A	1	01.5	01.55-5	0155505	Produção de ovos	7399	8
99	A	1	01.5	01.59-8	0159801	Apicultura	7399	8
101	A	1	01.5	01.59-8	0159803	Criação de escargô	7399	8
102	A	1	01.5	01.59-8	0159804	Criação de bicho-da-seda	7399	8
103	A	1	01.5	01.59-8	0159899	Criação de outros animais não especificados anteriormente	7399	8
106	A	1	01.6	01.61-0	0161001	Serviço de pulverização e controle de pragas agrícolas	7399	8
107	A	1	01.6	01.61-0	0161002	Serviço de poda de árvores para lavouras	7399	8
108	A	1	01.6	01.61-0	0161003	Serviço de preparação de terreno, cultivo e colheita	7399	8
111	A	1	01.6	01.62-8	0162801	Serviço de inseminação artificial de animais *	7399	8
112	A	1	01.6	01.62-8	0162802	Serviço de tosquiamento de ovinos	7399	8
113	A	1	01.6	01.62-8	0162803	Serviço de manejo de animais	7399	8
114	A	1	01.6	01.62-8	0162899	Atividades de apoio à pecuária não especificadas anteriormente	7399	8
116	A	1	01.6	01.63-6	0163600	Atividades de pós-colheita	7399	8
119	A	1	01.7	01.70-9	0170900	Caça e serviços relacionados	7399	8
123	A	2	02.1	02.10-1	0210101	Cultivo de eucalipto	7399	8
124	A	2	02.1	02.10-1	0210102	Cultivo de acácia-negra	7399	8
125	A	2	02.1	02.10-1	0210103	Cultivo de pinus	7399	8
126	A	2	02.1	02.10-1	0210104	Cultivo de teca	7399	8
128	A	2	02.1	02.10-1	0210106	Cultivo de mudas em viveiros florestais	7399	8
129	A	2	02.1	02.10-1	0210107	Extração de madeira em florestas plantadas	7399	8
130	A	2	02.1	02.10-1	0210108	Produção de carvão vegetal - florestas plantadas	7399	8
131	A	2	02.1	02.10-1	0210109	Produção de casca de acácia-negra - florestas plantadas	7399	8
132	A	2	02.1	02.10-1	0210199	Produção de produtos não-madeireiros não especificados anteriormente em florestas plantadas	7399	8
135	A	2	02.2	02.20-9	0220901	Extração de madeira em florestas nativas	7399	8
136	A	2	02.2	02.20-9	0220902	Produção de carvão vegetal - florestas nativas	7399	8
137	A	2	02.2	02.20-9	0220903	Coleta de castanha-do-pará em florestas nativas	7399	8
138	A	2	02.2	02.20-9	0220904	Coleta de látex em florestas nativas	7399	8
139	A	2	02.2	02.20-9	0220905	Coleta de palmito em florestas nativas	7399	8
140	A	2	02.2	02.20-9	0220906	Conservação de florestas nativas	7399	8
141	A	2	02.2	02.20-9	0220999	Coleta de produtos não-madeireiros não especificados anteriormente em florestas nativas	7399	8
144	A	2	02.3	02.30-6	0230600	Atividades de apoio à produção florestal	7399	8
148	A	3	03.1	03.11-6	0311601	Pesca de peixes em água salgada	7399	8
149	A	3	03.1	03.11-6	0311602	Pesca de crustáceos e moluscos em água salgada	7399	8
150	A	3	03.1	03.11-6	0311603	Coleta de outros produtos marinhos	7399	8
153	A	3	03.1	03.12-4	0312401	Pesca de peixes em água doce	7399	8
60	A	1	01.3	01.34-2	0134200	Cultivo de café	7399	8
156	A	3	03.1	03.12-4	0312404	Atividades de apoio à pesca em água doce	7399	8
159	A	3	03.2	03.21-3	0321301	Criação de peixes em água salgada e salobra	7399	8
160	A	3	03.2	03.21-3	0321302	Criação de camarões em água salgada e salobra	7399	8
162	A	3	03.2	03.21-3	0321304	Criação de peixes ornamentais em água salgada e salobra	7399	8
163	A	3	03.2	03.21-3	0321305	Atividades de apoio à aqüicultura em água salgada e salobra	7399	8
164	A	3	03.2	03.21-3	0321399	Cultivos e semicultivos da aqüicultura em água salgada e salobra não especificados anteriormente	7399	8
166	A	3	03.2	03.22-1	0322101	Criação de peixes em água doce	7399	8
167	A	3	03.2	03.22-1	0322102	Criação de camarões em água doce	7399	8
168	A	3	03.2	03.22-1	0322103	Criação de ostras e mexilhões em água doce	7399	8
169	A	3	03.2	03.22-1	0322104	Criação de peixes ornamentais em água doce	7399	8
170	A	3	03.2	03.22-1	0322105	Ranicultura	7399	8
171	A	3	03.2	03.22-1	0322106	Criação de jacaré	7399	8
172	A	3	03.2	03.22-1	0322107	Atividades de apoio à aqüicultura em água doce	7399	8
178	B	5	05.0	05.00-3	0500301	Extração de carvão mineral	7399	8
179	B	5	05.0	05.00-3	0500302	Beneficiamento de carvão mineral	7399	8
182	B	6	06.0	06.00-0	0600001	Extração de petróleo e gás natural	7399	8
183	B	6	06.0	06.00-0	0600002	Extração e beneficiamento de xisto	7399	8
184	B	6	06.0	06.00-0	0600003	Extração e beneficiamento de areias betuminosas	7399	8
188	B	7	07.1	07.10-3	0710301	Extração de minério de ferro	7399	8
189	B	7	07.1	07.10-3	0710302	Pelotização, sinterização e outros beneficiamentos de minério de ferro	7399	8
192	B	7	07.2	07.21-9	0721901	Extração de minério de alumínio	7399	8
193	B	7	07.2	07.21-9	0721902	Beneficiamento de minério de alumínio	7399	8
195	B	7	07.2	07.22-7	0722701	Extração de minério de estanho	7399	8
196	B	7	07.2	07.22-7	0722702	Beneficiamento de minério de estanho	7399	8
198	B	7	07.2	07.23-5	0723501	Extração de minério de manganês	7399	8
199	B	7	07.2	07.23-5	0723502	Beneficiamento de minério de manganês	7399	8
201	B	7	07.2	07.24-3	0724301	Extração de minério de metais preciosos	7399	8
202	B	7	07.2	07.24-3	0724302	Beneficiamento de minério de metais preciosos	7399	8
204	B	7	07.2	07.25-1	0725100	Extração de minerais radioativos	7399	8
207	B	7	07.2	07.29-4	0729402	Extração de minério de tungstênio	7399	8
208	B	7	07.2	07.29-4	0729403	Extração de minério de níquel	7399	8
209	B	7	07.2	07.29-4	0729404	Extração de minérios de cobre, chumbo, zinco e outros minerais metálicos não-ferrosos não especificados anteriormente	7399	8
214	B	8	08.1	08.10-0	0810001	Extração de ardósia e beneficiamento associado	7399	8
215	B	8	08.1	08.10-0	0810002	Extração de granito e beneficiamento associado	7399	8
216	B	8	08.1	08.10-0	0810003	Extração de mármore e beneficiamento associado	7399	8
217	B	8	08.1	08.10-0	0810004	Extração de calcário e dolomita e beneficiamento associado	7399	8
218	B	8	08.1	08.10-0	0810005	Extração de gesso e caulim	7399	8
219	B	8	08.1	08.10-0	0810006	Extração de areia, cascalho ou pedregulho e beneficiamento associado	7399	8
220	B	8	08.1	08.10-0	0810007	Extração de argila e beneficiamento associado	7399	8
221	B	8	08.1	08.10-0	0810008	Extração de saibro e beneficiamento associado	7399	8
222	B	8	08.1	08.10-0	0810009	Extração de basalto e beneficiamento associado	7399	8
223	B	8	08.1	08.10-0	0810010	Beneficiamento de gesso e caulim associado à extração	7399	8
224	B	8	08.1	08.10-0	0810099	Extração e britamento de pedras e outros materiais para construção e beneficiamento associado	7399	8
227	B	8	08.9	08.91-6	0891600	Extração de minerais para fabricação de adubos, fertilizantes e outros produtos químicos	7399	8
229	B	8	08.9	08.92-4	0892401	Extração de sal marinho	7399	8
230	B	8	08.9	08.92-4	0892402	Extração de sal-gema	7399	8
231	B	8	08.9	08.92-4	0892403	Refino e outros tratamentos do sal	7399	8
233	B	8	08.9	08.93-2	0893200	Extração de gemas (pedras preciosas e semipreciosas)	7399	8
235	B	8	08.9	08.99-1	0899101	Extração de grafita	7399	8
236	B	8	08.9	08.99-1	0899102	Extração de quartzo	7399	8
237	B	8	08.9	08.99-1	0899103	Extração de amianto	7399	8
238	B	8	08.9	08.99-1	0899199	Extração de outros minerais não-metálicos não especificados anteriormente	7399	8
242	B	9	09.1	09.10-6	0910600	Atividades de apoio à extração de petróleo e gás natural	7399	8
245	B	9	09.9	09.90-4	0990401	Atividades de apoio à extração de minério de ferro	7399	8
155	A	3	03.1	03.12-4	0312403	Coleta de outros produtos aquáticos de água doce	7399	8
253	C	10	10.1	10.11-2	1011202	Frigorífico - abate de eqüinos	7399	8
254	C	10	10.1	10.11-2	1011203	Frigorífico - abate de ovinos e caprinos	7399	8
255	C	10	10.1	10.11-2	1011204	Frigorífico - abate de bufalinos	7399	8
256	C	10	10.1	10.11-2	1011205	Matadouro - abate de reses sob contrato - exceto abate de suínos	7399	8
259	C	10	10.1	10.12-1	1012102	Abate de pequenos animais	7399	8
260	C	10	10.1	10.12-1	1012103	Frigorífico - abate de suínos	7399	8
261	C	10	10.1	10.12-1	1012104	Matadouro - abate de suínos sob contrato	7399	8
264	C	10	10.1	10.13-9	1013902	Preparação de subprodutos do abate	7399	8
267	C	10	10.2	10.20-1	1020101	Preservação de peixes, crustáceos e moluscos	7399	8
268	C	10	10.2	10.20-1	1020102	Fabricação de conservas de peixes, crustáceos e moluscos	7399	8
271	C	10	10.3	10.31-7	1031700	Fabricação de conservas de frutas	7399	8
273	C	10	10.3	10.32-5	1032501	Fabricação de conservas de palmito	7399	8
274	C	10	10.3	10.32-5	1032599	Fabricação de conservas de legumes e outros vegetais, exceto palmito	7399	8
276	C	10	10.3	10.33-3	1033301	Fabricação de sucos concentrados de frutas, hortaliças e legumes	7399	8
280	C	10	10.4	10.41-4	1041400	Fabricação de óleos vegetais em bruto, exceto óleo de milho	7399	8
282	C	10	10.4	10.42-2	1042200	Fabricação de óleos vegetais refinados, exceto óleo de milho	7399	8
284	C	10	10.4	10.43-1	1043100	Fabricação de margarina e outras gorduras vegetais e de óleos não-comestíveis de animais	7399	8
287	C	10	10.5	10.51-1	1051100	Preparação do leite	7399	8
289	C	10	10.5	10.52-0	1052000	Fabricação de laticínios	7399	8
291	C	10	10.5	10.53-8	1053800	Fabricação de sorvetes e outros gelados comestíveis	7399	8
294	C	10	10.6	10.61-9	1061901	Beneficiamento de arroz	7399	8
295	C	10	10.6	10.61-9	1061902	Fabricação de produtos do arroz	7399	8
297	C	10	10.6	10.62-7	1062700	Moagem de trigo e fabricação de derivados	7399	8
299	C	10	10.6	10.63-5	1063500	Fabricação de farinha de mandioca e derivados	7399	8
301	C	10	10.6	10.64-3	1064300	Fabricação de farinha de milho e derivados, exceto óleos de milho	7399	8
303	C	10	10.6	10.65-1	1065101	Fabricação de amidos e féculas de vegetais	7399	8
304	C	10	10.6	10.65-1	1065102	Fabricação de óleo de milho em bruto	7399	8
305	C	10	10.6	10.65-1	1065103	Fabricação de óleo de milho refinado	7399	8
307	C	10	10.6	10.66-0	1066000	Fabricação de alimentos para animais	7399	8
312	C	10	10.7	10.71-6	1071600	Fabricação de açúcar em bruto	7399	8
314	C	10	10.7	10.72-4	1072401	Fabricação de açúcar de cana refinado	7399	8
315	C	10	10.7	10.72-4	1072402	Fabricação de açúcar de cereais (dextrose) e de beterraba	7399	8
318	C	10	10.8	10.81-3	1081301	Beneficiamento de café	7399	8
319	C	10	10.8	10.81-3	1081302	Torrefação e moagem de café	7399	8
321	C	10	10.8	10.82-1	1082100	Fabricação de produtos à base de café	7399	8
326	C	10	10.9	10.92-9	1092900	Fabricação de biscoitos e bolachas	7399	8
328	C	10	10.9	10.93-7	1093701	Fabricação de produtos derivados do cacau e de chocolates	7399	8
329	C	10	10.9	10.93-7	1093702	Fabricação de frutas cristalizadas, balas e semelhantes	7399	8
333	C	10	10.9	10.95-3	1095300	Fabricação de especiarias, molhos, temperos e condimentos	7399	8
337	C	10	10.9	10.99-6	1099601	Fabricação de vinagres	7399	8
339	C	10	10.9	10.99-6	1099603	Fabricação de fermentos e leveduras	7399	8
340	C	10	10.9	10.99-6	1099604	Fabricação de gelo comum	7399	8
341	C	10	10.9	10.99-6	1099605	Fabricação de produtos para infusão (chá, mate, etc.)	7399	8
342	C	10	10.9	10.99-6	1099606	Fabricação de adoçantes naturais e artificiais	7399	8
343	C	10	10.9	10.99-6	1099699	Fabricação de outros produtos alimentícios não especificados anteriormente	7399	8
347	C	11	11.1	11.11-9	1111901	Fabricação de aguardente de cana-de-açúcar	7399	8
348	C	11	11.1	11.11-9	1111902	Fabricação de outras aguardentes e bebidas destiladas	7399	8
350	C	11	11.1	11.12-7	1112700	Fabricação de vinho	7399	8
353	C	11	11.1	11.13-5	1113502	Fabricação de cervejas e chopes	7399	8
356	C	11	11.2	11.21-6	1121600	Fabricação de águas envasadas	7399	8
612	C	22	22.1	22.12-9	2212900	Reforma de pneumáticos usados	7399	8
252	C	10	10.1	10.11-2	1011201	Frigorífico - abate de bovinos	7399	8
361	C	11	11.2	11.22-4	1122499	Fabricação de outras bebidas não-alcoólicas não especificadas anteriormente	7399	8
365	C	12	12.1	12.10-7	1210700	Processamento industrial do fumo	7399	8
368	C	12	12.2	12.20-4	1220401	Fabricação de cigarros	7399	8
369	C	12	12.2	12.20-4	1220402	Fabricação de cigarrilhas e charutos	7399	8
370	C	12	12.2	12.20-4	1220403	Fabricação de filtros para cigarros	7399	8
371	C	12	12.2	12.20-4	1220499	Fabricação de outros produtos do fumo, exceto cigarros, cigarrilhas e charutos	7399	8
331	C	10	10.9	10.94-5	1094500	Fabricação de massas alimentícias	5411	19
335	C	10	10.9	10.96-1	1096100	Fabricação de alimentos e pratos prontos	5411	19
375	C	13	13.1	13.11-1	1311100	Preparação e fiação de fibras de algodão	5948	39
377	C	13	13.1	13.12-0	1312000	Preparação e fiação de fibras têxteis naturais, exceto algodão	5948	39
2098	N	82	82.9	82.99-7	8299707	Salas de acesso à internet	5732	16
399	C	13	13.5	13.51-1	1351100	Fabricação de artefatos têxteis para uso doméstico	7399	8
401	C	13	13.5	13.52-9	1352900	Fabricação de artefatos de tapeçaria	7399	8
403	C	13	13.5	13.53-7	1353700	Fabricação de artefatos de cordoaria	7399	8
449	C	15	15.4	15.40-8	1540800	Fabricação de partes para calçados, de qualquer material	7399	8
453	C	16	16.1	16.10-2	1610201	Serrarias com desdobramento de madeira	7399	8
454	C	16	16.1	16.10-2	1610202	Serrarias sem desdobramento de madeira	7399	8
457	C	16	16.2	16.21-8	1621800	Fabricação de madeira laminada e de chapas de madeira compensada, prensada e aglomerada	7399	8
459	C	16	16.2	16.22-6	1622601	Fabricação de casas de madeira pré-fabricadas	7399	8
461	C	16	16.2	16.22-6	1622699	Fabricação de outros artigos de carpintaria para construção	7399	8
463	C	16	16.2	16.23-4	1623400	Fabricação de artefatos de tanoaria e de embalagens de madeira	7399	8
466	C	16	16.2	16.29-3	1629302	Fabricação de artefatos diversos de cortiça, bambu, palha, vime e outros materiais trançados, exceto móveis	7399	8
470	C	17	17.1	17.10-9	1710900	Fabricação de celulose e outras pastas para a fabricação de papel	7399	8
473	C	17	17.2	17.21-4	1721400	Fabricação de papel	7399	8
475	C	17	17.2	17.22-2	1722200	Fabricação de cartolina e papel-cartão	7399	8
478	C	17	17.3	17.31-1	1731100	Fabricação de embalagens de papel	7399	8
360	C	11	11.2	11.22-4	1122403	Fabricação de refrescos, xaropes e pós para refrescos, exceto refrescos de frutas	7399	8
485	C	17	17.4	17.41-9	1741901	Fabricação de formulários contínuos	7399	8
488	C	17	17.4	17.42-7	1742701	Fabricação de fraldas descartáveis	7399	8
489	C	17	17.4	17.42-7	1742702	Fabricação de absorventes higiênicos	7399	8
491	C	17	17.4	17.42-7	1742799	Fabricação de produtos de papel para uso doméstico e higiênico-sanitário não especificados anteriormente	7399	8
493	C	17	17.4	17.49-4	1749400	Fabricação de produtos de pastas celulósicas, papel, cartolina, papel-cartão e papelão ondulado não especificados anteriormente	7399	8
497	C	18	18.1	18.11-3	1811301	Impressão de jornais	7399	8
498	C	18	18.1	18.11-3	1811302	Impressão de livros, revistas e outras publicações periódicas	7399	8
500	C	18	18.1	18.12-1	1812100	Impressão de material de segurança	7399	8
503	C	18	18.1	18.13-0	1813099	Impressão de material para outros usos	7399	8
506	C	18	18.2	18.21-1	1821100	Serviços de pré-impressão	7399	8
381	C	13	13.1	13.14-6	1314600	Fabricação de linhas para costurar e bordar	5948	39
384	C	13	13.2	13.21-9	1321900	Tecelagem de fios de algodão	5948	39
388	C	13	13.2	13.23-5	1323500	Tecelagem de fios de fibras artificiais e sintéticas	5948	39
391	C	13	13.3	13.30-8	1330800	Fabricação de tecidos de malha	5948	39
395	C	13	13.4	13.40-5	1340502	Alvejamento, tingimento e torção em fios, tecidos, artefatos têxteis e peças do vestuário	5948	39
396	C	13	13.4	13.40-5	1340599	Outros serviços de acabamento em fios, tecidos, artefatos têxteis e peças do vestuário	5948	39
405	C	13	13.5	13.54-5	1354500	Fabricação de tecidos especiais, inclusive artefatos	5948	39
411	C	14	14.1	14.11-8	1411801	Confecção de roupas íntimas	5651	2
412	C	14	14.1	14.11-8	1411802	Facção de roupas íntimas	5651	2
414	C	14	14.1	14.12-6	1412601	Confecção de peças do vestuário, exceto roupas íntimas e as confeccionadas sob medida	5651	2
415	C	14	14.1	14.12-6	1412602	Confecção, sob medida, de peças do vestuário, exceto roupas íntimas	5651	2
416	C	14	14.1	14.12-6	1412603	Facção de peças do vestuário, exceto roupas íntimas	5651	2
418	C	14	14.1	14.13-4	1413401	Confecção de roupas profissionais, exceto sob medida	5137	2
419	C	14	14.1	14.13-4	1413402	Confecção, sob medida, de roupas profissionais	5137	2
420	C	14	14.1	14.13-4	1413403	Facção de roupas profissionais	5137	2
422	C	14	14.1	14.14-2	1414200	Fabricação de acessórios do vestuário, exceto para segurança e proteção	5651	2
425	C	14	14.2	14.21-5	1421500	Fabricação de meias	5651	2
427	C	14	14.2	14.22-3	1422300	Fabricação de artigos do vestuário, produzidos em malharias e tricotagens, exceto meias	5651	2
431	C	15	15.1	15.10-6	1510600	Curtimento e outras preparações de couro	5948	39
436	C	15	15.2	15.29-7	1529700	Fabricação de artefatos de couro não especificados anteriormente	5948	39
439	C	15	15.3	15.31-9	1531901	Fabricação de calçados de couro	5948	39
440	C	15	15.3	15.31-9	1531902	Acabamento de calçados de couro sob contrato	5948	39
442	C	15	15.3	15.32-7	1532700	Fabricação de tênis de qualquer material	5661	2
444	C	15	15.3	15.33-5	1533500	Fabricação de calçados de material sintético	5661	2
446	C	15	15.3	15.39-4	1539400	Fabricação de calçados de materiais não especificados anteriormente	5661	2
465	C	16	16.2	16.29-3	1629301	Fabricação de artefatos diversos de madeira, exceto móveis	5970	3
502	C	18	18.1	18.13-0	1813001	Impressão de material para uso publicitário	2741	9
508	C	18	18.2	18.22-9	1822900	Serviços de acabamentos gráficos	2741	9
511	C	18	18.3	18.30-0	1830001	Reprodução de som em qualquer suporte	7399	8
512	C	18	18.3	18.30-0	1830002	Reprodução de vídeo em qualquer suporte	7399	8
513	C	18	18.3	18.30-0	1830003	Reprodução de software em qualquer suporte	7399	8
520	C	19	19.2	19.21-7	1921700	Fabricação de produtos do refino de petróleo	7399	8
522	C	19	19.2	19.22-5	1922501	Formulação de combustíveis	7399	8
523	C	19	19.2	19.22-5	1922502	Rerrefino de óleos lubrificantes	7399	8
524	C	19	19.2	19.22-5	1922599	Fabricação de outros produtos derivados do petróleo, exceto produtos do refino	7399	8
527	C	19	19.3	19.31-4	1931400	Fabricação de álcool	7399	8
529	C	19	19.3	19.32-2	1932200	Fabricação de biocombustíveis, exceto álcool	7399	8
533	C	20	20.1	20.11-8	2011800	Fabricação de cloro e álcalis	7399	8
535	C	20	20.1	20.12-6	2012600	Fabricação de intermediários para fertilizantes	7399	8
537	C	20	20.1	20.13-4	2013400	Fabricação de adubos e fertilizantes	7399	8
541	C	20	20.1	20.19-3	2019301	Elaboração de combustíveis nucleares	7399	8
542	C	20	20.1	20.19-3	2019399	Fabricação de outros produtos químicos inorgânicos não especificados anteriormente	7399	8
545	C	20	20.2	20.21-5	2021500	Fabricação de produtos petroquímicos básicos	7399	8
547	C	20	20.2	20.22-3	2022300	Fabricação de intermediários para plastificantes, resinas e fibras	7399	8
549	C	20	20.2	20.29-1	2029100	Fabricação de produtos químicos orgânicos não especificados anteriormente	7399	8
552	C	20	20.3	20.31-2	2031200	Fabricação de resinas termoplásticas	7399	8
554	C	20	20.3	20.32-1	2032100	Fabricação de resinas termofixas	7399	8
556	C	20	20.3	20.33-9	2033900	Fabricação de elastômeros	7399	8
562	C	20	20.5	20.51-7	2051700	Fabricação de defensivos agrícolas	7399	8
564	C	20	20.5	20.52-5	2052500	Fabricação de desinfestantes domissanitários	7399	8
567	C	20	20.6	20.61-4	2061400	Fabricação de sabões e detergentes sintéticos	7399	8
572	C	20	20.6	20.63-1	2063100	Fabricação de cosméticos, produtos de perfumaria e de higiene pessoal	7399	8
575	C	20	20.7	20.71-1	2071100	Fabricação de tintas, vernizes, esmaltes e lacas	7399	8
577	C	20	20.7	20.72-0	2072000	Fabricação de tintas de impressão	7399	8
579	C	20	20.7	20.73-8	2073800	Fabricação de impermeabilizantes, solventes e produtos afins	7399	8
582	C	20	20.9	20.91-6	2091600	Fabricação de adesivos e selantes	7399	8
584	C	20	20.9	20.92-4	2092401	Fabricação de pólvoras, explosivos e detonantes	7399	8
585	C	20	20.9	20.92-4	2092402	Fabricação de artigos pirotécnicos	7399	8
586	C	20	20.9	20.92-4	2092403	Fabricação de fósforos de segurança	7399	8
590	C	20	20.9	20.94-1	2094100	Fabricação de catalisadores	7399	8
592	C	20	20.9	20.99-1	2099101	Fabricação de chapas, filmes, papéis e outros materiais e produtos químicos para fotografia	7399	8
593	C	20	20.9	20.99-1	2099199	Fabricação de outros produtos químicos não especificados anteriormente	7399	8
597	C	21	21.1	21.10-6	2110600	Fabricação de produtos farmoquímicos	7399	8
600	C	21	21.2	21.21-1	2121101	Fabricação de medicamentos alopáticos para uso humano	7399	8
601	C	21	21.2	21.21-1	2121102	Fabricação de medicamentos homeopáticos para uso humano	7399	8
602	C	21	21.2	21.21-1	2121103	Fabricação de medicamentos fitoterápicos para uso humano	7399	8
604	C	21	21.2	21.22-0	2122000	Fabricação de medicamentos para uso veterinário	7399	8
482	C	17	17.3	17.33-8	1733800	Fabricação de chapas e de embalagens de papelão ondulado	7399	8
617	C	22	22.2	22.21-8	2221800	Fabricação de laminados planos e tubulares de material plástico	7399	8
619	C	22	22.2	22.22-6	2222600	Fabricação de embalagens de material plástico	7399	8
621	C	22	22.2	22.23-4	2223400	Fabricação de tubos e acessórios de material plástico para uso na construção	7399	8
623	C	22	22.2	22.29-3	2229301	Fabricação de artefatos de material plástico para uso pessoal e doméstico	7399	8
624	C	22	22.2	22.29-3	2229302	Fabricação de artefatos de material plástico para usos industriais	7399	8
626	C	22	22.2	22.29-3	2229399	Fabricação de artefatos de material plástico para outros usos não especificados anteriormente	7399	8
630	C	23	23.1	23.11-7	2311700	Fabricação de vidro plano e de segurança	7399	8
632	C	23	23.1	23.12-5	2312500	Fabricação de embalagens de vidro	7399	8
634	C	23	23.1	23.19-2	2319200	Fabricação de artigos de vidro	7399	8
637	C	23	23.2	23.20-6	2320600	Fabricação de cimento	7399	8
641	C	23	23.3	23.30-3	2330302	Fabricação de artefatos de cimento para uso na construção	7399	8
642	C	23	23.3	23.30-3	2330303	Fabricação de artefatos de fibrocimento para uso na construção	7399	8
643	C	23	23.3	23.30-3	2330304	Fabricação de casas pré-moldadas de concreto	7399	8
644	C	23	23.3	23.30-3	2330305	Preparação de massa de concreto e argamassa para construção	7399	8
640	C	23	23.3	23.30-3	2330301	Fabricação de estruturas pré-moldadas de concreto armado, em série e sob encomenda	5039	9
647	C	23	23.4	23.41-9	2341900	Fabricação de produtos cerâmicos refratários	7399	8
649	C	23	23.4	23.42-7	2342701	Fabricação de azulejos e pisos	7399	8
650	C	23	23.4	23.42-7	2342702	Fabricação de artefatos de cerâmica e barro cozido para uso na construção, exceto azulejos e pisos	7399	8
652	C	23	23.4	23.49-4	2349401	Fabricação de material sanitário de cerâmica	7399	8
653	C	23	23.4	23.49-4	2349499	Fabricação de produtos cerâmicos não-refratários não especificados anteriormente	7399	8
657	C	23	23.9	23.91-5	2391502	Aparelhamento de pedras para construção, exceto associado à extração	7399	8
660	C	23	23.9	23.92-3	2392300	Fabricação de cal e gesso	7399	8
662	C	23	23.9	23.99-1	2399101	Decoração, lapidação, gravação, vitrificação e outros trabalhos em cerâmica, louça, vidro e cristal	7399	8
663	C	23	23.9	23.99-1	2399199	Fabricação de outros produtos de minerais não-metálicos não especificados anteriormente	7399	8
667	C	24	24.1	24.11-3	2411300	Produção de ferro-gusa	7399	8
669	C	24	24.1	24.12-1	2412100	Produção de ferroligas	7399	8
672	C	24	24.2	24.21-1	2421100	Produção de semi-acabados de aço	7399	8
674	C	24	24.2	24.22-9	2422901	Produção de laminados planos de aço ao carbono, revestidos ou não	7399	8
675	C	24	24.2	24.22-9	2422902	Produção de laminados planos de aços especiais	7399	8
677	C	24	24.2	24.23-7	2423701	Produção de tubos de aço sem costura	7399	8
678	C	24	24.2	24.23-7	2423702	Produção de laminados longos de aço, exceto tubos	7399	8
680	C	24	24.2	24.24-5	2424501	Produção de arames de aço	7399	8
681	C	24	24.2	24.24-5	2424502	Produção de relaminados, trefilados e perfilados de aço, exceto arames	7399	8
684	C	24	24.3	24.31-8	2431800	Produção de tubos de aço com costura	7399	8
686	C	24	24.3	24.39-3	2439300	Produção de outros tubos de ferro e aço	7399	8
689	C	24	24.4	24.41-5	2441501	Produção de alumínio e suas ligas em formas primárias	7399	8
690	C	24	24.4	24.41-5	2441502	Produção de laminados de alumínio	7399	8
692	C	24	24.4	24.42-3	2442300	Metalurgia dos metais preciosos	7399	8
694	C	24	24.4	24.43-1	2443100	Metalurgia do cobre	7399	8
696	C	24	24.4	24.49-1	2449101	Produção de zinco em formas primárias	7399	8
698	C	24	24.4	24.49-1	2449103	Produção de soldas e ânodos para galvanoplastia	7399	8
699	C	24	24.4	24.49-1	2449199	Metalurgia de outros metais não-ferrosos e suas ligas não especificados anteriormente	7399	8
702	C	24	24.5	24.51-2	2451200	Fundição de ferro e aço	7399	8
704	C	24	24.5	24.52-1	2452100	Fundição de metais não-ferrosos e suas ligas	7399	8
708	C	25	25.1	25.11-0	2511000	Fabricação de estruturas metálicas	7399	8
712	C	25	25.1	25.13-6	2513600	Fabricação de obras de caldeiraria pesada	7399	8
717	C	25	25.2	25.22-5	2522500	Fabricação de caldeiras geradoras de vapor, exceto para aquecimento central e para veículos	7399	8
720	C	25	25.3	25.31-4	2531401	Produção de forjados de aço	7399	8
721	C	25	25.3	25.31-4	2531402	Produção de forjados de metais não-ferrosos e suas ligas	7399	8
614	C	22	22.1	22.19-6	2219600	Fabricação de artefatos de borracha não especificados anteriormente	7399	8
726	C	25	25.3	25.39-0	2539000	Serviços de usinagem, solda, tratamento e revestimento em metais	7399	8
729	C	25	25.4	25.41-1	2541100	Fabricação de artigos de cutelaria	7399	8
733	C	25	25.4	25.43-8	2543800	Fabricação de ferramentas	7399	8
736	C	25	25.5	25.50-1	2550101	Fabricação de equipamento bélico pesado, exceto veículos militares de combate	7399	8
737	C	25	25.5	25.50-1	2550102	Fabricação de armas de fogo e munições	7399	8
740	C	25	25.9	25.91-8	2591800	Fabricação de embalagens metálicas	7399	8
742	C	25	25.9	25.92-6	2592601	Fabricação de produtos de trefilados de metal padronizados	7399	8
743	C	25	25.9	25.92-6	2592602	Fabricação de produtos de trefilados de metal, exceto padronizados	7399	8
745	C	25	25.9	25.93-4	2593400	Fabricação de artigos de metal para uso doméstico e pessoal	7399	8
747	C	25	25.9	25.99-3	2599301	Serviços de confecção de armações metálicas para a construção	7399	8
748	C	25	25.9	25.99-3	2599399	Fabricação de outros produtos de metal não especificados anteriormente	7399	8
752	C	26	26.1	26.10-8	2610800	Fabricação de componentes eletrônicos	7399	8
755	C	26	26.2	26.21-3	2621300	Fabricação de equipamentos de informática	7399	8
760	C	26	26.3	26.31-1	2631100	Fabricação de equipamentos transmissores de comunicação, peças e acessórios	7399	8
762	C	26	26.3	26.32-9	2632900	Fabricação de aparelhos telefônicos e de outros equipamentos de comunicação, peças e acessórios	7399	8
765	C	26	26.4	26.40-0	2640000	Fabricação de aparelhos de recepção, reprodução, gravação e amplificação de áudio e vídeo	7399	8
768	C	26	26.5	26.51-5	2651500	Fabricação de aparelhos e equipamentos de medida, teste e controle	7399	8
770	C	26	26.5	26.52-3	2652300	Fabricação de cronômetros e relógios	7399	8
710	C	25	25.1	25.12-8	2512800	Fabricação de esquadrias de metal	5211	23
1081	E	38	38.1	38.12-2	3812200	Coleta de resíduos perigosos	4900	39
773	C	26	26.6	26.60-4	2660400	Fabricação de aparelhos eletromédicos e eletroterapêuticos e equipamentos de irradiação	7399	8
776	C	26	26.7	26.70-1	2670101	Fabricação de equipamentos e instrumentos ópticos, peças e acessórios	7399	8
777	C	26	26.7	26.70-1	2670102	Fabricação de aparelhos fotográficos e cinematográficos, peças e acessórios	7399	8
780	C	26	26.8	26.80-9	2680900	Fabricação de mídias virgens, magnéticas e ópticas	7399	8
1131	F	42	42.9	42.91-0	4291000	Obras portuárias, marítimas e fluviais	7399	8
784	C	27	27.1	27.10-4	2710401	Fabricação de geradores de corrente contínua e alternada, peças e acessórios	7399	8
786	C	27	27.1	27.10-4	2710403	Fabricação de motores elétricos, peças e acessórios	7399	8
789	C	27	27.2	27.21-0	2721000	Fabricação de pilhas, baterias e acumuladores elétricos, exceto para veículos automotores	7399	8
791	C	27	27.2	27.22-8	2722801	Fabricação de baterias e acumuladores para veículos automotores	7399	8
792	C	27	27.2	27.22-8	2722802	Recondicionamento de baterias e acumuladores para veículos automotores	7399	8
795	C	27	27.3	27.31-7	2731700	Fabricação de aparelhos e equipamentos para distribuição e controle de energia elétrica	7399	8
797	C	27	27.3	27.32-5	2732500	Fabricação de material elétrico para instalações em circuito de consumo	7399	8
799	C	27	27.3	27.33-3	2733300	Fabricação de fios, cabos e condutores elétricos isolados	7399	8
802	C	27	27.4	27.40-6	2740601	Fabricação de lâmpadas	7399	8
803	C	27	27.4	27.40-6	2740602	Fabricação de luminárias e outros equipamentos de iluminação	7399	8
806	C	27	27.5	27.51-1	2751100	Fabricação de fogões, refrigeradores e máquinas de lavar e secar para uso doméstico, peças e acessórios	7399	8
808	C	27	27.5	27.59-7	2759701	Fabricação de aparelhos elétricos de uso pessoal, peças e acessórios	7399	8
809	C	27	27.5	27.59-7	2759799	Fabricação de outros aparelhos eletrodomésticos não especificados anteriormente, peças e acessórios	7399	8
812	C	27	27.9	27.90-2	2790201	Fabricação de eletrodos, contatos e outros artigos de carvão e grafita para uso elétrico, eletroímãs e isoladores	7399	8
813	C	27	27.9	27.90-2	2790202	Fabricação de equipamentos para sinalização e alarme	7399	8
814	C	27	27.9	27.90-2	2790299	Fabricação de outros equipamentos e aparelhos elétricos não especificados anteriormente	7399	8
820	C	28	28.1	28.12-7	2812700	Fabricação de equipamentos hidráulicos e pneumáticos, peças e acessórios, exceto válvulas	7399	8
822	C	28	28.1	28.13-5	2813500	Fabricação de válvulas, registros e dispositivos semelhantes, peças e acessórios	7399	8
824	C	28	28.1	28.14-3	2814301	Fabricação de compressores para uso industrial, peças e acessórios	7399	8
825	C	28	28.1	28.14-3	2814302	Fabricação de compressores para uso não industrial, peças e acessórios	7399	8
827	C	28	28.1	28.15-1	2815101	Fabricação de rolamentos para fins industriais	7399	8
828	C	28	28.1	28.15-1	2815102	Fabricação de equipamentos de transmissão para fins industriais, exceto rolamentos	7399	8
831	C	28	28.2	28.21-6	2821601	Fabricação de fornos industriais, aparelhos e equipamentos não-elétricos para instalações térmicas, peças e acessórios	7399	8
832	C	28	28.2	28.21-6	2821602	Fabricação de estufas e fornos elétricos para fins industriais, peças e acessórios	7399	8
724	C	25	25.3	25.32-2	2532202	Metalurgia do pó	7399	8
837	C	28	28.2	28.23-2	2823200	Fabricação de máquinas e aparelhos de refrigeração e ventilação para uso industrial e comercial, peças e acessórios	7399	8
839	C	28	28.2	28.24-1	2824101	Fabricação de aparelhos e equipamentos de ar condicionado para uso industrial	7399	8
840	C	28	28.2	28.24-1	2824102	Fabricação de aparelhos e equipamentos de ar condicionado para uso não-industrial	7399	8
844	C	28	28.2	28.29-1	2829101	Fabricação de máquinas de escrever, calcular e outros equipamentos não-eletrônicos para escritório, peças e acessórios	7399	8
845	C	28	28.2	28.29-1	2829199	Fabricação de outras máquinas e equipamentos de uso geral não especificados anteriormente, peças e acessórios	7399	8
848	C	28	28.3	28.31-3	2831300	Fabricação de tratores agrícolas, peças e acessórios	7399	8
850	C	28	28.3	28.32-1	2832100	Fabricação de equipamentos para irrigação agrícola, peças e acessórios	7399	8
852	C	28	28.3	28.33-0	2833000	Fabricação de máquinas e equipamentos para a agricultura e pecuária, peças e acessórios, exceto para irrigação	7399	8
854	C	28	28.4	28.40-2	2840200	Fabricação de máquinas-ferramenta, peças e acessórios	7399	8
857	C	28	28.5	28.51-8	2851800	Fabricação de máquinas e equipamentos para a prospecção e extração de petróleo, peças e acessórios	7399	8
859	C	28	28.5	28.52-6	2852600	Fabricação de outras máquinas e equipamentos para uso na extração mineral, peças e acessórios, exceto na extração de petróleo	7399	8
861	C	28	28.5	28.53-4	2853400	Fabricação de tratores, peças e acessórios, exceto agrícolas	7399	8
863	C	28	28.5	28.54-2	2854200	Fabricação de máquinas e equipamentos para terraplenagem, pavimentação e construção, peças e acessórios, exceto tratores	7399	8
866	C	28	28.6	28.61-5	2861500	Fabricação de máquinas para a indústria metalúrgica, peças e acessórios, exceto máquinas-ferramenta	7399	8
868	C	28	28.6	28.62-3	2862300	Fabricação de máquinas e equipamentos para as indústrias de alimentos, bebidas e fumo, peças e acessórios	7399	8
870	C	28	28.6	28.63-1	2863100	Fabricação de máquinas e equipamentos para a indústria têxtil, peças e acessórios	7399	8
874	C	28	28.6	28.65-8	2865800	Fabricação de máquinas e equipamentos para as indústrias de celulose, papel e papelão e artefatos, peças e acessórios	7399	8
876	C	28	28.6	28.66-6	2866600	Fabricação de máquinas e equipamentos para a indústria do plástico, peças e acessórios	7399	8
878	C	28	28.6	28.69-1	2869100	Fabricação de máquinas e equipamentos para uso industrial específico não especificados anteriormente, peças e acessórios	7399	8
882	C	29	29.1	29.10-7	2910701	Fabricação de automóveis, camionetas e utilitários	7399	8
883	C	29	29.1	29.10-7	2910702	Fabricação de chassis com motor para automóveis, camionetas e utilitários	7399	8
884	C	29	29.1	29.10-7	2910703	Fabricação de motores para automóveis, camionetas e utilitários	7399	8
887	C	29	29.2	29.20-4	2920401	Fabricação de caminhões e ônibus	7399	8
888	C	29	29.2	29.20-4	2920402	Fabricação de motores para caminhões e ônibus	7399	8
891	C	29	29.3	29.30-1	2930101	Fabricação de cabines, carrocerias e reboques para caminhões	7399	8
892	C	29	29.3	29.30-1	2930102	Fabricação de carrocerias para ônibus	7399	8
893	C	29	29.3	29.30-1	2930103	Fabricação de cabines, carrocerias e reboques para outros veículos automotores, exceto caminhões e ônibus	7399	8
896	C	29	29.4	29.41-7	2941700	Fabricação de peças e acessórios para o sistema motor de veículos automotores	7399	8
898	C	29	29.4	29.42-5	2942500	Fabricação de peças e acessórios para os sistemas de marcha e transmissão de veículos automotores	7399	8
900	C	29	29.4	29.43-3	2943300	Fabricação de peças e acessórios para o sistema de freios de veículos automotores	7399	8
902	C	29	29.4	29.44-1	2944100	Fabricação de peças e acessórios para o sistema de direção e suspensão de veículos automotores	7399	8
904	C	29	29.4	29.45-0	2945000	Fabricação de material elétrico e eletrônico para veículos automotores, exceto baterias	7399	8
906	C	29	29.4	29.49-2	2949201	Fabricação de bancos e estofados para veículos automotores	7399	8
907	C	29	29.4	29.49-2	2949299	Fabricação de outras peças e acessórios para veículos automotores não especificadas anteriormente	7399	8
914	C	30	30.1	30.11-3	3011301	Construção de embarcações de grande porte	7399	8
917	C	30	30.1	30.12-1	3012100	Construção de embarcações para esporte e lazer	7399	8
925	C	30	30.3	30.31-8	3031800	Fabricação de locomotivas, vagões e outros materiais rodantes	7399	8
927	C	30	30.3	30.32-6	3032600	Fabricação de peças e acessórios para veículos ferroviários	7399	8
930	C	30	30.4	30.41-5	3041500	Fabricação de aeronaves	7399	8
932	C	30	30.4	30.42-3	3042300	Fabricação de turbinas, motores e outros componentes e peças para aeronaves	7399	8
835	C	28	28.2	28.22-4	2822402	Fabricação de máquinas, equipamentos e aparelhos para transporte e elevação de cargas, peças e acessórios	7399	8
942	C	30	30.9	30.99-7	3099700	Fabricação de equipamentos de transporte não especificados anteriormente	7399	8
948	C	31	31.0	31.02-1	3102100	Fabricação de móveis com predominância de metal	7399	8
950	C	31	31.0	31.03-9	3103900	Fabricação de móveis de outros materiais, exceto madeira e metal	7399	8
952	C	31	31.0	31.04-7	3104700	Fabricação de colchões	7399	8
957	C	32	32.1	32.11-6	3211602	Fabricação de artefatos de joalheria e ourivesaria	7399	8
958	C	32	32.1	32.11-6	3211603	Cunhagem de moedas e medalhas	7399	8
963	C	32	32.2	32.20-5	3220500	Fabricação de instrumentos musicais, peças e acessórios	7399	8
966	C	32	32.3	32.30-2	3230200	Fabricação de artefatos para pesca e esporte	7399	8
969	C	32	32.4	32.40-0	3240001	Fabricação de jogos eletrônicos	7399	8
971	C	32	32.4	32.40-0	3240003	Fabricação de mesas de bilhar, de sinuca e acessórios associada à locação	7399	8
972	C	32	32.4	32.40-0	3240099	Fabricação de outros brinquedos e jogos recreativos não especificados anteriormente	7399	8
975	C	32	32.5	32.50-7	3250701	Fabricação de instrumentos não-eletrônicos e utensílios para uso médico, cirúrgico, odontológico e de laboratório	7399	8
976	C	32	32.5	32.50-7	3250702	Fabricação de mobiliário para uso médico, cirúrgico, odontológico e de laboratório	7399	8
977	C	32	32.5	32.50-7	3250703	Fabricação de aparelhos e utensílios para correção de defeitos físicos e aparelhos ortopédicos em geral sob encomenda	7399	8
978	C	32	32.5	32.50-7	3250704	Fabricação de aparelhos e utensílios para correção de defeitos físicos e aparelhos ortopédicos em geral, exceto sob encomenda	7399	8
979	C	32	32.5	32.50-7	3250705	Fabricação de materiais para medicina e odontologia	7399	8
981	C	32	32.5	32.50-7	3250707	Fabricação de artigos ópticos	7399	8
982	C	32	32.5	32.50-7	3250708	Fabricação de artefatos de tecido não tecido para uso odonto-médico-hospitalar	7399	8
985	C	32	32.9	32.91-4	3291400	Fabricação de escovas, pincéis e vassouras	7399	8
987	C	32	32.9	32.92-2	3292201	Fabricação de roupas de proteção e segurança e resistentes a fogo	7399	8
946	C	31	31.0	31.01-2	3101200	Fabricação de móveis com predominância de madeira	5712	24
956	C	32	32.1	32.11-6	3211601	Lapidação de gemas	5094	39
960	C	32	32.1	32.12-4	3212400	Fabricação de bijuterias e artefatos semelhantes	5631	2
980	C	32	32.5	32.50-7	3250706	Serviços de prótese dentária	5047	9
1092	E	38	38.3	38.32-7	3832700	Recuperação de materiais plásticos	5211	23
1094	E	38	38.3	38.39-4	3839401	Usinas de compostagem	5211	23
1279	G	46	46.3	46.35-4	4635401	Comércio atacadista de água mineral	5411	19
988	C	32	32.9	32.92-2	3292202	Fabricação de equipamentos e acessórios para segurança pessoal e profissional	7399	8
990	C	32	32.9	32.99-0	3299001	Fabricação de guarda-chuvas e similares	7399	8
993	C	32	32.9	32.99-0	3299004	Fabricação de painéis e letreiros luminosos	7399	8
994	C	32	32.9	32.99-0	3299005	Fabricação de aviamentos para costura	7399	8
995	C	32	32.9	32.99-0	3299099	Fabricação de produtos diversos não especificados anteriormente	7399	8
1133	F	42	42.9	42.92-8	4292801	Montagem de estruturas metálicas	7399	8
999	C	33	33.1	33.11-2	3311200	Manutenção e reparação de tanques, reservatórios metálicos e caldeiras, exceto para veículos	7399	8
1002	C	33	33.1	33.12-1	3312102	Manutenção e reparação de aparelhos e instrumentos de medida, teste e controle	7399	8
1003	C	33	33.1	33.12-1	3312103	Manutenção e reparação de aparelhos eletromédicos e eletroterapêuticos e equipamentos de irradiação	7399	8
1004	C	33	33.1	33.12-1	3312104	Manutenção e reparação de equipamentos e instrumentos ópticos	7399	8
1006	C	33	33.1	33.13-9	3313901	Manutenção e reparação de geradores, transformadores e motores elétricos	7399	8
1007	C	33	33.1	33.13-9	3313902	Manutenção e reparação de baterias e acumuladores elétricos, exceto para veículos	7399	8
1010	C	33	33.1	33.14-7	3314701	Manutenção e reparação de máquinas motrizes não-elétricas	7399	8
1011	C	33	33.1	33.14-7	3314702	Manutenção e reparação de equipamentos hidráulicos e pneumáticos, exceto válvulas	7399	8
1012	C	33	33.1	33.14-7	3314703	Manutenção e reparação de válvulas industriais	7399	8
1013	C	33	33.1	33.14-7	3314704	Manutenção e reparação de compressores	7399	8
1014	C	33	33.1	33.14-7	3314705	Manutenção e reparação de equipamentos de transmissão para fins industriais	7399	8
1015	C	33	33.1	33.14-7	3314706	Manutenção e reparação de máquinas, aparelhos e equipamentos para instalações térmicas	7399	8
1017	C	33	33.1	33.14-7	3314708	Manutenção e reparação de máquinas, equipamentos e aparelhos para transporte e elevação de cargas	7399	8
938	C	30	30.9	30.91-1	3091100	Fabricação de motocicletas, peças e acessórios	7399	8
1022	C	33	33.1	33.14-7	3314713	Manutenção e reparação de máquinas-ferramenta	7399	8
1025	C	33	33.1	33.14-7	3314716	Manutenção e reparação de tratores, exceto agrícolas	7399	8
1026	C	33	33.1	33.14-7	3314717	Manutenção e reparação de máquinas e equipamentos de terraplenagem, pavimentação e construção, exceto tratores	7399	8
1027	C	33	33.1	33.14-7	3314718	Manutenção e reparação de máquinas para a indústria metalúrgica, exceto máquinas-ferramenta	7399	8
1028	C	33	33.1	33.14-7	3314719	Manutenção e reparação de máquinas e equipamentos para as indústrias de alimentos, bebidas e fumo	7399	8
1029	C	33	33.1	33.14-7	3314720	Manutenção e reparação de máquinas e equipamentos para a indústria têxtil, do vestuário, do couro e calçados	7399	8
1030	C	33	33.1	33.14-7	3314721	Manutenção e reparação de máquinas e aparelhos para a indústria de celulose, papel e papelão e artefatos	7399	8
1031	C	33	33.1	33.14-7	3314722	Manutenção e reparação de máquinas e aparelhos para a indústria do plástico	7399	8
1032	C	33	33.1	33.14-7	3314799	Manutenção e reparação de outras máquinas e equipamentos para usos industriais não especificados anteriormente	7399	8
1034	C	33	33.1	33.15-5	3315500	Manutenção e reparação de veículos ferroviários	7399	8
1036	C	33	33.1	33.16-3	3316301	Manutenção e reparação de aeronaves, exceto a manutenção na pista	7399	8
1037	C	33	33.1	33.16-3	3316302	Manutenção de aeronaves na pista *	7399	8
1039	C	33	33.1	33.19-8	3319800	Manutenção e reparação de equipamentos e produtos não especificados anteriormente	7399	8
1042	C	33	33.2	33.21-0	3321000	Instalação de máquinas e equipamentos industriais	7399	8
1044	C	33	33.2	33.29-5	3329501	Serviços de montagem de móveis de qualquer material	7399	8
1045	C	33	33.2	33.29-5	3329599	Instalação de outros equipamentos não especificados anteriormente	7399	8
1018	C	33	33.1	33.14-7	3314709	Manutenção e reparação de máquinas de escrever, calcular e de outros equipamentos não-eletrônicos para escritório	5978	39
1019	C	33	33.1	33.14-7	3314710	Manutenção e reparação de máquinas e equipamentos para uso geral não especificados anteriormente	7699	36
1050	D	35	35.1	35.11-5	3511500	Geração de energia elétrica	4900	39
1052	D	35	35.1	35.12-3	3512300	Transmissão de energia elétrica	4900	39
1054	D	35	35.1	35.13-1	3513100	Comércio atacadista de energia elétrica	4900	39
1056	D	35	35.1	35.14-0	3514000	Distribuição de energia elétrica	4900	39
1060	D	35	35.2	35.20-4	3520402	Distribuição de combustíveis gasosos por redes urbanas	4900	39
1063	D	35	35.3	35.30-1	3530100	Produção e distribuição de vapor, água quente e ar condicionado	4900	39
1068	E	36	36.0	36.00-6	3600601	Captação, tratamento e distribuição de água	4900	39
1069	E	36	36.0	36.00-6	3600602	Distribuição de água por caminhões	4900	39
1073	E	37	37.0	37.01-1	3701100	Gestão de redes de esgoto	4900	39
1075	E	37	37.0	37.02-9	3702900	Atividades relacionadas a esgoto, exceto a gestão de redes	4900	39
1079	E	38	38.1	38.11-4	3811400	Coleta de resíduos não-perigosos	4900	39
1681	J	58	58.1	58.11-5	5811500	Edição de livros	2741	9
1111	F	42	42.1	42.11-1	4211101	Construção de rodovias e ferrovias	7399	8
1112	F	42	42.1	42.11-1	4211102	Pintura para sinalização em pistas rodoviárias e aeroportos	7399	8
1114	F	42	42.1	42.12-0	4212000	Construção de obras de arte especiais	7399	8
1122	F	42	42.2	42.21-9	4221904	Construção de estações e redes de telecomunicações	7399	8
1125	F	42	42.2	42.22-7	4222701	Construção de redes de abastecimento de água, coleta de esgoto e construções correlatas, exceto obras de irrigação	7399	8
1126	F	42	42.2	42.22-7	4222702	Obras de irrigação	7399	8
1128	F	42	42.2	42.23-5	4223500	Construção de redes de transportes por dutos, exceto para água e esgoto	7399	8
1134	F	42	42.9	42.92-8	4292802	Obras de montagem industrial	7399	8
1021	C	33	33.1	33.14-7	3314712	Manutenção e reparação de tratores agrícolas	7399	8
1144	F	43	43.1	43.12-6	4312600	Perfurações e sondagens	7399	8
1146	F	43	43.1	43.13-4	4313400	Obras de terraplenagem	7399	8
1159	F	43	43.2	43.29-1	4329103	Instalação, manutenção e reparação de elevadores, escadas e esteiras rolantes, exceto de fabricação própria	7399	8
1160	F	43	43.2	43.29-1	4329104	Montagem e instalação de sistemas e equipamentos de iluminação e sinalização em vias públicas, portos e aeroportos	7399	8
1173	F	43	43.9	43.91-6	4391600	Obras de fundações	7399	8
1176	F	43	43.9	43.99-1	4399102	Montagem e desmontagem de andaimes e outras estruturas temporárias	7399	8
1089	E	38	38.3	38.31-9	3831901	Recuperação de sucatas de alumínio	5211	23
1095	E	38	38.3	38.39-4	3839499	Recuperação de materiais não especificados anteriormente	5211	23
1099	E	39	39.0	39.00-5	3900500	Descontaminação e outros serviços de gestão de resíduos	5211	23
1104	F	41	41.1	41.10-7	4110700	Incorporação de empreendimentos imobiliários	1520	11
1107	F	41	41.2	41.20-4	4120400	Construção de edifícios	1520	11
1116	F	42	42.1	42.13-8	4213800	Obras de urbanização - ruas, praças e calçadas	1520	11
1119	F	42	42.2	42.21-9	4221901	Construção de barragens e represas para geração de energia elétrica	1520	11
1121	F	42	42.2	42.21-9	4221903	Manutenção de redes de distribuição de energia elétrica	4900	39
1123	F	42	42.2	42.21-9	4221905	Manutenção de estações e redes de telecomunicações	1731	11
1142	F	43	43.1	43.11-8	4311802	Preparação de canteiro e limpeza de terreno	1520	11
1148	F	43	43.1	43.19-3	4319300	Serviços de preparação do terreno não especificados anteriormente	1520	11
1151	F	43	43.2	43.21-5	4321500	Instalação e manutenção elétrica	1731	11
1153	F	43	43.2	43.22-3	4322301	Instalações hidráulicas, sanitárias e de gás	1711	11
1154	F	43	43.2	43.22-3	4322302	Instalação e manutenção de sistemas centrais de ar condicionado, de ventilação e refrigeração	1711	11
1155	F	43	43.2	43.22-3	4322303	Instalações de sistema de prevenção contra incêndio	1711	11
1157	F	43	43.2	43.29-1	4329101	Instalação de painéis publicitários	2741	9
1161	F	43	43.2	43.29-1	4329105	Tratamentos térmicos, acústicos ou de vibração	1740	11
1162	F	43	43.2	43.29-1	4329199	Outras obras de instalações em construções não especificadas anteriormente	1799	11
1165	F	43	43.3	43.30-4	4330401	Impermeabilização em obras de engenharia civil	1520	11
1166	F	43	43.3	43.30-4	4330402	Instalação de portas, janelas, tetos, divisórias e armários embutidos de qualquer material	1799	11
1167	F	43	43.3	43.30-4	4330403	Obras de acabamento em gesso e estuque	1799	11
1168	F	43	43.3	43.30-4	4330404	Serviços de pintura de edifícios em geral	1799	11
1169	F	43	43.3	43.30-4	4330405	Aplicação de revestimentos e de resinas em interiores e exteriores	1799	11
1170	F	43	43.3	43.30-4	4330499	Outras obras de acabamento da construção	1520	11
1175	F	43	43.9	43.99-1	4399101	Administração de obras	1520	11
1177	F	43	43.9	43.99-1	4399103	Obras de alvenaria	1520	11
1179	F	43	43.9	43.99-1	4399105	Perfuração e construção de poços de água	1799	11
1180	F	43	43.9	43.99-1	4399199	Serviços especializados para construção não especificados anteriormente	1520	11
1185	G	45	45.1	45.11-1	4511101	Comércio a varejo de automóveis, camionetas e utilitários novos	5511	6
1186	G	45	45.1	45.11-1	4511102	Comércio a varejo de automóveis, camionetas e utilitários usados	5511	6
1188	G	45	45.1	45.11-1	4511104	Comércio por atacado de caminhões novos e usados	5511	6
1189	G	45	45.1	45.11-1	4511105	Comércio por atacado de reboques e semi-reboques novos e usados	5511	6
1190	G	45	45.1	45.11-1	4511106	Comércio por atacado de ônibus e microônibus novos e usados	5511	6
1192	G	45	45.1	45.12-9	4512901	Representantes comerciais e agentes do comércio de veículos automotores	5511	6
1193	G	45	45.1	45.12-9	4512902	Comércio sob consignação de veículos automotores	5511	6
1196	G	45	45.2	45.20-0	4520001	Serviços de manutenção e reparação mecânica de veículos automotores	7538	7
1229	G	46	46.1	46.12-5	4612500	Representantes comerciais e agentes do comércio de combustíveis, minerais, produtos siderúrgicos e químicos	7399	8
1231	G	46	46.1	46.13-3	4613300	Representantes comerciais e agentes do comércio de madeira, material de construção e ferragens	7399	8
1233	G	46	46.1	46.14-1	4614100	Representantes comerciais e agentes do comércio de máquinas, equipamentos, embarcações e aeronaves	7399	8
1235	G	46	46.1	46.15-0	4615000	Representantes comerciais e agentes do comércio de eletrodomésticos, móveis e artigos de uso doméstico	7399	8
1237	G	46	46.1	46.16-8	4616800	Representantes comerciais e agentes do comércio de têxteis, vestuário, calçados e artigos de viagem	7399	8
1239	G	46	46.1	46.17-6	4617600	Representantes comerciais e agentes do comércio de produtos alimentícios, bebidas e fumo	7399	8
1241	G	46	46.1	46.18-4	4618401	Representantes comerciais e agentes do comércio de medicamentos, cosméticos e produtos de perfumaria	7399	8
1242	G	46	46.1	46.18-4	4618402	Representantes comerciais e agentes do comércio de instrumentos e materiais odonto-médico-hospitalares	7399	8
1243	G	46	46.1	46.18-4	4618403	Representantes comerciais e agentes do comércio de jornais, revistas e outras publicações	7399	8
1246	G	46	46.1	46.19-2	4619200	Representantes comerciais e agentes do comércio de mercadorias em geral não especializado	7399	8
1249	G	46	46.2	46.21-4	4621400	Comércio atacadista de café em grão	7399	8
1251	G	46	46.2	46.22-2	4622200	Comércio atacadista de soja	7399	8
1253	G	46	46.2	46.23-1	4623101	Comércio atacadista de animais vivos	7399	8
1254	G	46	46.2	46.23-1	4623102	Comércio atacadista de couros, lãs, peles e outros subprodutos não-comestíveis de origem animal	7399	8
1255	G	46	46.2	46.23-1	4623103	Comércio atacadista de algodão	7399	8
1256	G	46	46.2	46.23-1	4623104	Comércio atacadista de fumo em folha não beneficiado	7399	8
1257	G	46	46.2	46.23-1	4623105	Comércio atacadista de cacau *	7399	8
1259	G	46	46.2	46.23-1	4623107	Comércio atacadista de sisal	7399	8
1260	G	46	46.2	46.23-1	4623108	Comércio atacadista de matérias-primas agrícolas com atividade de fracionamento e acondicionamento associada	7399	8
1262	G	46	46.2	46.23-1	4623199	Comércio atacadista de matérias-primas agrícolas não especificadas anteriormente	7399	8
1264	G	46	46.3	46.31-1	4631100	Comércio atacadista de leite e laticínios	7399	8
1266	G	46	46.3	46.32-0	4632001	Comércio atacadista de cereais e leguminosas beneficiados	7399	8
1267	G	46	46.3	46.32-0	4632002	Comércio atacadista de farinhas, amidos e féculas	7399	8
1271	G	46	46.3	46.33-8	4633802	Comércio atacadista de aves vivas e ovos	7399	8
1272	G	46	46.3	46.33-8	4633803	Comércio atacadista de coelhos e outros pequenos animais vivos para alimentação	7399	8
1274	G	46	46.3	46.34-6	4634601	Comércio atacadista de carnes bovinas e suínas e derivados	7399	8
1275	G	46	46.3	46.34-6	4634602	Comércio atacadista de aves abatidas e derivados	7399	8
1276	G	46	46.3	46.34-6	4634603	Comércio atacadista de pescados e frutos do mar	7399	8
1277	G	46	46.3	46.34-6	4634699	Comércio atacadista de carnes e derivados de outros animais	7399	8
1197	G	45	45.2	45.20-0	4520002	Serviços de lanternagem ou funilaria e pintura de veículos automotores	7531	36
1198	G	45	45.2	45.20-0	4520003	Serviços de manutenção e reparação elétrica de veículos automotores	7538	7
1199	G	45	45.2	45.20-0	4520004	Serviços de alinhamento e balanceamento de veículos automotores	7538	7
1200	G	45	45.2	45.20-0	4520005	Serviços de lavagem, lubrificação e polimento de veículos automotores	7538	7
1201	G	45	45.2	45.20-0	4520006	Serviços de borracharia para veículos automotores	7534	6
1206	G	45	45.3	45.30-7	4530701	Comércio por atacado de peças e acessórios novos para veículos automotores	5533	7
1207	G	45	45.3	45.30-7	4530702	Comércio por atacado de pneumáticos e câmaras-de-ar	5532	7
1208	G	45	45.3	45.30-7	4530703	Comércio a varejo de peças e acessórios novos para veículos automotores	5533	7
1209	G	45	45.3	45.30-7	4530704	Comércio a varejo de peças e acessórios usados para veículos automotores	5533	7
1210	G	45	45.3	45.30-7	4530705	Comércio a varejo de pneumáticos e câmaras-de-ar	5532	7
1214	G	45	45.4	45.41-2	4541201	Comércio por atacado de motocicletas e motonetas	5571	39
1215	G	45	45.4	45.41-2	4541202	Comércio por atacado de peças e acessórios para motocicletas e motonetas	5571	39
1216	G	45	45.4	45.41-2	4541203	Comércio a varejo de motocicletas e motonetas novas	5571	39
1221	G	45	45.4	45.42-1	4542102	Comércio sob consignação de motocicletas e motonetas	5571	39
1223	G	45	45.4	45.43-9	4543900	Manutenção e reparação de motocicletas e motonetas	5571	39
1244	G	46	46.1	46.18-4	4618499	Outros representantes comerciais e agentes do comércio especializado em produtos não especificados anteriormente	5499	19
1261	G	46	46.2	46.23-1	4623109	Comércio atacadista de alimentos para animais	5995	39
1270	G	46	46.3	46.33-8	4633801	Comércio atacadista de frutas, verduras, raízes, tubérculos, hortaliças e legumes frescos	5499	19
1685	J	58	58.1	58.13-1	5813100	Edição de revistas	2741	9
1967	M	74	74.1	74.10-2	7410202	Decoração de interiores	8911	31
1284	G	46	46.3	46.36-2	4636201	Comércio atacadista de fumo beneficiado	7399	8
1285	G	46	46.3	46.36-2	4636202	Comércio atacadista de cigarros, cigarrilhas e charutos	7399	8
1287	G	46	46.3	46.37-1	4637101	Comércio atacadista de café torrado, moído e solúvel	7399	8
1288	G	46	46.3	46.37-1	4637102	Comércio atacadista de açúcar	7399	8
1289	G	46	46.3	46.37-1	4637103	Comércio atacadista de óleos e gorduras	7399	8
1290	G	46	46.3	46.37-1	4637104	Comércio atacadista de pães, bolos, biscoitos e similares	7399	8
1291	G	46	46.3	46.37-1	4637105	Comércio atacadista de massas alimentícias	7399	8
1292	G	46	46.3	46.37-1	4637106	Comércio atacadista de sorvetes	7399	8
1293	G	46	46.3	46.37-1	4637107	Comércio atacadista de chocolates, confeitos, balas, bombons e semelhantes	7399	8
1310	G	46	46.4	46.44-3	4644301	Comércio atacadista de medicamentos e drogas de uso humano	7399	8
1311	G	46	46.4	46.44-3	4644302	Comércio atacadista de medicamentos e drogas de uso veterinário	7399	8
1343	G	46	46.6	46.61-3	4661300	Comércio atacadista de máquinas, aparelhos e equipamentos para uso agropecuário; partes e peças	7399	8
1351	G	46	46.6	46.65-6	4665600	Comércio atacadista de máquinas e equipamentos para uso comercial; partes e peças	7399	8
1354	G	46	46.6	46.69-9	4669999	Comércio atacadista de outras máquinas e equipamentos não especificados anteriormente; partes e peças	7399	8
1280	G	46	46.3	46.35-4	4635402	Comércio atacadista de cerveja, chope e refrigerante	5411	19
1282	G	46	46.3	46.35-4	4635499	Comércio atacadista de bebidas não especificadas anteriormente	5411	19
1300	G	46	46.4	46.41-9	4641901	Comércio atacadista de tecidos	5949	3
1301	G	46	46.4	46.41-9	4641902	Comércio atacadista de artigos de cama, mesa e banho	5719	24
1302	G	46	46.4	46.41-9	4641903	Comércio atacadista de artigos de armarinho	5131	9
1304	G	46	46.4	46.42-7	4642701	Comércio atacadista de artigos do vestuário e acessórios, exceto profissionais e de segurança	5651	2
1305	G	46	46.4	46.42-7	4642702	Comércio atacadista de roupas e acessórios para uso profissional e de segurança do trabalho	5137	2
1307	G	46	46.4	46.43-5	4643501	Comércio atacadista de calçados	5661	2
1308	G	46	46.4	46.43-5	4643502	Comércio atacadista de bolsas, malas e artigos de viagem	5948	39
1313	G	46	46.4	46.45-1	4645101	Comércio atacadista de instrumentos e materiais para uso médico, cirúrgico, hospitalar e de laboratórios	5047	9
1314	G	46	46.4	46.45-1	4645102	Comércio atacadista de próteses e artigos de ortopedia	5976	21
1315	G	46	46.4	46.45-1	4645103	Comércio atacadista de produtos odontológicos	5047	9
1317	G	46	46.4	46.46-0	4646001	Comércio atacadista de cosméticos e produtos de perfumaria	5977	29
1318	G	46	46.4	46.46-0	4646002	Comércio atacadista de produtos de higiene pessoal	5977	29
1320	G	46	46.4	46.47-8	4647801	Comércio atacadista de artigos de escritório e de papelaria	5111	9
1321	G	46	46.4	46.47-8	4647802	Comércio atacadista de livros, jornais e outras publicações	2741	9
1323	G	46	46.4	46.49-4	4649401	Comércio atacadista de equipamentos elétricos de uso pessoal e doméstico	5732	16
1324	G	46	46.4	46.49-4	4649402	Comércio atacadista de aparelhos eletrônicos de uso pessoal e doméstico	5722	24
1325	G	46	46.4	46.49-4	4649403	Comércio atacadista de bicicletas, triciclos e outros veículos recreativos	5940	32
1326	G	46	46.4	46.49-4	4649404	Comércio atacadista de móveis e artigos de colchoaria	5712	24
1327	G	46	46.4	46.49-4	4649405	Comércio atacadista de artigos de tapeçaria; persianas e cortinas	5714	24
1329	G	46	46.4	46.49-4	4649407	Comércio atacadista de filmes, CDs, DVDs, fitas e discos	5735	17
1331	G	46	46.4	46.49-4	4649408	Comércio atacadista de produtos de higiene, limpeza e conservação domiciliar	2842	9
1332	G	46	46.4	46.49-4	4649409	Comércio atacadista de produtos de higiene, limpeza e conservação domiciliar, com atividade de fracionamento e acondicionamento associada	2842	9
1333	G	46	46.4	46.49-4	4649410	Comércio atacadista de jóias, relógios e bijuterias, inclusive pedras preciosas e semipreciosas lapidadas	5094	39
1334	G	46	46.4	46.49-4	4649499	Comércio atacadista de outros equipamentos e artigos de uso pessoal e doméstico não especificados anteriormente	5722	24
1337	G	46	46.5	46.51-6	4651601	Comércio atacadista de equipamentos de informática	5045	9
1340	G	46	46.5	46.52-4	4652400	Comércio atacadista de componentes eletrônicos e equipamentos de telefonia e comunicação	4812	42
1347	G	46	46.6	46.63-0	4663000	Comércio atacadista de máquinas e equipamentos para uso industrial; partes e peças	5046	9
1349	G	46	46.6	46.64-8	4664800	Comércio atacadista de máquinas, aparelhos e equipamentos para uso odonto-médico-hospitalar; partes e peças	5047	9
1353	G	46	46.6	46.69-9	4669901	Comércio atacadista de bombas e compressores; partes e peças	5085	9
1357	G	46	46.7	46.71-1	4671100	Comércio atacadista de madeira e produtos derivados	5211	23
1359	G	46	46.7	46.72-9	4672900	Comércio atacadista de ferragens e ferramentas	5251	23
1361	G	46	46.7	46.73-7	4673700	Comércio atacadista de material elétrico	5065	9
1363	G	46	46.7	46.74-5	4674500	Comércio atacadista de cimento	5211	23
1365	G	46	46.7	46.79-6	4679601	Comércio atacadista de tintas, vernizes e similares	5198	23
1391	G	46	46.8	46.87-7	4687701	Comércio atacadista de resíduos de papel e papelão	7399	8
1392	G	46	46.8	46.87-7	4687702	Comércio atacadista de resíduos e sucatas não-metálicos, exceto de papel e papelão	7399	8
1393	G	46	46.8	46.87-7	4687703	Comércio atacadista de resíduos e sucatas metálicos	7399	8
1395	G	46	46.8	46.89-3	4689301	Comércio atacadista de produtos da extração mineral, exceto combustíveis	7399	8
1396	G	46	46.8	46.89-3	4689302	Comércio atacadista de fios e fibras têxteis beneficiados *	7399	8
1366	G	46	46.7	46.79-6	4679602	Comércio atacadista de mármores e granitos	5211	23
1367	G	46	46.7	46.79-6	4679603	Comércio atacadista de vidros, espelhos e vitrais	5231	23
1368	G	46	46.7	46.79-6	4679604	Comércio atacadista especializado de materiais de construção não especificados anteriormente	5211	23
1369	G	46	46.7	46.79-6	4679699	Comércio atacadista de materiais de construção em geral	5211	23
1373	G	46	46.8	46.81-8	4681802	Comércio atacadista de combustíveis realizado por transportador retalhista (TRR)	5172	9
1376	G	46	46.8	46.81-8	4681805	Comércio atacadista de lubrificantes	5172	9
1378	G	46	46.8	46.82-6	4682600	Comércio atacadista de gás liqüefeito de petróleo (GLP)	5172	9
1382	G	46	46.8	46.84-2	4684201	Comércio atacadista de resinas e elastômeros	5198	23
1383	G	46	46.8	46.84-2	4684202	Comércio atacadista de solventes	2842	9
1384	G	46	46.8	46.84-2	4684299	Comércio atacadista de outros produtos químicos e petroquímicos não especificados anteriormente	5172	9
1386	G	46	46.8	46.85-1	4685100	Comércio atacadista de produtos siderúrgicos e metalúrgicos, exceto para construção	5211	23
1388	G	46	46.8	46.86-9	4686901	Comércio atacadista de papel e papelão em bruto	5111	9
1389	G	46	46.8	46.86-9	4686902	Comércio atacadista de embalagens	5111	9
1400	G	46	46.9	46.91-5	4691500	Comércio atacadista de mercadorias em geral, com predominância de produtos alimentícios	5300	5
1402	G	46	46.9	46.92-3	4692300	Comércio atacadista de mercadorias em geral, com predominância de insumos agropecuários	5261	23
1408	G	47	47.1	47.11-3	4711301	Comércio varejista de mercadorias em geral, com predominância de produtos alimentícios - hipermercados	5411	19
1409	G	47	47.1	47.11-3	4711302	Comércio varejista de mercadorias em geral, com predominância de produtos alimentícios - supermercados	5411	19
1411	G	47	47.1	47.12-1	4712100	Comércio varejista de mercadorias em geral, com predominância de produtos alimentícios - minimercados, mercearias e armazéns	5411	19
1413	G	47	47.1	47.13-0	4713001	Lojas de departamentos ou magazines	5311	12
1414	G	47	47.1	47.13-0	4713002	Lojas de variedades, exceto lojas de departamentos ou magazines	5651	2
1415	G	47	47.1	47.13-0	4713003	Lojas duty free de aeroportos internacionais	5309	12
1418	G	47	47.2	47.21-1	4721101	Padaria e confeitaria com predominância de produção própria	5462	19
1419	G	47	47.2	47.21-1	4721102	Padaria e confeitaria com predominância de revenda	5462	19
1420	G	47	47.2	47.21-1	4721103	Comércio varejista de laticínios e frios	5462	19
1421	G	47	47.2	47.21-1	4721104	Comércio varejista de doces, balas, bombons e semelhantes	5441	19
1423	G	47	47.2	47.22-9	4722901	Comércio varejista de carnes - açougues	5422	19
1424	G	47	47.2	47.22-9	4722902	Peixaria	5422	19
1426	G	47	47.2	47.23-7	4723700	Comércio varejista de bebidas	5411	19
1428	G	47	47.2	47.24-5	4724500	Comércio varejista de hortifrutigranjeiros	5411	19
1430	G	47	47.2	47.29-6	4729601	Tabacaria	5993	39
1434	G	47	47.3	47.31-8	4731800	Comércio varejista de combustíveis para veículos automotores	5541	7
1436	G	47	47.3	47.32-6	4732600	Comércio varejista de lubrificantes	5541	7
1439	G	47	47.4	47.41-5	4741500	Comércio varejista de tintas e materiais para pintura	5231	23
1441	G	47	47.4	47.42-3	4742300	Comércio varejista de material elétrico	5211	23
1443	G	47	47.4	47.43-1	4743100	Comércio varejista de vidros	5231	23
1445	G	47	47.4	47.44-0	4744001	Comércio varejista de ferragens e ferramentas	5211	23
1446	G	47	47.4	47.44-0	4744002	Comércio varejista de madeira e artefatos	5211	23
1447	G	47	47.4	47.44-0	4744003	Comércio varejista de materiais hidráulicos	5211	23
1448	G	47	47.4	47.44-0	4744004	Comércio varejista de cal, areia, pedra britada, tijolos e telhas	5211	23
1449	G	47	47.4	47.44-0	4744005	Comércio varejista de materiais de construção não especificados anteriormente	5211	23
1450	G	47	47.4	47.44-0	4744099	Comércio varejista de materiais de construção em geral	5211	23
1453	G	47	47.5	47.51-2	4751200	Comércio varejista especializado de equipamentos e suprimentos de informática	5045	9
1455	G	47	47.5	47.52-1	4752100	Comércio varejista especializado de equipamentos de telefonia e comunicação	4812	42
1457	G	47	47.5	47.53-9	4753900	Comércio varejista especializado de eletrodomésticos e equipamentos de áudio e vídeo	5722	24
1459	G	47	47.5	47.54-7	4754701	Comércio varejista de móveis	5712	24
1460	G	47	47.5	47.54-7	4754702	Comércio varejista de artigos de colchoaria	5712	24
1683	J	58	58.1	58.12-3	5812300	Edição de jornais	2741	9
1511	G	47	47.8	47.85-7	4785799	Comércio varejista de outros artigos usados	7399	8
1521	G	47	47.8	47.89-0	4789009	Comércio varejista de armas e munições	7399	8
1522	G	47	47.8	47.89-0	4789099	Comércio varejista de outros produtos não especificados anteriormente	7399	8
1461	G	47	47.5	47.54-7	4754703	Comércio varejista de artigos de iluminação	5200	24
1463	G	47	47.5	47.55-5	4755501	Comércio varejista de tecidos	5949	3
1464	G	47	47.5	47.55-5	4755502	Comercio varejista de artigos de armarinho	5947	39
1465	G	47	47.5	47.55-5	4755503	Comercio varejista de artigos de cama, mesa e banho	5719	24
1475	G	47	47.6	47.61-0	4761001	Comércio varejista de livros	5942	39
1476	G	47	47.6	47.61-0	4761002	Comércio varejista de jornais e revistas	2741	9
1477	G	47	47.6	47.61-0	4761003	Comércio varejista de artigos de papelaria	5111	9
1479	G	47	47.6	47.62-8	4762800	Comércio varejista de discos, CDs, DVDs e fitas	5735	17
1481	G	47	47.6	47.63-6	4763601	Comércio varejista de brinquedos e artigos recreativos	5945	3
1482	G	47	47.6	47.63-6	4763602	Comércio varejista de artigos esportivos	5941	39
1483	G	47	47.6	47.63-6	4763603	Comércio varejista de bicicletas e triciclos; peças e acessórios	5940	32
1484	G	47	47.6	47.63-6	4763604	Comércio varejista de artigos de caça, pesca e camping	5941	39
1485	G	47	47.6	47.63-6	4763605	Comércio varejista de embarcações e outros veículos recreativos; peças e acessórios	4457	34
1489	G	47	47.7	47.71-7	4771702	Comércio varejista de produtos farmacêuticos, com manipulação de fórmulas	5912	21
1490	G	47	47.7	47.71-7	4771703	Comércio varejista de produtos farmacêuticos homeopáticos	5912	21
1491	G	47	47.7	47.71-7	4771704	Comércio varejista de medicamentos veterinários	5995	39
1493	G	47	47.7	47.72-5	4772500	Comércio varejista de cosméticos, produtos de perfumaria e de higiene pessoal	5977	29
1495	G	47	47.7	47.73-3	4773300	Comércio varejista de artigos médicos e ortopédicos	5047	9
1497	G	47	47.7	47.74-1	4774100	Comércio varejista de artigos de óptica	8043	21
1500	G	47	47.8	47.81-4	4781400	Comércio varejista de artigos do vestuário e acessórios	5651	2
1502	G	47	47.8	47.82-2	4782201	Comércio varejista de calçados	5661	2
1503	G	47	47.8	47.82-2	4782202	Comércio varejista de artigos de viagem	5948	39
1506	G	47	47.8	47.83-1	4783102	Comércio varejista de artigos de relojoaria	5944	39
1508	G	47	47.8	47.84-9	4784900	Comércio varejista de gás liqüefeito de petróleo (GLP)	5541	7
1510	G	47	47.8	47.85-7	4785701	Comércio varejista de antigüidades	5971	4
1513	G	47	47.8	47.89-0	4789001	Comércio varejista de suvenires, bijuterias e artesanatos	5631	2
1514	G	47	47.8	47.89-0	4789002	Comércio varejista de plantas e flores naturais	5992	39
1515	G	47	47.8	47.89-0	4789003	Comércio varejista de objetos de arte	5971	4
1517	G	47	47.8	47.89-0	4789005	Comércio varejista de produtos saneantes domissanitários	2842	9
1518	G	47	47.8	47.89-0	4789006	Comércio varejista de fogos de artifício e artigos pirotécnicos	5999	39
1519	G	47	47.8	47.89-0	4789007	Comércio varejista de equipamentos para escritório	5111	9
1520	G	47	47.8	47.89-0	4789008	Comércio varejista de artigos fotográficos e para filmagem	7221	30
1530	H	49	49.1	49.12-4	4912401	Transporte ferroviário de passageiros intermunicipal e interestadual	4112	43
1531	H	49	49.1	49.12-4	4912402	Transporte ferroviário de passageiros municipal e em região metropolitana	4112	43
1532	H	49	49.1	49.12-4	4912403	Transporte metroviário	4112	43
1535	H	49	49.2	49.21-3	4921301	Transporte rodoviário coletivo de passageiros, com itinerário fixo, municipal	4131	43
1536	H	49	49.2	49.21-3	4921302	Transporte rodoviário coletivo de passageiros, com itinerário fixo, intermunicipal em região metropolitana	4131	43
1539	H	49	49.2	49.22-1	4922102	Transporte rodoviário coletivo de passageiros, com itinerário fixo, interestadual	4131	43
1540	H	49	49.2	49.22-1	4922103	Transporte rodoviário coletivo de passageiros, com itinerário fixo, internacional	4131	43
1542	H	49	49.2	49.23-0	4923001	Serviço de táxi	4121	43
1543	H	49	49.2	49.23-0	4923002	Serviço de transporte de passageiros - locação de automóveis com motorista	4789	43
1545	H	49	49.2	49.24-8	4924800	Transporte escolar	4789	43
1547	H	49	49.2	49.29-9	4929901	Transporte rodoviário coletivo de passageiros, sob regime de fretamento, municipal	4131	43
1549	H	49	49.2	49.29-9	4929903	Organização de excursões em veículos rodoviários próprios, municipal	4131	43
1550	H	49	49.2	49.29-9	4929904	Organização de excursões em veículos rodoviários próprios, intermunicipal, interestadual e internacional	4131	43
1551	H	49	49.2	49.29-9	4929999	Outros transportes rodoviários de passageiros não especificados anteriormente	4131	43
1554	H	49	49.3	49.30-2	4930201	Transporte rodoviário de carga, exceto produtos perigosos e mudanças, municipal	4789	43
1555	H	49	49.3	49.30-2	4930202	Transporte rodoviário de carga, exceto produtos perigosos e mudanças, intermunicipal, interestadual e internacional	4789	43
1602	H	51	51.3	51.30-7	5130700	Transporte espacial	7399	8
1606	H	52	52.1	52.11-7	5211701	Armazéns gerais - emissão de warrant	7399	8
1607	H	52	52.1	52.11-7	5211702	Guarda-móveis	7399	8
1608	H	52	52.1	52.11-7	5211799	Depósitos de mercadorias para terceiros, exceto armazéns gerais e guarda-móveis	7399	8
1610	H	52	52.1	52.12-5	5212500	Carga e descarga	7399	8
1624	H	52	52.3	52.31-1	5231101	Administração da infra-estrutura portuária	7399	8
1625	H	52	52.3	52.31-1	5231102	Operações de terminais	7399	8
1627	H	52	52.3	52.32-0	5232000	Atividades de agenciamento marítimo	7399	8
1632	H	52	52.4	52.40-1	5240101	Operação dos aeroportos e campos de aterrissagem	7399	8
1633	H	52	52.4	52.40-1	5240199	Atividades auxiliares dos transportes aéreos, exceto operação dos aeroportos e campos de aterrissagem	7399	8
1673	I	56	56.2	56.20-1	5620101	Fornecimento de alimentos preparados preponderantemente para empresas	7399	8
1700	J	59	59.1	59.11-1	5911101	Estúdios cinematográficos	7399	8
1557	H	49	49.3	49.30-2	4930204	Transporte rodoviário de mudanças	4789	43
1560	H	49	49.4	49.40-0	4940000	Transporte dutoviário	4789	43
1563	H	49	49.5	49.50-7	4950700	Trens turísticos, teleféricos e similares	4789	43
1567	H	50	50.1	50.11-4	5011401	Transporte marítimo de cabotagem - Carga	4789	43
1568	H	50	50.1	50.11-4	5011402	Transporte marítimo de cabotagem - passageiros	4411	40
1570	H	50	50.1	50.12-2	5012201	Transporte marítimo de longo curso - Carga	4789	43
1574	H	50	50.2	50.21-1	5021101	Transporte por navegação interior de carga, municipal, exceto travessia	4789	43
1575	H	50	50.2	50.21-1	5021102	Transporte por navegação interior de carga, intermunicipal, interestadual e internacional, exceto travessia	4789	43
1577	H	50	50.2	50.22-0	5022001	Transporte por navegação interior de passageiros em linhas regulares, municipal, exceto travessia	4789	43
1581	H	50	50.3	50.30-1	5030101	Navegação de apoio marítimo	4789	43
1582	H	50	50.3	50.30-1	5030102	Navegação de apoio portuário	4789	43
1585	H	50	50.9	50.91-2	5091201	Transporte por navegação de travessia, municipal	4789	43
1852	K	65	65.3	65.30-8	6530800	Resseguros	6300	25
1586	H	50	50.9	50.91-2	5091202	Transporte por navegação de travessia, intermunicipal	4789	43
1588	H	50	50.9	50.99-8	5099801	Transporte aquaviário para passeios turísticos	4789	43
1589	H	50	50.9	50.99-8	5099899	Outros transportes aquaviários não especificados anteriormente	4789	43
1593	H	51	51.1	51.11-1	5111100	Transporte aéreo de passageiros regular	\N	\N
1595	H	51	51.1	51.12-9	5112901	Serviço de táxi aéreo e locação de aeronaves com tripulação	4511	40
1596	H	51	51.1	51.12-9	5112999	Outros serviços de transporte aéreo de passageiros não-regular	4511	40
1599	H	51	51.2	51.20-0	5120000	Transporte aéreo de carga	4215	43
1613	H	52	52.2	52.21-4	5221400	Concessionárias de rodovias, pontes, túneis e serviços relacionados	4784	43
1615	H	52	52.2	52.22-2	5222200	Terminais rodoviários e ferroviários	4111	43
1617	H	52	52.2	52.23-1	5223100	Estacionamento de veículos	7523	7
1619	H	52	52.2	52.29-0	5229001	Serviços de apoio ao transporte por táxi, inclusive centrais de chamada	4121	43
1620	H	52	52.2	52.29-0	5229002	Serviços de reboque de veículos	7549	7
1637	H	52	52.5	52.50-8	5250802	Atividades de despachantes aduaneiros	9311	20
1638	H	52	52.5	52.50-8	5250803	Agenciamento de cargas, exceto para o transporte marítimo	4215	43
1639	H	52	52.5	52.50-8	5250804	Organização logística do transporte de carga	4215	43
1640	H	52	52.5	52.50-8	5250805	Operador de transporte multimodal - OTM	4215	43
1644	H	53	53.1	53.10-5	5310501	Atividades do Correio Nacional	9402	20
1645	H	53	53.1	53.10-5	5310502	Atividades de  franqueadas e permissionárias do Correio Nacional	9402	20
1648	H	53	53.2	53.20-2	5320201	Serviços de malote não realizados pelo Correio Nacional	7399	8
1649	H	53	53.2	53.20-2	5320202	Serviços de entrega rápida	7399	8
1655	I	55	55.1	55.10-8	5510802	Apart-hotéis	7011	40
1656	I	55	55.1	55.10-8	5510803	Motéis	7011	40
1659	I	55	55.9	55.90-6	5590601	Albergues, exceto assistenciais	7011	40
1660	I	55	55.9	55.90-6	5590602	Campings	7033	32
1661	I	55	55.9	55.90-6	5590603	Pensões	7011	40
1666	I	56	56.1	56.11-2	5611201	Restaurantes e similares	5812	14
1667	I	56	56.1	56.11-2	5611202	Bares e outros estabelecimentos especializados em servir bebidas	5813	14
1668	I	56	56.1	56.11-2	5611203	Lanchonetes, casas de chá, de sucos e similares	5814	14
1670	I	56	56.1	56.12-1	5612100	Serviços ambulantes de alimentação	5814	14
1674	I	56	56.2	56.20-1	5620102	Serviços de alimentação para eventos e recepções - bufê	5811	14
1675	I	56	56.2	56.20-1	5620103	Cantinas - serviços de alimentação privativos	5814	14
1692	J	58	58.2	58.22-1	5822100	Edição integrada à impressão de jornais	2741	9
1694	J	58	58.2	58.23-9	5823900	Edição integrada à impressão de revistas	2741	9
1701	J	59	59.1	59.11-1	5911102	Produção de filmes para publicidade	7399	8
1702	J	59	59.1	59.11-1	5911199	Atividades de produção cinematográfica, de vídeos e de programas de televisão não especificadas anteriormente	7399	8
1704	J	59	59.1	59.12-0	5912001	Serviços de dublagem	7399	8
1705	J	59	59.1	59.12-0	5912002	Serviços de mixagem sonora	7399	8
1706	J	59	59.1	59.12-0	5912099	Atividades de pós-produção cinematográfica, de vídeos e de programas de televisão não especificadas anteriormente	7399	8
1708	J	59	59.1	59.13-8	5913800	Distribuição cinematográfica, de vídeo e de programas de televisão	7399	8
1710	J	59	59.1	59.14-6	5914600	Atividades de exibição cinematográfica	7399	8
1713	J	59	59.2	59.20-1	5920100	Atividades de gravação de som e de edição de música	7399	8
1717	J	60	60.1	60.10-1	6010100	Atividades de rádio	7399	8
1720	J	60	60.2	60.21-7	6021700	Atividades de televisão aberta	7399	8
1778	K	64	64.1	64.10-7	6410700	Banco Central	7399	8
1781	K	64	64.2	64.21-2	6421200	Bancos comerciais	7399	8
1783	K	64	64.2	64.22-1	6422100	Bancos múltiplos, com carteira comercial	7399	8
1785	K	64	64.2	64.23-9	6423900	Caixas econômicas	7399	8
1787	K	64	64.2	64.24-7	6424701	Bancos cooperativos	7399	8
1788	K	64	64.2	64.24-7	6424702	Cooperativas centrais de crédito	7399	8
1789	K	64	64.2	64.24-7	6424703	Cooperativas de crédito mútuo	7399	8
1793	K	64	64.3	64.31-0	6431000	Bancos múltiplos, sem carteira comercial	7399	8
1795	K	64	64.3	64.32-8	6432800	Bancos de investimento	7399	8
1797	K	64	64.3	64.33-6	6433600	Bancos de desenvolvimento	7399	8
1799	K	64	64.3	64.34-4	6434400	Agências de fomento	7399	8
1801	K	64	64.3	64.35-2	6435201	Sociedades de crédito imobiliário	7399	8
1802	K	64	64.3	64.35-2	6435202	Associações de poupança e empréstimo	7399	8
1803	K	64	64.3	64.35-2	6435203	Companhias hipotecárias	7399	8
1807	K	64	64.3	64.37-9	6437900	Sociedades de crédito ao microempreendedor	7399	8
1810	K	64	64.4	64.40-9	6440900	Arrendamento mercantil	7399	8
1813	K	64	64.5	64.50-6	6450600	Sociedades de capitalização	7399	8
1816	K	64	64.6	64.61-1	6461100	Holdings de instituições financeiras	7399	8
1818	K	64	64.6	64.62-0	6462000	Holdings de instituições não-financeiras	7399	8
1824	K	64	64.7	64.70-1	6470102	Fundos de investimento previdenciários	7399	8
1825	K	64	64.7	64.70-1	6470103	Fundos de investimento imobiliários	7399	8
1828	K	64	64.9	64.91-3	6491300	Sociedades de fomento mercantil - factoring	7399	8
1830	K	64	64.9	64.92-1	6492100	Securitização de créditos	7399	8
1834	K	64	64.9	64.99-9	6499901	Clubes de investimento	7399	8
1835	K	64	64.9	64.99-9	6499902	Sociedades de investimento	7399	8
1836	K	64	64.9	64.99-9	6499903	Fundo garantidor de crédito	7399	8
1722	J	60	60.2	60.22-5	6022501	Programadoras	4899	41
1723	J	60	60.2	60.22-5	6022502	Atividades relacionadas à televisão por assinatura, exceto programadoras	4899	41
1727	J	61	61.1	61.10-8	6110801	Serviços de telefonia fixa comutada - STFC	4814	42
1729	J	61	61.1	61.10-8	6110803	Serviços de comunicação multimídia - SMC	4814	42
1730	J	61	61.1	61.10-8	6110899	Serviços de telecomunicações por fio não especificados anteriormente	4814	42
1733	J	61	61.2	61.20-5	6120501	Telefonia móvel celular	4814	42
1734	J	61	61.2	61.20-5	6120502	Serviço móvel especializado - SME	4814	42
1735	J	61	61.2	61.20-5	6120599	Serviços de telecomunicações sem fio não especificados anteriormente	4814	42
1738	J	61	61.3	61.30-2	6130200	Telecomunicações por satélite	4814	42
1741	J	61	61.4	61.41-8	6141800	Operadoras de televisão por assinatura por cabo	4899	41
1743	J	61	61.4	61.42-6	6142600	Operadoras de televisão por assinatura por microondas	4899	41
1745	J	61	61.4	61.43-4	6143400	Operadoras de televisão por assinatura por satélite	4899	41
1748	J	61	61.9	61.90-6	6190601	Provedores de acesso às redes de comunicações	4816	42
1749	J	61	61.9	61.90-6	6190602	Provedores de voz sobre protocolo internet - VOIP	4816	42
1754	J	62	62.0	62.01-5	6201500	Desenvolvimento de programas de computador sob encomenda	7372	31
1758	J	62	62.0	62.03-1	6203100	Desenvolvimento e licenciamento de programas de computador não-customizáveis	7372	31
1760	J	62	62.0	62.04-0	6204000	Consultoria em tecnologia da informação	7379	9
1762	J	62	62.0	62.09-1	6209100	Suporte técnico, manutenção e outros serviços em tecnologia da informação	7379	9
1766	J	63	63.1	63.11-9	6311900	Tratamento de dados, provedores de serviços de aplicação e serviços de hospedagem na internet	4816	42
1768	J	63	63.1	63.19-4	6319400	Portais, provedores de conteúdo e outros serviços de informação na internet	4816	42
1771	J	63	63.9	63.91-7	6391700	Agências de notícias	4816	42
1773	J	63	63.9	63.99-2	6399200	Outras atividades de prestação de serviços de informação não especificadas anteriormente	4816	42
1837	K	64	64.9	64.99-9	6499904	Caixas de financiamento de corporações	7399	8
1838	K	64	64.9	64.99-9	6499905	Concessão de crédito pelas OSCIP	7399	8
1865	K	66	66.1	66.11-8	6611801	Bolsa de valores	7399	8
1867	K	66	66.1	66.11-8	6611803	Bolsa de mercadorias e futuros	7399	8
1868	K	66	66.1	66.11-8	6611804	Administração de mercados de balcão organizados	7399	8
1870	K	66	66.1	66.12-6	6612601	Corretoras de títulos e valores mobiliários	7399	8
1871	K	66	66.1	66.12-6	6612602	Distribuidoras de títulos e valores mobiliários	7399	8
1872	K	66	66.1	66.12-6	6612603	Corretoras de câmbio	7399	8
1873	K	66	66.1	66.12-6	6612604	Corretoras de contratos de mercadorias	7399	8
1878	K	66	66.1	66.19-3	6619301	Serviços de liquidação e custódia	7399	8
1879	K	66	66.1	66.19-3	6619302	Correspondentes de instituições financeiras	7399	8
1880	K	66	66.1	66.19-3	6619303	Representações de bancos estrangeiros	7399	8
1881	K	66	66.1	66.19-3	6619304	Caixas eletrônicos	7399	8
1882	K	66	66.1	66.19-3	6619305	Operadoras de cartões de débito	7399	8
1883	K	66	66.1	66.19-3	6619399	Outras atividades auxiliares dos serviços financeiros não especificadas anteriormente	7399	8
1887	K	66	66.2	66.21-5	6621502	Auditoria e consultoria atuarial	7399	8
1894	K	66	66.3	66.30-4	6630400	Atividades de administração de fundos por contrato ou comissão	7399	8
1913	M	69	69.1	69.11-7	6911703	Agente de propriedade industrial	7399	8
1933	M	71	71.1	71.19-7	7119701	Serviços de cartografia, topografia e geodésia	7399	8
1934	M	71	71.1	71.19-7	7119702	Atividades de estudos geológicos	7399	8
1936	M	71	71.1	71.19-7	7119704	Serviços de perícia técnica relacionados à segurança do trabalho	7399	8
1940	M	71	71.2	71.20-1	7120100	Testes e análises técnicas	7399	8
1944	M	72	72.1	72.10-0	7210000	Pesquisa e desenvolvimento experimental em ciências físicas e naturais	7399	8
1947	M	72	72.2	72.20-7	7220700	Pesquisa e desenvolvimento experimental em ciências sociais e humanas	7399	8
1823	K	64	64.7	64.70-1	6470101	Fundos de investimento, exceto previdenciários e imobiliários	7399	8
1959	M	73	73.1	73.19-0	7319099	Outras atividades de publicidade não especificadas anteriormente	7399	8
1962	M	73	73.2	73.20-3	7320300	Pesquisas de mercado e de opinião pública	7399	8
1843	K	65	65.1	65.11-1	6511101	Seguros de vida	6300	25
1844	K	65	65.1	65.11-1	6511102	Planos de auxílio-funeral	6300	25
1846	K	65	65.1	65.12-0	6512000	Seguros não-vida	6300	25
1849	K	65	65.2	65.20-1	6520100	Seguros-saúde	6300	25
1855	K	65	65.4	65.41-3	6541300	Previdência complementar fechada	6300	25
1858	K	65	65.4	65.42-1	6542100	Previdência complementar aberta	6300	25
1861	K	65	65.5	65.50-2	6550200	Planos de saúde	6300	25
1876	K	66	66.1	66.13-4	6613400	Administração de cartões de crédito	6051	18
1886	K	66	66.2	66.21-5	6621501	Peritos e avaliadores de seguros	6300	25
1889	K	66	66.2	66.22-3	6622300	Corretores e agentes de seguros, de planos de previdência complementar e de saúde	6300	25
1899	L	68	68.1	68.10-2	6810201	Compra e venda de imóveis próprios	1520	11
1900	L	68	68.1	68.10-2	6810202	Aluguel de imóveis próprios	6513	34
1903	L	68	68.2	68.21-8	6821801	Corretagem na compra e venda e avaliação de imóveis	1520	11
1904	L	68	68.2	68.21-8	6821802	Corretagem no aluguel de imóveis	6513	34
1906	L	68	68.2	68.22-6	6822600	Gestão e administração da propriedade imobiliária*	6513	34
1911	M	69	69.1	69.11-7	6911701	Serviços advocatícios	8111	26
1912	M	69	69.1	69.11-7	6911702	Atividades auxiliares da justiça	8111	26
1915	M	69	69.1	69.12-5	6912500	Cartórios	5999	39
1918	M	69	69.2	69.20-6	6920601	Atividades de contabilidade	8931	31
1919	M	69	69.2	69.20-6	6920602	Atividades de consultoria e auditoria contábil e tributária	8931	31
1929	M	71	71.1	71.11-1	7111100	Serviços de arquitetura	8911	31
1931	M	71	71.1	71.12-0	7112000	Serviços de engenharia	8911	31
1937	M	71	71.1	71.19-7	7119799	Atividades técnicas relacionadas à engenharia e arquitetura não especificadas anteriormente	8911	31
1956	M	73	73.1	73.19-0	7319002	Promoção de vendas	5969	13
1957	M	73	73.1	73.19-0	7319003	Marketing direto	5969	13
1958	M	73	73.1	73.19-0	7319004	Consultoria em publicidade	7311	31
1966	M	74	74.1	74.10-2	7410201	Design	8911	31
1970	M	74	74.2	74.20-0	7420001	Atividades de produção de fotografias, exceto aérea e submarina	7221	30
1971	M	74	74.2	74.20-0	7420002	Atividades de produção de fotografias aéreas e submarinas	7221	30
1972	M	74	74.2	74.20-0	7420003	Laboratórios fotográficos	7221	30
1973	M	74	74.2	74.20-0	7420004	Filmagem de festas e eventos	7221	30
1974	M	74	74.2	74.20-0	7420005	Serviços de microfilmagem	7221	30
1980	M	74	74.9	74.90-1	7490104	Atividades de intermediação e agenciamento de serviços e negócios em geral, exceto imobiliários	7399	8
1982	M	74	74.9	74.90-1	7490199	Outras atividades profissionais, científicas e técnicas não especificadas anteriormente	7399	8
1994	N	77	77.1	77.19-5	7719502	Locação de aeronaves sem tripulação	7399	8
2004	N	77	77.2	77.29-2	7729201	Aluguel de aparelhos de jogos eletrônicos	7399	8
2006	N	77	77.2	77.29-2	7729203	Aluguel de material médico*	7399	8
2007	N	77	77.2	77.29-2	7729299	Aluguel de outros objetos pessoais e domésticos não especificados anteriormente	7399	8
2010	N	77	77.3	77.31-4	7731400	Aluguel de máquinas e equipamentos agrícolas sem operador	7399	8
2012	N	77	77.3	77.32-2	7732201	Aluguel de máquinas e equipamentos para construção sem operador, exceto andaimes	7399	8
2013	N	77	77.3	77.32-2	7732202	Aluguel de andaimes	7399	8
2015	N	77	77.3	77.33-1	7733100	Aluguel de máquinas e equipamentos para escritórios	7399	8
2017	N	77	77.3	77.39-0	7739001	Aluguel de máquinas e equipamentos para extração de minérios e petróleo, sem operador	7399	8
2018	N	77	77.3	77.39-0	7739002	Aluguel de equipamentos científicos, médicos e hospitalares, sem operador	7399	8
2020	N	77	77.3	77.39-0	7739099	Aluguel de outras máquinas e equipamentos comerciais e industriais não especificados anteriormente, sem operador	7399	8
2023	N	77	77.4	77.40-3	7740300	Gestão de ativos intangíveis não-financeiros	7399	8
2033	N	78	78.3	78.30-2	7830200	Fornecimento e gestão de recursos humanos para terceiros	7399	8
2046	N	80	80.1	80.11-1	8011101	Atividades de vigilância e segurança privada	7399	8
2049	N	80	80.1	80.12-9	8012900	Atividades de transporte de valores	7399	8
2090	N	82	82.9	82.92-0	8292000	Envasamento e empacotamento sob contrato	7399	8
2093	N	82	82.9	82.99-7	8299702	Emissão de vales-alimentação, vales-transporte e similares	7399	8
2096	N	82	82.9	82.99-7	8299705	Serviços de levantamento de fundos sob contrato	7399	8
1977	M	74	74.9	74.90-1	7490101	Serviços de tradução, interpretação e similares	7399	8
1978	M	74	74.9	74.90-1	7490102	Escafandria e mergulho	4722	43
1981	M	74	74.9	74.90-1	7490105	Agenciamento de profissionais para atividades esportivas, culturais e artísticas	8999	31
1986	M	75	75.0	75.00-1	7500100	Atividades veterinárias	742	31
1991	N	77	77.1	77.11-0	7711000	Locação de automóveis sem condutor	\N	\N
1993	N	77	77.1	77.19-5	7719501	Locação de embarcações sem tripulação, exceto para fins recreativos	4457	34
1995	N	77	77.1	77.19-5	7719599	Locação de outros meios de transporte não especificados anteriormente, sem condutor	7512	40
1998	N	77	77.2	77.21-7	7721700	Aluguel de equipamentos recreativos e esportivos	5941	39
2000	N	77	77.2	77.22-5	7722500	Aluguel de fitas de vídeo, DVDs e similares	7841	34
2002	N	77	77.2	77.23-3	7723300	Aluguel de objetos do vestuário, jóias e acessórios	7296	34
2019	N	77	77.3	77.39-0	7739003	Aluguel de palcos, coberturas e outras estruturas de uso temporário, exceto andaimes	7394	34
2027	N	78	78.1	78.10-8	7810800	Seleção e agenciamento de mão-de-obra	7361	29
2030	N	78	78.2	78.20-5	7820500	Locação de mão-de-obra temporária	7361	29
2037	N	79	79.1	79.11-2	7911200	Agências de viagens	4722	43
2039	N	79	79.1	79.12-1	7912100	Operadores turísticos	4722	43
2047	N	80	80.1	80.11-1	8011102	Serviços de adestramento de cães de guarda	5995	39
2052	N	80	80.2	80.20-0	8020000	Atividades de monitoramento de sistemas de segurança	5732	16
2055	N	80	80.3	80.30-7	8030700	Atividades de investigação particular	7393	38
2061	N	81	81.1	81.12-5	8112500	Condomínios prediais	8699	5
2064	N	81	81.2	81.21-4	8121400	Limpeza em prédios e em domicílios	5722	24
2066	N	81	81.2	81.22-2	8122200	Imunização e controle de pragas urbanas	2842	9
2068	N	81	81.2	81.29-0	8129000	Atividades de limpeza não especificadas anteriormente	5722	24
2071	N	81	81.3	81.30-3	8130300	Atividades paisagísticas	8911	31
2075	N	82	82.1	82.11-3	8211300	Serviços combinados de escritório e apoio administrativo	8931	31
2077	N	82	82.1	82.19-9	8219901	Fotocópias	7338	30
2084	N	82	82.3	82.30-0	8230001	Serviços de organização de feiras, congressos, exposições e festas	5811	14
2085	N	82	82.3	82.30-0	8230002	Casas de festas e eventos	5811	14
2088	N	82	82.9	82.91-1	8291100	Atividades de cobranças e informações cadastrais	7399	8
2092	N	82	82.9	82.99-7	8299701	Medição de consumo de energia elétrica, gás e água	4900	39
2095	N	82	82.9	82.99-7	8299704	Leiloeiros independentes	8999	31
2097	N	82	82.9	82.99-7	8299706	Casas lotéricas	7995	32
2099	N	82	82.9	82.99-7	8299799	Outras atividades de serviços prestados principalmente às empresas não especificadas anteriormente	7299	29
2104	O	84	84.1	84.11-6	8411600	Administração pública em geral	9311	20
2106	O	84	84.1	84.12-4	8412400	Regulação das atividades de saúde, educação, serviços culturais e outros serviços sociais	9311	20
2108	O	84	84.1	84.13-2	8413200	Regulação das atividades econômicas	9311	20
2113	O	84	84.2	84.21-3	8421300	Relações exteriores	7399	8
2115	O	84	84.2	84.22-1	8422100	Defesa	7399	8
2119	O	84	84.2	84.24-8	8424800	Segurança e ordem pública	7399	8
2121	O	84	84.2	84.25-6	8425600	Defesa Civil	7399	8
2124	O	84	84.3	84.30-2	8430200	Seguridade social obrigatória	7399	8
2117	O	84	84.2	84.23-0	8423000	Justiça	9211	26
2129	P	85	85.1	85.11-2	8511200	Educação infantil - creche	8351	29
2133	P	85	85.1	85.13-9	8513900	Ensino fundamental	8211	15
2136	P	85	85.2	85.20-1	8520100	Ensino médio	8211	15
2139	P	85	85.3	85.31-7	8531700	Educação superior - graduação	8220	15
2141	P	85	85.3	85.32-5	8532500	Educação superior - graduação e pós-graduação	8220	15
2143	P	85	85.3	85.33-3	8533300	Educação superior - pós-graduação e extensão	8220	15
2146	P	85	85.4	85.41-4	8541400	Educação profissional de nível técnico	8249	15
2148	P	85	85.4	85.42-2	8542200	Educação profissional de nível tecnológico	8249	15
2151	P	85	85.5	85.50-3	8550301	Administração de caixas escolares	8299	15
2152	P	85	85.5	85.50-3	8550302	Serviços auxiliares à educação	8299	15
2155	P	85	85.9	85.91-1	8591100	Ensino de esportes	8299	15
2158	P	85	85.9	85.92-9	8592902	Ensino de artes cênicas, exceto dança	8299	15
2159	P	85	85.9	85.92-9	8592903	Ensino de música	8299	15
2160	P	85	85.9	85.92-9	8592999	Ensino de arte e cultura não especificado anteriormente	8299	15
2162	P	85	85.9	85.93-7	8593700	Ensino de idiomas	8299	15
2164	P	85	85.9	85.99-6	8599601	Formação de condutores	8299	15
2165	P	85	85.9	85.99-6	8599602	Cursos de pilotagem	8299	15
2166	P	85	85.9	85.99-6	8599603	Treinamento em informática	8244	15
2168	P	85	85.9	85.99-6	8599605	Cursos preparatórios para concursos	8299	15
2174	Q	86	86.1	86.10-1	8610101	Atividades de atendimento hospitalar, exceto pronto-socorro e unidades para atendimento a urgências	8062	21
2175	Q	86	86.1	86.10-1	8610102	Atividades de atendimento em pronto-socorro e unidades hospitalares para atendimento a urgências	8062	21
2178	Q	86	86.2	86.21-6	8621601	UTI móvel	8062	21
2179	Q	86	86.2	86.21-6	8621602	Serviços móveis de atendimento a urgências, exceto por UTI móvel	8062	21
2184	Q	86	86.3	86.30-5	8630501	Atividade médica ambulatorial com recursos para realização de procedimentos cirúrgicos	8062	21
2185	Q	86	86.3	86.30-5	8630502	Atividade médica ambulatorial com recursos para realização de exames complementares	8062	21
2186	Q	86	86.3	86.30-5	8630503	Atividade médica ambulatorial restrita a consultas	8062	21
2187	Q	86	86.3	86.30-5	8630504	Atividade odontológica com recursos para realização de procedimentos cirúrgicos	8021	21
2189	Q	86	86.3	86.30-5	8630506	Serviços de vacinação e imunização humana	8062	21
2190	Q	86	86.3	86.30-5	8630507	Atividades de reprodução humana assistida	8099	21
2191	Q	86	86.3	86.30-5	8630599	Atividades de atenção ambulatorial não especificadas anteriormente	8062	21
2194	Q	86	86.4	86.40-2	8640201	Laboratórios de anatomia patológica e citológica	8071	21
2195	Q	86	86.4	86.40-2	8640202	Laboratórios clínicos	8071	21
2196	Q	86	86.4	86.40-2	8640203	Serviços de diálise e nefrologia	8071	21
2197	Q	86	86.4	86.40-2	8640204	Serviços de tomografia	8071	21
2198	Q	86	86.4	86.40-2	8640205	Serviços de diagnóstico por imagem com uso de radiação ionizante, exceto tomografia	8071	21
2199	Q	86	86.4	86.40-2	8640206	Serviços de ressonância magnética	8071	21
2202	Q	86	86.4	86.40-2	8640209	Serviços de diagnóstico por métodos ópticos - endoscopia e outros exames análogos	8071	21
2203	Q	86	86.4	86.40-2	8640210	Serviços de quimioterapia	8062	21
2204	Q	86	86.4	86.40-2	8640211	Serviços de radioterapia	8062	21
2205	Q	86	86.4	86.40-2	8640212	Serviços de hemoterapia	8062	21
2206	Q	86	86.4	86.40-2	8640213	Serviços de litotripsia	8062	21
2207	Q	86	86.4	86.40-2	8640214	Serviços de bancos de células e tecidos humanos	8062	21
2211	Q	86	86.5	86.50-0	8650001	Atividades de enfermagem	8062	21
2212	Q	86	86.5	86.50-0	8650002	Atividades de profissionais da nutrição	8099	21
2213	Q	86	86.5	86.50-0	8650003	Atividades de psicologia e psicanálise	8099	21
2214	Q	86	86.5	86.50-0	8650004	Atividades de fisioterapia	8099	21
2215	Q	86	86.5	86.50-0	8650005	Atividades de terapia ocupacional	8099	21
2216	Q	86	86.5	86.50-0	8650006	Atividades de fonoaudiologia	8099	21
2218	Q	86	86.5	86.50-0	8650099	Atividades de profissionais da área de saúde não especificadas anteriormente	8099	21
2221	Q	86	86.6	86.60-7	8660700	Atividades de apoio à gestão de saúde	8099	21
2224	Q	86	86.9	86.90-9	8690901	Atividades de práticas integrativas e complementares em saúde humana	8099	21
2225	Q	86	86.9	86.90-9	8690902	Atividades de banco de leite humano	8099	21
2243	Q	87	87.3	87.30-1	8730101	Orfanatos	7399	8
2244	Q	87	87.3	87.30-1	8730102	Albergues assistenciais	7399	8
1	A	\N	\N	\N	\N	AGRICULTURA, PECUÁRIA, PRODUÇÃO FLORESTAL, PESCA E AQÜICULTURA	7399	8
9	A	1	01.1	01.12-1	\N	Cultivo de algodão herbáceo e de outras fibras de lavoura temporária	7399	8
46	A	1	01.3	01.33-4	\N	Cultivo de frutas de lavoura permanente, exceto laranja e uva	7399	8
98	A	1	01.5	01.59-8	\N	Criação de animais não especificados anteriormente	7399	8
134	A	2	02.2	02.20-9	\N	Produção florestal - florestas nativas	7399	8
2230	Q	87	87.1	87.11-5	8711501	Clínicas e residências geriátricas	8050	22
2231	Q	87	87.1	87.11-5	8711502	Instituições de longa permanência para idosos	8050	22
2232	Q	87	87.1	87.11-5	8711503	Atividades de assistência a deficientes físicos, imunodeprimidos e convalescentes	8050	22
2233	Q	87	87.1	87.11-5	8711504	Centros de apoio a pacientes com câncer e com AIDS	8050	22
2234	Q	87	87.1	87.11-5	8711505	Condomínios residenciais para idosos	8050	22
2239	Q	87	87.2	87.20-4	8720401	Atividades de centros de assistência psicossocial	8050	22
2245	Q	87	87.3	87.30-1	8730199	Atividades de assistência social prestadas em residências coletivas e particulares não especificadas anteriormente	8398	5
2249	Q	88	88.0	88.00-6	8800600	Serviços de assistência social sem alojamento	8398	5
2253	R	90	90.0	90.01-9	9001901	Produção teatral	7922	17
2254	R	90	90.0	90.01-9	9001902	Produção musical	7922	17
2255	R	90	90.0	90.01-9	9001903	Produção de espetáculos de dança	7922	17
2256	R	90	90.0	90.01-9	9001904	Produção de espetáculos circenses, de marionetes e similares	7922	17
2257	R	90	90.0	90.01-9	9001905	Produção de espetáculos de rodeios, vaquejadas e similares	7999	32
2258	R	90	90.0	90.01-9	9001906	Atividades de sonorização e de iluminação	7922	17
2261	R	90	90.0	90.02-7	9002701	Atividades de artistas plásticos, jornalistas independentes e escritores	7299	29
2262	R	90	90.0	90.02-7	9002702	Restauração de obras-de-arte	5932	1
2264	R	90	90.0	90.03-5	9003500	Gestão de espaços para artes cênicas, espetáculos e outras atividades artísticas	7922	17
2268	R	91	91.0	91.01-5	9101500	Atividades de bibliotecas e arquivos	5942	39
2270	R	91	91.0	91.02-3	9102301	Atividades de museus e de exploração de lugares e prédios históricos e atrações similares	7991	32
2271	R	91	91.0	91.02-3	9102302	Restauração e conservação de lugares e prédios históricos	7991	32
2273	R	91	91.0	91.03-1	9103100	Atividades de jardins botânicos, zoológicos, parques nacionais, reservas ecológicas e áreas de proteção ambiental	7998	17
2277	R	92	92.0	92.00-3	9200301	Casas de bingo	7995	32
2278	R	92	92.0	92.00-3	9200302	Exploração de apostas em corridas de cavalos	7995	32
2279	R	92	92.0	92.00-3	9200399	Exploração de jogos de azar e apostas não especificados anteriormente	7995	32
2283	R	93	93.1	93.11-5	9311500	Gestão de instalações de esportes	7999	32
2285	R	93	93.1	93.12-3	9312300	Clubes sociais, esportivos e similares	7997	5
2287	R	93	93.1	93.13-1	9313100	Atividades de condicionamento físico	7941	17
2289	R	93	93.1	93.19-1	9319101	Produção e promoção de eventos esportivos	7999	32
2290	R	93	93.1	93.19-1	9319199	Outras atividades esportivas não especificadas anteriormente	7999	32
2293	R	93	93.2	93.21-2	9321200	Parques de diversão e parques temáticos	7996	17
2295	R	93	93.2	93.29-8	9329801	Discotecas, danceterias, salões de dança e similares	5813	14
2296	R	93	93.2	93.29-8	9329802	Exploração de boliches	7933	32
2299	R	93	93.2	93.29-8	9329899	Outras atividades de recreação e lazer não especificadas anteriormente	7999	32
2304	S	94	94.1	94.11-1	9411100	Atividades de organizações associativas patronais e empresariais	8641	5
2306	S	94	94.1	94.12-0	9412000	Atividades de organizações associativas profissionais	8699	5
2309	S	94	94.2	94.20-1	9420100	Atividades de organizações sindicais	8699	5
2312	S	94	94.3	94.30-8	9430800	Atividades de associações de defesa de direitos sociais	8641	5
2315	S	94	94.9	94.91-0	9491000	Atividades de organizações religiosas	8661	5
2317	S	94	94.9	94.92-8	9492800	Atividades de organizações políticas	8651	5
2321	S	94	94.9	94.99-5	9499500	Atividades associativas não especificadas anteriormente	8699	5
2325	S	95	95.1	95.11-8	9511800	Reparação e manutenção de computadores e de equipamentos periféricos	7622	36
2327	S	95	95.1	95.12-6	9512600	Reparação e manutenção de equipamentos de comunicação	7622	36
2330	S	95	95.2	95.21-5	9521500	Reparação e manutenção de equipamentos eletroeletrônicos de uso pessoal e doméstico	7629	36
2332	S	95	95.2	95.29-1	9529101	Reparação de calçados, de bolsas e artigos de viagem*	7251	29
2333	S	95	95.2	95.29-1	9529102	Chaveiros	7399	8
2334	S	95	95.2	95.29-1	9529103	Reparação de relógios	7631	36
2335	S	95	95.2	95.29-1	9529104	Reparação de bicicletas, triciclos e outros veículos não-motorizados	7699	36
2359	S	96	96.0	96.09-2	9609204	Exploração de máquinas de serviços pessoais acionadas por moeda	7399	8
2360	S	96	96.0	96.09-2	9609299	Outras atividades de serviços pessoais não especificadas anteriormente	7399	8
2370	U	99	99.0	99.00-8	9900800	Organismos internacionais e outras instituições extraterritoriais	7399	8
5	A	1	01.1	01.11-3	0111301	Cultivo de arroz	7399	8
6	A	1	01.1	01.11-3	0111302	Cultivo de milho	7399	8
7	A	1	01.1	01.11-3	0111303	Cultivo de trigo	7399	8
25	A	1	01.1	01.19-9	0119901	Cultivo de abacaxi	7399	8
26	A	1	01.1	01.19-9	0119902	Cultivo de alho	7399	8
34	A	1	01.1	01.19-9	0119999	Cultivo de outras plantas de lavoura temporária não especificadas anteriormente	7399	8
55	A	1	01.3	01.33-4	0133409	Cultivo de maracujá	7399	8
56	A	1	01.3	01.33-4	0133410	Cultivo de manga	7399	8
57	A	1	01.3	01.33-4	0133411	Cultivo de pêssego	7399	8
58	A	1	01.3	01.33-4	0133499	Cultivo de frutas de lavoura permanente não especificadas anteriormente	7399	8
70	A	1	01.3	01.39-3	0139399	Cultivo de outras plantas de lavoura permanente não especificadas anteriormente	7399	8
100	A	1	01.5	01.59-8	0159802	Criação de animais de estimação	7399	8
109	A	1	01.6	01.61-0	0161099	Atividades de apoio à agricultura não especificadas anteriormente	7399	8
127	A	2	02.1	02.10-1	0210105	Cultivo de espécies madeireiras, exceto eucalipto, acácia-negra, pinus e teca	7399	8
151	A	3	03.1	03.11-6	0311604	Atividades de apoio à pesca em água salgada	7399	8
154	A	3	03.1	03.12-4	0312402	Pesca de crustáceos e moluscos em água doce	7399	8
176	B	5	05.0	\N	\N	Extração de carvão mineral	7399	8
190	B	7	07.2	\N	\N	Extração de minerais metálicos não-ferrosos	7399	8
225	B	8	08.9	\N	\N	Extração de outros minerais não-metálicos	7399	8
226	B	8	08.9	08.91-6	\N	Extração de minerais para fabricação de adubos, fertilizantes e outros produtos químicos	7399	8
257	C	10	10.1	10.12-1	\N	Abate de suínos, aves e outros pequenos animais	7399	8
283	C	10	10.4	10.43-1	\N	Fabricação de margarina e outras gorduras vegetais e de óleos não-comestíveis de animais	7399	8
317	C	10	10.8	10.81-3	\N	Torrefação e moagem de café	7399	8
336	C	10	10.9	10.99-6	\N	Fabricação de produtos alimentícios não especificados anteriormente	7399	8
367	C	12	12.2	12.20-4	\N	Fabricação de produtos do fumo	7399	8
385	C	13	13.2	13.22-7	\N	Tecelagem de fios de fibras têxteis naturais, exceto algodão	7399	8
406	C	13	13.5	13.59-6	\N	Fabricação de outros produtos têxteis não especificados anteriormente	7399	8
433	C	15	15.2	15.21-1	\N	Fabricação de artigos para viagem, bolsas e semelhantes de qualquer material	7399	8
456	C	16	16.2	16.21-8	\N	Fabricação de madeira laminada e de chapas de madeira compensada, prensada e aglomerada	7399	8
483	C	17	17.4	\N	\N	Fabricação de produtos diversos de papel, cartolina, papel-cartão e papelão ondulado	7399	8
484	C	17	17.4	17.41-9	\N	Fabricação de produtos de papel, cartolina, papel-cartão e papelão ondulado para uso industrial, comercial e de escritório	7399	8
534	C	20	20.1	20.12-6	\N	Fabricação de intermediários para fertilizantes	7399	8
557	C	20	20.4	\N	\N	Fabricação de fibras artificiais e sintéticas	7399	8
565	C	20	20.6	\N	\N	Fabricação de sabões, detergentes, produtos de limpeza, cosméticos, produtos de perfumaria e de higiene pessoal	7399	8
609	C	22	22.1	22.11-1	\N	Fabricação de pneumáticos e de câmaras-de-ar	7399	8
633	C	23	23.1	23.19-2	\N	Fabricação de artigos de vidro	7399	8
648	C	23	23.4	23.42-7	\N	Fabricação de produtos cerâmicos não-refratários para uso estrutural na construção	7399	8
688	C	24	24.4	24.41-5	\N	Metalurgia do alumínio e suas ligas	7399	8
2336	S	95	95.2	95.29-1	9529105	Reparação de artigos do mobiliário	7641	36
2337	S	95	95.2	95.29-1	9529106	Reparação de jóias	7631	36
2342	S	96	96.0	96.01-7	9601701	Lavanderias	7210	10
2343	S	96	96.0	96.01-7	9601702	Tinturarias	7216	10
2344	S	96	96.0	96.01-7	9601703	Toalheiros	7210	10
2346	S	96	96.0	96.02-5	9602501	Cabeleireiros	7230	29
2347	S	96	96.0	96.02-5	9602502	Outras atividades de tratamento de beleza	7230	29
2349	S	96	96.0	96.03-3	9603301	Gestão e manutenção de cemitérios	7261	31
2350	S	96	96.0	96.03-3	9603302	Serviços de cremação	7261	31
2351	S	96	96.0	96.03-3	9603303	Serviços de sepultamento	7261	31
2352	S	96	96.0	96.03-3	9603304	Serviços de funerárias	7261	31
2353	S	96	96.0	96.03-3	9603305	Serviços de somatoconservação	7261	31
2356	S	96	96.0	96.09-2	9609201	Clínicas de estética e similares*	8099	21
2357	S	96	96.0	96.09-2	9609202	Agências matrimoniais	7273	29
2358	S	96	96.0	96.09-2	9609203	Alojamento, higiene e embelezamento de animais	5995	39
2365	T	97	97.0	97.00-5	9700500	Serviços domésticos	7349	10
711	C	25	25.1	25.13-6	\N	Fabricação de obras de caldeiraria pesada	7399	8
173	A	3	03.2	03.22-1	0322199	Cultivos e semicultivos da aqüicultura em água doce não especificados anteriormente	7399	8
206	B	7	07.2	07.29-4	0729401	Extração de minérios de nióbio e titânio	7399	8
246	B	9	09.9	09.90-4	0990402	Atividades de apoio à extração de minerais metálicos não-ferrosos	7399	8
247	B	9	09.9	09.90-4	0990403	Atividades de apoio à extração de minerais não-metálicos	7399	8
263	C	10	10.1	10.13-9	1013901	Fabricação de produtos de carne	7399	8
277	C	10	10.3	10.33-3	1033302	Fabricação de sucos de frutas, hortaliças e legumes, exceto concentrados	7399	8
309	C	10	10.6	10.69-4	1069400	Moagem e fabricação de produtos de origem vegetal não especificados anteriormente	7399	8
338	C	10	10.9	10.99-6	1099602	Fabricação de pós alimentícios	7399	8
352	C	11	11.1	11.13-5	1113501	Fabricação de malte, inclusive malte uísque	7399	8
358	C	11	11.2	11.22-4	1122401	Fabricação de refrigerantes	7399	8
359	C	11	11.2	11.22-4	1122402	Fabricação de chá mate e outros chás prontos para consumo	7399	8
460	C	16	16.2	16.22-6	1622602	Fabricação de esquadrias de madeira e de peças de madeira para instalações industriais e comerciais	7399	8
480	C	17	17.3	17.32-0	1732000	Fabricação de embalagens de cartolina e papel-cartão	7399	8
486	C	17	17.4	17.41-9	1741902	Fabricação de produtos de papel, cartolina, papel-cartão e papelão ondulado para uso industrial, comercial e de escritório, exceto formulário contínuo	7399	8
539	C	20	20.1	20.14-2	2014200	Fabricação de gases industriais	7399	8
559	C	20	20.4	20.40-1	2040100	Fabricação de fibras artificiais e sintéticas	7399	8
588	C	20	20.9	20.93-2	2093200	Fabricação de aditivos de uso industrial	7399	8
606	C	21	21.2	21.23-8	2123800	Fabricação de preparações farmacêuticas	7399	8
610	C	22	22.1	22.11-1	2211100	Fabricação de pneumáticos e de câmaras-de-ar	7399	8
625	C	22	22.2	22.29-3	2229303	Fabricação de artefatos de material plástico para uso na construção, exceto tubos e acessórios	7399	8
656	C	23	23.9	23.91-5	2391501	Britamento de pedras, exceto associado à extração	7399	8
697	C	24	24.4	24.49-1	2449102	Produção de laminados de zinco	7399	8
715	C	25	25.2	25.21-7	2521700	Fabricação de tanques, reservatórios metálicos e caldeiras para aquecimento central	7399	8
734	C	25	25.5	\N	\N	Fabricação de equipamento bélico pesado, armas de fogo e munições	7399	8
756	C	26	26.2	26.22-1	\N	Fabricação de periféricos para equipamentos de informática	7399	8
774	C	26	26.7	\N	\N	Fabricação de equipamentos e instrumentos ópticos, fotográficos e cinematográficos	7399	8
805	C	27	27.5	27.51-1	\N	Fabricação de fogões, refrigeradores e máquinas de lavar e secar para uso doméstico	7399	8
823	C	28	28.1	28.14-3	\N	Fabricação de compressores	7399	8
826	C	28	28.1	28.15-1	\N	Fabricação de equipamentos de transmissão para fins industriais	7399	8
851	C	28	28.3	28.33-0	\N	Fabricação de máquinas e equipamentos para a agricultura e pecuária, exceto para irrigação	7399	8
877	C	28	28.6	28.69-1	\N	Fabricação de máquinas e equipamentos para uso industrial específico não especificados anteriormente	7399	8
916	C	30	30.1	30.12-1	\N	Construção de embarcações para esporte e lazer	7399	8
924	C	30	30.3	30.31-8	\N	Fabricação de locomotivas, vagões e outros materiais rodantes	7399	8
926	C	30	30.3	30.32-6	\N	Fabricação de peças e acessórios para veículos ferroviários	7399	8
949	C	31	31.0	31.03-9	\N	Fabricação de móveis de outros materiais, exceto madeira e metal	7399	8
973	C	32	32.5	\N	\N	Fabricação de instrumentos e materiais para uso médico e odontológico e de artigos ópticos	7399	8
1038	C	33	33.1	33.19-8	\N	Manutenção e reparação de equipamentos e produtos não especificados anteriormente	7399	8
1058	D	35	35.2	35.20-4	\N	Produção de gás; processamento de gás natural; distribuição de combustíveis gasosos por redes urbanas	7399	8
1072	E	37	37.0	37.01-1	\N	Gestão de redes de esgoto	7399	8
1074	E	37	37.0	37.02-9	\N	Atividades relacionadas a esgoto, exceto a gestão de redes	7399	8
1093	E	38	38.3	38.39-4	\N	Recuperação de materiais não especificados anteriormente	7399	8
1115	F	42	42.1	42.13-8	\N	Obras de urbanização - ruas, praças e calçadas	7399	8
1140	F	43	43.1	43.11-8	\N	Demolição e preparação de canteiros de obras	7399	8
1174	F	43	43.9	43.99-1	\N	Serviços especializados para construção não especificados anteriormente	7399	8
1213	G	45	45.4	45.41-2	\N	Comércio por atacado e a varejo de motocicletas, peças e acessórios	7399	8
1222	G	45	45.4	45.43-9	\N	Manutenção e reparação de motocicletas	7399	8
1224	G	46	\N	\N	\N	COMÉRCIO POR ATACADO, EXCETO VEÍCULOS AUTOMOTORES E MOTOCICLETAS	7399	8
407	C	13	13.5	13.59-6	1359600	Fabricação de outros produtos têxteis não especificados anteriormente	5948	39
434	C	15	15.2	15.21-1	1521100	Fabricação de artigos para viagem, bolsas e semelhantes de qualquer material	5948	39
658	C	23	23.9	23.91-5	2391503	Aparelhamento de placas e execução de trabalhos em mármore, granito, ardósia e outras pedras	1740	11
757	C	26	26.2	26.22-1	2622100	Fabricação de periféricos para equipamentos de informática	7399	8
818	C	28	28.1	28.11-9	2811900	Fabricação de motores e turbinas, peças e acessórios, exceto para aviões e veículos rodoviários	7399	8
834	C	28	28.2	28.22-4	2822401	Fabricação de máquinas, equipamentos e aparelhos para transporte e elevação de pessoas, peças e acessórios	7399	8
842	C	28	28.2	28.25-9	2825900	Fabricação de máquinas e equipamentos para saneamento básico e ambiental, peças e acessórios	7399	8
872	C	28	28.6	28.64-0	2864000	Fabricação de máquinas e equipamentos para as indústrias do vestuário, do couro e de calçados, peças e acessórios	7399	8
915	C	30	30.1	30.11-3	3011302	Construção de embarcações para uso comercial e para usos especiais, exceto de grande porte	7399	8
935	C	30	30.5	30.50-4	3050400	Fabricação de veículos militares de combate	7399	8
940	C	30	30.9	30.92-0	3092000	Fabricação de bicicletas e triciclos não-motorizados, peças e acessórios	7399	8
970	C	32	32.4	32.40-0	3240002	Fabricação de mesas de bilhar, de sinuca e acessórios não associada à locação	7399	8
991	C	32	32.9	32.99-0	3299002	Fabricação de canetas, lápis e outros artigos para escritório	7399	8
1008	C	33	33.1	33.13-9	3313999	Manutenção e reparação de máquinas, aparelhos e materiais elétricos não especificados anteriormente	7399	8
1020	C	33	33.1	33.14-7	3314711	Manutenção e reparação de máquinas e equipamentos para agricultura e pecuária	7399	8
1023	C	33	33.1	33.14-7	3314714	Manutenção e reparação de máquinas e equipamentos para a prospecção e extração de petróleo	7399	8
1158	F	43	43.2	43.29-1	4329102	Instalação de equipamentos para orientação à navegação marítima, fluvial e lacustre	7399	8
1236	G	46	46.1	46.16-8	\N	Representantes comerciais e agentes do comércio de têxteis, vestuário, calçados e artigos de viagem	7399	8
1309	G	46	46.4	46.44-3	\N	Comércio atacadista de produtos farmacêuticos para uso humano e veterinário	7399	8
1344	G	46	46.6	46.62-1	\N	Comércio atacadista de máquinas, equipamentos para terraplenagem, mineração e construção; partes e peças	7399	8
1364	G	46	46.7	46.79-6	\N	Comércio atacadista especializado de materiais de construção não especificados anteriormente e de materiais de construção em geral	7399	8
1394	G	46	46.8	46.89-3	\N	Comércio atacadista especializado de outros produtos intermediários não especificados anteriormente	7399	8
1429	G	47	47.2	47.29-6	\N	Comércio varejista de produtos alimentícios em geral ou especializado em produtos alimentícios não especificados anteriormente; produtos do fumo	7399	8
1468	G	47	47.5	47.57-1	\N	Comércio varejista especializado de peças e acessórios para aparelhos eletroeletrônicos para uso doméstico, exceto informática e comunicação	7399	8
1499	G	47	47.8	47.81-4	\N	Comércio varejista de artigos do vestuário e acessórios	7399	8
1512	G	47	47.8	47.89-0	\N	Comércio varejista de outros produtos novos não especificados anteriormente	7399	8
1537	H	49	49.2	49.22-1	\N	Transporte rodoviário coletivo de passageiros, com itinerário fixo, intermunicipal, interestadual e internacional	7399	8
1592	H	51	51.1	51.11-1	\N	Transporte aéreo de passageiros regular	7399	8
1612	H	52	52.2	52.21-4	\N	Concessionárias de rodovias, pontes, túneis e serviços relacionados	7399	8
1635	H	52	52.5	52.50-8	\N	Atividades relacionadas à organização do transporte de carga	7399	8
1665	I	56	56.1	56.11-2	\N	Restaurantes e outros estabelecimentos de serviços de alimentação e bebidas	7399	8
1695	J	58	58.2	58.29-8	\N	Edição integrada à impressão de cadastros, listas e de outros produtos gráficos	7399	8
1721	J	60	60.2	60.22-5	\N	Programadoras e atividades relacionadas à televisão por assinatura	7399	8
1258	G	46	46.2	46.23-1	4623106	Comércio atacadista de sementes, flores, plantas e gramas	7399	8
1297	G	46	46.3	46.39-7	4639702	Comércio atacadista de produtos alimentícios em geral, com atividade de fracionamento e acondicionamento associada	7399	8
1345	G	46	46.6	46.62-1	4662100	Comércio atacadista de máquinas, equipamentos para terraplenagem, mineração e construção; partes e peças	7399	8
1874	K	66	66.1	66.12-6	6612605	Agentes de investimentos em aplicações financeiras	7399	8
1059	D	35	35.2	35.20-4	3520401	Produção de gás; processamento de gás natural	4900	39
1090	E	38	38.3	38.31-9	3831999	Recuperação de materiais metálicos, exceto alumínio	5211	23
1120	F	42	42.2	42.21-9	4221902	Construção de estações e redes de distribuição de energia elétrica	1520	11
1136	F	42	42.9	42.99-5	4299501	Construção de instalações esportivas e recreativas	1520	11
1137	F	42	42.9	42.99-5	4299599	Outras obras de engenharia civil não especificadas anteriormente	1520	11
1178	F	43	43.9	43.99-1	4399104	Serviços de operação e fornecimento de equipamentos para transporte e elevação de cargas e pessoas para uso em obras	5065	9
1203	G	45	45.2	45.20-0	4520007	Serviços de instalação, manutenção e reparação de acessórios para veículos automotores	5533	7
1217	G	45	45.4	45.41-2	4541204	Comércio a varejo de motocicletas e motonetas usadas	5571	39
1294	G	46	46.3	46.37-1	4637199	Comércio atacadista especializado em outros produtos alimentícios não especificados anteriormente	5199	9
1328	G	46	46.4	46.49-4	4649406	Comércio atacadista de lustres, luminárias e abajures	5200	24
1330	H	49	49.1	49.11-6	4911600	Transporte ferroviário de carga	4011	43
1397	G	46	46.8	46.89-3	4689399	Comércio atacadista especializado em outros produtos intermediários não especificados anteriormente *	7399	8
1629	H	52	52.3	52.39-7	5239700	Atividades auxiliares dos transportes aquaviários não especificadas anteriormente	7399	8
1636	H	52	52.5	52.50-8	5250801	Comissaria de despachos	7399	8
1752	J	62	62.0	\N	\N	Atividades dos serviços de tecnologia da informação	7399	8
1765	J	63	63.1	63.11-9	\N	Tratamento de dados, provedores de serviços de aplicação e serviços de hospedagem na internet	7399	8
1774	K	\N	\N	\N	\N	ATIVIDADES FINANCEIRAS, DE SEGUROS E SERVIÇOS RELACIONADOS	7399	8
1779	K	64	64.2	\N	\N	Intermediação monetária - depósitos à vista	7399	8
1804	K	64	64.3	64.36-1	\N	Sociedades de crédito, financiamento e investimento - financeiras	7399	8
1826	K	64	64.9	\N	\N	Atividades de serviços financeiros não especificadas anteriormente	7399	8
1854	K	65	65.4	65.41-3	\N	Previdência complementar fechada	7399	8
1862	K	66	\N	\N	\N	ATIVIDADES AUXILIARES DOS SERVIÇOS FINANCEIROS, SEGUROS, PREVIDÊNCIA COMPLEMENTAR E PLANOS DE SAÚDE	7399	8
1905	L	68	68.2	68.22-6	\N	Gestão e administração da propriedade imobiliária*	7399	8
1910	M	69	69.1	69.11-7	\N	Atividades jurídicas, exceto cartórios	7399	8
1916	M	69	69.2	\N	\N	Atividades de contabilidade, consultoria e auditoria contábil e tributária	7399	8
1942	M	72	72.1	\N	\N	Pesquisa e desenvolvimento experimental em ciências físicas e naturais	7399	8
1969	M	74	74.2	74.20-0	\N	Atividades fotográficas e similares	7399	8
1988	N	77	\N	\N	\N	ALUGUÉIS NÃO-IMOBILIÁRIOS E GESTÃO DE ATIVOS INTANGÍVEIS NÃO-FINANCEIROS	7399	8
2016	N	77	77.3	77.39-0	\N	Aluguel de máquinas e equipamentos não especificados anteriormente	7399	8
2038	N	79	79.1	79.12-1	\N	Operadores turísticos	7399	8
2040	N	79	79.9	\N	\N	Serviços de reservas e outros serviços de turismo não especificados anteriormente	7399	8
2058	N	81	81.1	81.11-7	\N	Serviços combinados para apoio a edifícios, exceto condomínios prediais	7399	8
2076	N	82	82.1	82.19-9	\N	Fotocópias, preparação de documentos e outros serviços especializados de apoio administrativo	7399	8
2118	O	84	84.2	84.24-8	\N	Segurança e ordem pública	7399	8
2135	P	85	85.2	85.20-1	\N	Ensino médio	7399	8
2144	P	85	85.4	\N	\N	Educação profissional de nível técnico e tecnológico	7399	8
2173	Q	86	86.1	86.10-1	\N	Atividades de atendimento hospitalar	7399	8
2228	Q	87	87.1	\N	\N	Atividades de assistência a idosos, deficientes físicos, imunodeprimidos e convalescentes, e de infra-estrutura e apoio a pacientes prestadas em residências coletivas e particulares	7399	8
2292	R	93	93.2	93.21-2	\N	Parques de diversão e parques temáticos	7399	8
2302	S	94	94.1	\N	\N	Atividades de organizações associativas patronais, empresariais e profissionais	7399	8
1805	K	64	64.3	64.36-1	6436100	Sociedades de crédito, financiamento e investimento - financeiras	7399	8
1820	K	64	64.6	64.63-8	6463800	Outras sociedades de participação, exceto holdings	7399	8
1832	K	64	64.9	64.93-0	6493000	Administração de consórcios para aquisição de bens e direitos	7399	8
1866	K	66	66.1	66.11-8	6611802	Bolsa de mercadorias	7399	8
1374	G	46	46.8	46.81-8	4681803	Comércio atacadista de combustíveis de origem vegetal, exceto álcool carburante	5172	9
1380	G	46	46.8	46.83-4	4683400	Comércio atacadista de defensivos agrícolas, adubos, fertilizantes e corretivos do solo	5261	23
1431	G	47	47.2	47.29-6	4729699	Comércio varejista de produtos alimentícios em geral ou especializado em produtos alimentícios não especificados anteriormente	5411	19
1467	G	47	47.5	47.56-3	4756300	Comércio varejista especializado de instrumentos musicais e acessórios	5733	39
1469	G	47	47.5	47.57-1	4757100	Comércio varejista especializado de peças e acessórios para aparelhos eletroeletrônicos para uso doméstico, exceto informática e comunicação	5732	16
1505	G	47	47.8	47.83-1	4783101	Comércio varejista de artigos de joalheria	5944	39
1516	G	47	47.8	47.89-0	4789004	Comércio varejista de animais vivos e de artigos e alimentos para animais de estimação	5995	39
1538	H	49	49.2	49.22-1	4922101	Transporte rodoviário coletivo de passageiros, com itinerário fixo, intermunicipal, exceto em região metropolitana	4131	43
1556	H	49	49.3	49.30-2	4930203	Transporte rodoviário de produtos perigosos	4789	43
1571	H	50	50.1	50.12-2	5012202	Transporte marítimo de longo curso - Passageiros	4411	40
1662	I	55	55.9	55.90-6	5590699	Outros alojamentos não especificados anteriormente	7011	40
1676	I	56	56.2	56.20-1	5620104	Fornecimento de alimentos preparados preponderantemente para consumo domiciliar	5814	14
1687	J	58	58.1	58.19-1	5819100	Edição de cadastros, listas e de outros produtos gráficos	2741	9
1696	J	58	58.2	58.29-8	5829800	Edição integrada à impressão de cadastros, listas e de outros produtos gráficos	2741	9
1728	J	61	61.1	61.10-8	6110802	Serviços de redes de transportes de telecomunicações - SRTT	4814	42
1750	J	61	61.9	61.90-6	6190699	Outras atividades de telecomunicações não especificadas anteriormente	4816	42
1979	M	74	74.9	74.90-1	7490103	Serviços de agronomia e de consultoria às atividades agrícolas e pecuárias	7399	8
2078	N	82	82.1	82.19-9	8219999	Preparação de documentos e serviços especializados de apoio administrativo não especificados anteriormente	7399	8
2188	Q	86	86.3	86.30-5	8630505	Atividade odontológica sem recursos para realização de procedimentos cirúrgicos	7399	8
2322	S	95	\N	\N	\N	REPARAÇÃO E MANUTENÇÃO DE EQUIPAMENTOS DE INFORMÁTICA E COMUNICAÇÃO E DE OBJETOS PESSOAIS E DOMÉSTICOS	7399	8
2367	U	99	\N	\N	\N	ORGANISMOS INTERNACIONAIS E OUTRAS INSTITUIÇÕES EXTRATERRITORIAIS	7399	8
1024	C	33	33.1	33.14-7	3314715	Manutenção e reparação de máquinas e equipamentos para uso na extração mineral, exceto na extração de petróleo	7399	8
12	A	1	01.1	01.12-1	0112199	Cultivo de outras fibras de lavoura temporária não especificadas anteriormente	7399	8
161	A	3	03.2	03.21-3	0321303	Criação de ostras e mexilhões em água salgada e salobra	7399	8
210	B	7	07.2	07.29-4	0729405	Beneficiamento de minérios de cobre, chumbo, zinco e outros minerais metálicos não-ferrosos não especificados anteriormente	7399	8
723	C	25	25.3	25.32-2	2532201	Produção de artefatos estampados de metal	7399	8
785	C	27	27.1	27.10-4	2710402	Fabricação de transformadores, indutores, conversores, sincronizadores e semelhantes, peças e acessórios	7399	8
1227	G	46	46.1	46.11-7	4611700	Representantes comerciais e agentes do comércio de matérias-primas agrícolas e animais vivos	7399	8
1268	G	46	46.3	46.32-0	4632003	Comércio atacadista de cereais e leguminosas beneficiados, farinhas, amidos e féculas, com atividade de fracionamento e acondicionamento associada	7399	8
1790	K	64	64.2	64.24-7	6424704	Cooperativas de crédito rural	7399	8
324	C	10	10.9	10.91-1	1091100	Fabricação de produtos de panificação	5462	19
379	C	13	13.1	13.13-8	1313800	Fiação de fibras artificiais e sintéticas	5948	39
386	C	13	13.2	13.22-7	1322700	Tecelagem de fios de fibras têxteis naturais, exceto algodão	5948	39
394	C	13	13.4	13.40-5	1340501	Estamparia e texturização em fios, tecidos, artefatos têxteis e peças do vestuário	5948	39
569	C	20	20.6	20.62-2	2062200	Fabricação de produtos de limpeza e polimento	2842	9
645	C	23	23.3	23.30-3	2330399	Fabricação de outros artefatos e produtos de concreto, cimento, fibrocimento, gesso e materiais semelhantes	5039	9
731	C	25	25.4	25.42-0	2542000	Fabricação de artigos de serralheria, exceto esquadrias	7399	8
910	C	29	29.5	29.50-6	2950600	Recondicionamento e recuperação de motores para veículos automotores	7538	7
992	C	32	32.9	32.99-0	3299003	Fabricação de letras, letreiros e placas de qualquer material, exceto luminosos	7399	8
1016	C	33	33.1	33.14-7	3314707	Manutenção e reparação de máquinas e aparelhos de refrigeração e ventilação para uso industrial e comercial	7623	36
1084	E	38	38.2	38.21-1	3821100	Tratamento e disposição de resíduos não-perigosos	4900	39
1086	E	38	38.2	38.22-0	3822000	Tratamento e disposição de resíduos perigosos	4900	39
1141	F	43	43.1	43.11-8	4311801	Demolição de edifícios e outras estruturas	1520	11
1187	G	45	45.1	45.11-1	4511103	Comércio por atacado de automóveis, camionetas e utilitários novos e usados	5511	6
1891	K	66	66.2	66.29-1	6629100	Atividades auxiliares dos seguros, da previdência complementar e dos planos de saúde não especificadas anteriormente	6300	25
1935	M	71	71.1	71.19-7	7119703	Serviços de desenho técnico relacionados à arquitetura e engenharia	8911	31
1951	M	73	73.1	73.11-4	7311400	Agências de publicidade	7311	31
1953	M	73	73.1	73.12-2	7312200	Agenciamento de espaços para publicidade, exceto em veículos de comunicação	7311	31
2005	N	77	77.2	77.29-2	7729202	Aluguel de móveis, utensílios e aparelhos de uso doméstico e pessoal; instrumentos musicais	7394	34
2042	N	79	79.9	79.90-2	7990200	Serviços de reservas e outros serviços de turismo não especificados anteriormente	4722	43
2094	N	82	82.9	82.99-7	8299703	Serviços de gravação de carimbos, exceto confecção	5111	9
2131	P	85	85.1	85.12-1	8512100	Educação infantil - pré-escola	8351	29
2157	P	85	85.9	85.92-9	8592901	Ensino de dança	7911	32
2167	P	85	85.9	85.99-6	8599604	Treinamento em desenvolvimento profissional e gerencial	8299	15
2181	Q	86	86.2	86.22-4	8622400	Serviços de remoção de pacientes, exceto os serviços móveis de atendimento a urgências	8062	21
2200	Q	86	86.4	86.40-2	8640207	Serviços de diagnóstico por imagem sem uso de radiação ionizante, exceto ressonância magnética	8071	21
2201	Q	86	86.4	86.40-2	8640208	Serviços de diagnóstico por registro gráfico - ECG, EEG e outros exames análogos	8071	21
2217	Q	86	86.5	86.50-0	8650007	Atividades de terapia de nutrição enteral e parenteral	8099	21
2236	Q	87	87.1	87.12-3	8712300	Atividades de fornecimento de infra-estrutura de apoio e assistência a paciente no domicílio	8050	22
2259	R	90	90.0	90.01-9	9001999	Artes cênicas, espetáculos e atividades complementares não especificadas anteriormente	7922	17
2297	R	93	93.2	93.29-8	9329803	Exploração de jogos de sinuca, bilhar e similares	7932	32
2319	S	94	94.9	94.93-6	9493600	Atividades de organizações associativas ligadas à cultura e à arte	8699	5
2338	S	95	95.2	95.29-1	9529199	Reparação e manutenção de outros objetos e equipamentos pessoais e domésticos não especificados anteriormente	7699	36
1211	G	45	45.3	45.30-7	4530706	Representantes comerciais e agentes do comércio de peças e acessórios novos e usados para veículos automotores	5533	7
1218	G	45	45.4	45.41-2	4541205	Comércio a varejo de peças e acessórios para motocicletas e motonetas	5571	39
1220	G	45	45.4	45.42-1	4542101	Representantes comerciais e agentes do comércio de motocicletas e motonetas, peças e acessórios	5571	39
1281	G	46	46.3	46.35-4	4635403	Comércio atacadista de bebidas com atividade de fracionamento e acondicionamento associada	5411	19
1296	G	46	46.3	46.39-7	4639701	Comércio atacadista de produtos alimentícios em geral	5199	9
1372	G	46	46.8	46.81-8	4681801	Comércio atacadista de álcool carburante, biodiesel, gasolina e demais derivados de petróleo, exceto lubrificantes, não realizado por transportador retalhista (TRR)	5172	9
1375	G	46	46.8	46.81-8	4681804	Comércio atacadista de combustíveis de origem mineral em bruto	5172	9
1404	G	46	46.9	46.93-1	4693100	Comércio atacadista de mercadorias em geral, sem predominância de alimentos ou de insumos agropecuários	5261	23
1471	G	47	47.5	47.59-8	4759801	Comércio varejista de artigos de tapeçaria, cortinas e persianas	5713	24
1472	G	47	47.5	47.59-8	4759899	Comércio varejista de outros artigos de uso doméstico não especificados anteriormente	5722	24
1548	H	49	49.2	49.29-9	4929902	Transporte rodoviário coletivo de passageiros, sob regime de fretamento, intermunicipal, interestadual e internacional	4131	43
1578	H	50	50.2	50.22-0	5022002	Transporte por navegação interior de passageiros em linhas regulares, intermunicipal, interestadual e internacional, exceto travessia	4789	43
1621	H	52	52.2	52.29-0	5229099	Outras atividades auxiliares dos transportes terrestres não especificadas anteriormente	4789	43
1654	I	55	55.1	55.10-8	5510801	Hotéis	\N	\N
1690	J	58	58.2	58.21-2	5821200	Edição integrada à impressão de livros	2741	9
1756	J	62	62.0	62.02-3	6202300	Desenvolvimento e licenciamento de programas de computador customizáveis	7372	31
1839	K	64	64.9	64.99-9	6499999	Outras atividades de serviços financeiros não especificadas anteriormente	6012	18
1925	M	70	70.2	70.20-4	7020400	Atividades de consultoria em gestão empresarial, exceto consultoria técnica específica	8999	31
1955	M	73	73.1	73.19-0	7319001	Criação e montagem de estandes para feiras e exposições	5199	9
2059	N	81	81.1	81.11-7	8111700	Serviços combinados para apoio a edifícios, exceto condomínios prediais	8699	5
2081	N	82	82.2	82.20-2	8220200	Atividades de teleatendimento	5969	13
2169	P	85	85.9	85.99-6	8599699	Outras atividades de ensino não especificadas anteriormente	8299	15
2208	Q	86	86.4	86.40-2	8640299	Atividades de serviços de complementação diagnóstica e terapêutica não especificadas anteriormente	8062	21
2226	Q	86	86.9	86.90-9	8690999	Outras atividades de atenção à saúde humana não especificadas anteriormente	8099	21
2240	Q	87	87.2	87.20-4	8720499	Atividades de assistência psicossocial e à saúde a portadores de distúrbios psíquicos, deficiência mental e dependência química não especificadas anteriormente	8099	21
2298	R	93	93.2	93.29-8	9329804	Exploração de jogos eletrônicos recreativos	7994	32
2354	S	96	96.0	96.03-3	9603399	Atividades funerárias e serviços relacionados não especificados anteriormente	7261	31
\.


--
-- Name: cnae_cnae_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cnae_cnae_id_seq', 2370, true);


--
-- Name: cnae_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cnae
    ADD CONSTRAINT cnae_pkey PRIMARY KEY (cnae_id);


--
-- PostgreSQL database dump complete
--

