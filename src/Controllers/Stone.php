<?php

namespace Credenciamento\Controllers;

use Credenciamento\Structs\Products\Stone as StoneProduct;
use Respect\Validation\Exceptions\NestedValidationExceptionInterface;

class Stone extends Controller {

  public $produto;

  public function __construct($onlyContent)
  {
    parent::__construct($onlyContent);

    $this->produto = new StoneProduct;
  }

  public function obterFormulario()
  {
    return $this->view->render(StoneProduct::$formulario_cadastro,
      array(
        'assets' => $this->assets,
        'produto' => 'Stone',
        'oldInput' => $this->oldInput,
        'template' => $this->template
      )
    );
  }

  public function salvarFormulario($input = array(), $usuario_id = null)
  {
    $this->produto->fillAttr($input);

    if($this->produto->validator())
    {
      $data['form_data'] = json_encode($input);
      $data['identificador'] = $input['dados_cpfcnpj'] . ' - ' . $input['dados_razaosocial_nomecompleto'];

      if($usuario_id)
      {
        $data['usuario_id'] = $usuario_id;
      }

      $data['ordenacao'] = $this->obterClassificacao($input);

      $this->produto->insert($data, $input);

      return [$data, array()];
    }

    $errors = array();
    try {
      $this->produto->validator->assert($this->produto);
    } catch(NestedValidationExceptionInterface $exception) {
      $errors = array_filter($exception->findMessages($this->produto->expected_data));
    }

    return [array(), $errors];
  }

  protected function obterClassificacao($input)
  {
    $dom = $this->produto->obterClassificador($input['dados_cpfcnpj'], $input['dados_razaosocial_nomecompleto']);

    return $this->classificar($dom, $input);
  }

  protected function classificar($dom, $input)
  {
    $classificacao = 1;

    $data = $this->obterDadosClassificador($dom);

    if($data['Razão Social'] == \strtoupper($input['dados_razaosocial_nomecompleto']))
    {
      ++$classificacao;
    }

    if($data['CNPJ'] == $input['dados_cpfcnpj'])
    {
      ++$classificacao;
    }

    if($data['Status da empresa'] == 'Ativa')
    {
      ++$classificacao;
    }

    return $classificacao;
  }

  protected function obterDadosClassificador($dom)
  {
    $divs = $dom->find('.col-md-8 div');
    $divs_fora = $dom->find('.col-md-8');

    $data = array();
    foreach($divs as $key => $value)
    {
      if(!is_object($value->find('h4')[0]))
      {
        continue;
      }

      $v = $value->find('h4')[0]->innerHtml();
      $v = str_replace('<strong>', '', $v);
      $v = str_replace('</strong>', '', $v);
      $v = trim($v);

      if(!is_object($value->find('h5')[0]))
      {
        continue;
      }

      $data[$v] = trim($value->find('h5')[0]->innerHtml());
    }

    foreach($divs_fora as $key => $value)
    {
      if(!is_object($value->find('h4')[0]))
      {
        continue;
      }

      $v = $value->find('h4')[0]->innerHtml();
      $v = str_replace('<strong>', '', $v);
      $v = str_replace('</strong>', '', $v);
      $v = trim($v);

      if(!is_object($value->find('h5')[0]))
      {
        continue;
      }

      $data[$v] = trim($value->find('h5')[0]->innerHtml());
    }

    if(array_key_exists('Endereço', $data))
    {
      unset($data['Endereço']);
    }

    return $data;
  }

}
