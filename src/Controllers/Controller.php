<?php

namespace Credenciamento\Controllers;

abstract class Controller {

  public $view;
  public $assets;
  public $oldInput = array();
  protected $template;

  public function __construct($onlyContent) {

    $cache = CACHE_DEBUG;
    $twig_ops = array();

    if(!$cache)
    {
      $twig_ops = array(
        array(
          'cache' => CREDENCIAMENTO_BASE_DIR . '/src/resources/cache'
        )
      );

    }

    $this->template = 'index.phtml';

    if($onlyContent)
    {
      $this->template = 'onlyHtml.phtml';
    }

    $loader = new \Twig_Loader_Filesystem(TEMPLATE_PATH);
    $this->view = $twig = new \Twig_Environment($loader, $twig_ops);
  }

}
