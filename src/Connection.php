<?php

namespace Credenciamento;

use PDO
  , FluentPDO;

/**
* Singleton para conexão no banco de dados
**/
class Connection
{
  static $pdo_config = array();
  static $pdo = null;
  static $fpdo = null;

  public static function instancePDO()
  {
    if(self::$pdo === NULL)
    {
      $pdo = new PDO("pgsql:host=localhost;dbname=" . self::$pdo_config['db'], self::$pdo_config['user'], self::$pdo_config['password']);
    }

    return self::$pdo = $pdo;
  }

  public static function instanceFPDO()
  {
    if(self::$fpdo === NULL)
    {
      self::$fpdo = $fpdo = new FluentPDO(self::$pdo);
    }

    return self::$fpdo;
  }

}
