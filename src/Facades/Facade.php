<?php

namespace Credenciamento\Facades;

abstract class Facade {

  public $assets = array();
  public $onlyContent = true;
  public $oldInput = array();

}
