<?php

namespace Credenciamento\Facades;

class Stone extends Facade {

  public $usuario_id;

  public function obterFormulario()
  {
    $controller = new \Credenciamento\Controllers\Stone($this->onlyContent);

    $controller->assets = $this->assets;
    $controller->oldInput = $this->oldInput;

    return $controller->obterFormulario();
  }

  public function salvarFormulario($input = array())
  {
    $controller = new \Credenciamento\Controllers\Stone($this->onlyContent);

    return $controller->salvarFormulario($input, $this->usuario_id);
  }

}
