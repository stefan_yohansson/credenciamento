<?php

namespace Credenciamento\Structs\Products;

use Credenciamento\Structs\Maquineta,
    Respect\Validation\Validator as v,
    PHPHtmlParser\Dom,
    Credenciamento\Connection,
    Credenciamento\Curl,
    Cocur\Slugify\Slugify;

class Stone extends Maquineta {

  const ID = 1;

  public static $formulario_cadastro = '/forms/cadastro_stone.phtml';
  public $validator;

  public $expected_data = array(
    'dados_cpfcnpj',
    'dados_mcc',
    'dados_razaosocial_nomecompleto',
    'dados_faturamentomensal',
    'dados_ticketmedio',

    'pagamentos_banco',
    'pagamentos_agencia',
    'pagamentos_agenciadv',
    'pagamentos_conta',
    'pagamentos_contadv',

    'contatos_nome',
    'contatos_email',
    'contatos_telefone',
    'contatos_celular',

    'enderecos_endereco',
    'enderecos_numero',
    'enderecos_complemento',
    'enderecos_bairro',
    'enderecos_cep',
    'enderecos_cidade',
    'enderecos_uf',
    'endereco_operadora',

    'condcomerciais_txdebito',
    'condcomerciais_txcredito',
    'condcomerciais_txparcelado26',
    'condcomerciais_txparcelado712',

    'condcomerciais_meiocaptura',
  );

  public function validator()
  {
    $this->validator = $stoneValidator = v::attribute('dados_cpfcnpj', v::string()->notEmpty()->length(1,32))
                      ->attribute('dados_mcc', v::string()->notEmpty()->length(1, 64))
                      ->attribute('dados_razaosocial_nomecompleto', v::string()->notEmpty()->length(1, 125))
                      ->attribute('dados_faturamentomensal', v::string()->length(1, 32))
                      ->attribute('dados_ticketmedio', v::string()->length(1, 32))

                      ->attribute('pagamentos_banco', v::string()->notEmpty()->length(1, 32))
                      ->attribute('pagamentos_agencia', v::string()->notEmpty()->length(1, 32))
                      ->attribute('pagamentos_agenciadv', v::string()->notEmpty()->length(1, 32))
                      ->attribute('pagamentos_conta', v::string()->notEmpty()->length(1, 32))
                      ->attribute('pagamentos_contadv', v::string()->notEmpty()->length(1, 32))

                      ->attribute('contatos_nome', v::string()->notEmpty()->length(1, 125))
                      ->attribute('contatos_email', v::string()->notEmpty()->length(1, 125))
                      ->attribute('contatos_celular', v::string()->notEmpty()->length(1, 32))
                      ->attribute('contatos_telefone', v::string()->notEmpty()->length(1, 32))

                      ->attribute('enderecos_endereco', v::string()->notEmpty()->length(1, 32))
                      ->attribute('enderecos_numero', v::string()->notEmpty()->length(1, 32))
                      ->attribute('enderecos_complemento', v::string()->length(0, 32))
                      ->attribute('enderecos_bairro', v::string()->notEmpty()->length(1, 32))
                      ->attribute('enderecos_cep', v::string()->notEmpty()->length(1, 32))
                      ->attribute('enderecos_cidade', v::string()->notEmpty()->length(1, 32))
                      ->attribute('enderecos_uf', v::string()->notEmpty()->length(1, 32))
                      ->attribute('endereco_operadora', v::arr()->notEmpty())

                      ->attribute('condcomerciais_txdebito', v::string()->notEmpty()->length(1, 125))
                      ->attribute('condcomerciais_txcredito', v::string()->notEmpty()->length(1, 125))
                      ->attribute('condcomerciais_txparcelado26', v::string()->notEmpty()->length(1, 32))
                      ->attribute('condcomerciais_txparcelado712', v::string()->notEmpty()->length(1, 32))

                      ->attribute('condcomerciais_meiocaptura', v::arr()->notEmpty());

    return $stoneValidator->validate($this);
  }

  public function insert(array $values, array $input)
  {
    $values['rede'] = self::ID;

    $query = Connection::instanceFPDO()->insertInto('produtos')->values($values);
    $lastInsert = $query->execute('produtos_id_seq');
    
    $input['produto_id'] = $lastInsert;

    $input = $this->tratarDados($input);

    $query = Connection::instanceFPDO()->insertInto('produtos_stone')->values($input);

    $query->execute();
  }

  protected function tratarDados($input)
  {
    $input['endereco_operadora'] = \implode(', ', $input['endereco_operadora']);
    $input['condcomerciais_meiocaptura'] = \implode(', ', $input['condcomerciais_meiocaptura']);

    return $input;
  }

  public function obterClassificador($cnpj, $nome_empresa)
  {
    $dom = new Dom;
    
    $slugify = new Slugify();
    $nome_empresa = $slugify->slugify($nome_empresa);
    $cnpj = $slugify->slugify($cnpj, '');
    
    $url = 'http://empresasdobrasil.com/empresa/' . $nome_empresa . '-'.$cnpj;
    
    $curl = new Curl;
    $dom->loadFromUrl($url, [], $curl);
    
    
    return $dom;
  }

}
