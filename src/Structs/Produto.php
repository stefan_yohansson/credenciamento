<?php

namespace Credenciamento\Structs;

use Credenciamento\Structs\Interfaces\ProdutoInterface;

abstract class Produto implements ProdutoInterface {

  public $expected_data = array();

  public function fillAttr($input = array())
  {
    foreach($input as $k => $v)
    {
      $this->$k = $v;
    }
  }

}
