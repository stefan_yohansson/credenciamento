<?php

define('CREDENCIAMENTO_BASE_DIR', dirname(__FILE__));
define('TEMPLATE_PATH', CREDENCIAMENTO_BASE_DIR . '/src/templates');

require_once CREDENCIAMENTO_BASE_DIR . '/vendor/autoload.php';

require_once(CREDENCIAMENTO_BASE_DIR . '/init.php');
