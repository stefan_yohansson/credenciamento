Credenciamento
===

# Como Instalar

- php
- postgres
- bower

Basta instalar as dependências via composer com o comando `composer install` e as do bower com o comando `bower install`.

Feito isso, crie um arquivo `env.php` na raiz do projeto e coloque o seguinte conteúdo:

```
<?php

define('CACHE_DEBUG', true);

\Credenciamento\Connection::$pdo_config = [
    'db' => 'credenciamento',
    'user' => 'homestead',
    'password' => 'secret'
];
```

rode na raiz do projeto: `php -S localhost:5000` e acesse `http://localhost:5000/examples/formulariostone.php`
